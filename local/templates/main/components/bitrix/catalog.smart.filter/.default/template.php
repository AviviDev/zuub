<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!empty($arResult["ITEMS"])):
?>
<form action="/bolezni.html" method="get" data-checkup="true">
<input type="hidden" name="set_filter" value="y"/>
  <div class="row">
	  <div class="row-table">
		  <div class="col-xs-16 col-sm-18 col-vertical-middle">

			  <div class="b-form_box">

				  <label class="b-form_box_field">
					  <select name="<?=$arParams["FILTER_NAME"]?>_19" class="js-selectric">
						  <option value="">Симптомы</option>
						  <?foreach($arResult["ITEMS"][19]["VALUES"] as $arItem):?>
						  <option value="<?=$arItem["HTML_VALUE_ALT"]?>" <?if($arItem["CHECKED"]):?>selected<?endif;?>><?=$arItem["VALUE"]?></option>
						  <?endforeach;?>
					  </select>
				  </label>

			  </div>

		  </div>
		  <div class="col-xs-8 col-sm-6 col-vertical-middle">

			  <button type="submit" class="e-btn e-btn_green_outline e-btn_md e-btn_block">Поиск</button>

		  </div>
	  </div>
  </div>

</form>
<?endif;?>

