<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem)
{
    $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/doctor_list_item.php"), Array("arItem" => $arItem, "isGray" => ($key%2)?true:false), Array("MODE"=>"php") );
}
?>
<?if ($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<? echo $arResult["NAV_STRING"];?>
<?endif;?>
<?
/*
print("<pre>");
print_r($arResult);
print("</pre>");
*/
?>
