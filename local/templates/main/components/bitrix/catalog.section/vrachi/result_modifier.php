<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
foreach($arResult["ITEMS"] as $key => $arItem)
{
	//Обработка изображения. Начало
	if(empty($arItem["PREVIEW_PICTURE"]))
		$arItem["PREVIEW_PICTURE"] = $arItem["DETAIL_PICTURE"];
	if(!empty($arItem["PREVIEW_PICTURE"]))
	{
		$img = CFile::ResizeImageGet(
			$arItem["PREVIEW_PICTURE"]["ID"],
			array("width"=>290, "height"=>600),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			false
		);
		if(!empty($img))
		{
			$arItem["PREVIEW_PICTURE"]["SRC"] = $img["src"];
			$arItem["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
			$arItem["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
		}
	}
	$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arItem["PREVIEW_PICTURE"];
	//Определяем стаж работы. Начало
	$year = 0;
	if(!empty($arItem["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))
		$year = (time() - MakeTimeStamp($arItem["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))/(365*86400);
	$arResult["ITEMS"][$key]["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"] = intVal($year);

	//Подготовка вывода специализации врача. Начало
	$specializatsiya = "";
	if(!empty($arItem["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]) &&
		!is_array($arItem["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]))
		$arItem["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] = array($arItem["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]);
	foreach($arItem["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] as $keyProf=>$profession)
	{
		if($keyProf > 0) $specializatsiya .= ", ";
		$specializatsiya .= strtolower($profession);
	}
	$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] = ucfirst_utf8(strtolower($specializatsiya));
	//Подготовка вывода специализации врача. Конец

	//Выясняем количество отзывов
	if(!empty($arItem["PROPERTIES"]["HLB_VRACHI"]["VALUE"]))
	{
		$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"] = "#";
		$iblockObj = CIBlock::GetByID(BX_INFOBLOCK_OTZYV);
		if ($iblock = $iblockObj->GetNext())
			$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"] = $iblock["~LIST_PAGE_URL"];
		$elementsObj = CIBlockElement::GetList(
			array("DATE_ACTIVE_FROM" => "DESC"),
			array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_OTZYV, "PROPERTY_HLB_VRACHI" => $arItem["PROPERTIES"]["HLB_VRACHI"]["VALUE"])
		);
		$elementsObj->NavStart(1);
		$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"] = intVal($elementsObj->SelectedRowsCount());
	}

	//Определяем основную клинику для врача. Начало
	if(!empty($arItem["PROPERTIES"]["HLB_KLINIKI"]["VALUE"]))
	{
		$elObj = CIBlockElement::GetList(
			array("SORT" => "ASC"),
			array("PROPERTY_HLB_KLINIKI" => $arItem["PROPERTIES"]["HLB_KLINIKI"]["VALUE"], "IBLOCK_ID" => 8),
			false,
			false,
			array("ID", "NAME", "PROPERTY_SHORT_NAME", "DETAIL_PAGE_URL")
		);
		while($element = $elObj->GetNext())
		{
			/*if(!empty($arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"]))
				$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"].=" / ";*/
			$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"][] = (!empty($element["PROPERTY_SHORT_NAME_VALUE"]))?$element["PROPERTY_SHORT_NAME_VALUE"]:$element["NAME"];
		}
	}

	//Определяем основную клинику для врача. Конец

}

?>