<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('highloadblock'); // подключаем модуль HL блоков
$hlblock_id = 1; // ID вашего Highload-блока
$hlblock   = Bitrix\Highloadblock\HighloadBlockTable::getById( $hlblock_id )->fetch(); // получаем объект вашего HL блока
$entity   = Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $hlblock );  // получаем рабочую сущность
$entity_data_class = $entity->getDataClass(); // получаем экземпляр класса
$entity_table_name = $hlblock['TABLE_NAME']; // присваиваем переменной название HL таблицы
$sTableID = 'tbl_'.$entity_table_name; // добавляем префикс и окончательно формируем название


// $arrmonth = array( 
//   "01" => "Январь", 
//   "02" => "Февраль", 
//   "03" => "Март", 
//   "04" => "Апрель", 
//   "05" => "Май", 
//   "06" => "Июнь", 
//   "07" => "Июль", 
//   "08" => "Август", 
//   "09" => "Сентябрь", 
//   "10" => "Октябрь", 
//   "11" => "Ноябрь", 
//   "12" => "Декабрь" 
// ); 
$arResultNew = array();
foreach($arResult["ITEMS"] as $key=>$arItem)
{
	if(!empty($arItem["PROPERTIES"]["VRACH"]["VALUE"]))
	{
		$elementObj = CIBlockElement::GetByID($arItem["PROPERTIES"]["VRACH"]["VALUE"]);
		if($element = $elementObj->GetNextElement())
		{
			$arItem["ANSWER_DOCTOR"] = $element->GetFields();
			if(empty($arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]))
				$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"] = $arItem["ANSWER_DOCTOR"]["DETAIL_PICTURE"];

			if(!empty($arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]))
			{
				$img = CFile::ResizeImageGet(
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"],
					array("width"=>290, "height"=>600),
					BX_RESIZE_IMAGE_PROPORTIONAL,
					false
				);

				if(!empty($img))
				{
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"] = array();
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["SRC"] = $img["src"];
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
				}
			}
			$arItem["ANSWER_DOCTOR"]["PROPERTIES"] = $element->GetProperties();
		}
	}
	if(!empty($arItem["ACTIVE_FROM"]))
	{
		$arItem["ACTIVE_FROM"] = array(
			"SHORT"	=> $DB->FormatDate($arItem["ACTIVE_FROM"], CSite::GetDateFormat("FULL"), "YYYY-MM-DD"),
			"RUS"	=> FormatDateFromDB($arItem["ACTIVE_FROM"], 'DD MMMM YYYY')
		);
		// $arDate = explode('-',$arItem["ACTIVE_FROM"]["SHORT"]);
		// $arItem["ACTIVE_FROM"]['RUS'] = $arDate[0].' г. '.$arrmonth[$arDate[1]].' '.$arDate[2];
		
	}
	if(!empty($arItem["PROPERTIES"]["USLUGI_TAGS"]["VALUE"]))
	{
		$res = CIBlockElement::GetList(Array(), array("IBLOCK_ID"=>1, "ACTIVE"=>"Y", "ID"=>$arItem["PROPERTIES"]["USLUGI_TAGS"]["VALUE"]), false, false, array("ID", "DETAIL_PAGE_URL","NAME"));
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$arLinks[$arFields['ID']] = array('LINK'=>$arFields['DETAIL_PAGE_URL'], "NAME"=>$arFields['NAME']);
		}
		foreach ($arItem["PROPERTIES"]["USLUGI_TAGS"]["VALUE"] as $id) {
			foreach ($arLinks as $key_link => $link) {
				if ($id == $key_link) {
					$arItem["PROPERTIES"]["USLUGI_TAGS"]["VALUE_USLUGA_LINK"][$id]['LINK'] = $link['LINK'];
					$arItem["PROPERTIES"]["USLUGI_TAGS"]["VALUE_USLUGA_LINK"][$id]['NAME'] = $link['NAME'];
				}
			}			
			
		}
	}
	/*if(!empty($arItem["PROPERTIES"]["HLB_USLUGI"]["VALUE"]))
	{
		foreach ($arItem["PROPERTIES"]["HLB_USLUGI"]["VALUE"] as $value) {
			$arFilter = array("UF_XML_ID" => $value); 
			$arSelect = array('UF_NAME', 'UF_USLUGA'); 
			$arOrder = array("UF_NAME"=>"ASC"); 

			$rsData = $entity_data_class::getList(array(
			    "select" => $arSelect,
			    "filter" => $arFilter,
			    "limit" => '5',
			    "order" => $arOrder
			));
			
			$rsData = new CDBResult($rsData, $sTableID); 
			while($arRes = $rsData->Fetch()){
				$arItem["PROPERTIES"]["HLB_USLUGI"]["VALUE_TEXT"][] = $arRes['UF_NAME'];
				$arItem["PROPERTIES"]["HLB_USLUGI"]["VALUE_USLUGA_ID"][] = $arRes['UF_USLUGA'];
			}
		}
		$res = CIBlockElement::GetList(Array(), array("IBLOCK_ID"=>1, "ACTIVE"=>"Y", "ID"=>$arItem["PROPERTIES"]["HLB_USLUGI"]["VALUE_USLUGA_ID"]), false, false, array("ID", "DETAIL_PAGE_URL"));
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$arLinks[$arFields['ID']] = $arFields['DETAIL_PAGE_URL'];
		}
		foreach ($arItem["PROPERTIES"]["HLB_USLUGI"]["VALUE_USLUGA_ID"] as $id) {
			foreach ($arLinks as $key_link => $link) {
				if ($id == $key_link) {
					$arItem["PROPERTIES"]["HLB_USLUGI"]["VALUE_USLUGA_LINK"][$id] = $link;
				}
			}			
			
		}
		
	}*/	
	
	$string = substr($arItem["~PREVIEW_TEXT"], 0, 200);
	$string = rtrim($string, "!,.-");
	$string = substr($string, 0, strrpos($string, ' '));
	$arItem['PREVIEW_TEXT'] = $string."… ";


	// $arResult["ITEMS"][$key] = $arItem;

	$arResultNew["ITEMS"][$arItem['ID']] = $arItem;

}

$arResult['ITEMS'] = $arResultNew["ITEMS"];

if ($_REQUEST['id']) {

	$arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, false, array());
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arProps = $ob->GetProperties();
		$in_array = false;
		foreach ($arResult["ITEMS"][$_REQUEST['id']]['PROPERTIES']['HLB_USLUGI']['VALUE'] as $artServ) {
			foreach ($arProps["HLB_USLUGI"]['VALUE'] as $arArt) {
				if ($artServ == $arArt) {
					$in_array = true;
				}
			}
		}
		if (!$in_array) {
			continue;
		}
		if(!empty($arFields["DETAIL_PICTURE"]))
			$arFields["PREVIEW_PICTURE"] = $arFields["DETAIL_PICTURE"];
		if(!empty($arFields["PREVIEW_PICTURE"]))
		{
			$img = CFile::ResizeImageGet(
				$arFields["PREVIEW_PICTURE"],
				array("width"=>268, "height"=>182),
				BX_RESIZE_IMAGE_EXACT,
				false
			);
			$arFields["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
		}
		


		$arResult['STATI'][] = array('FIELDS'=>$arFields, 'PROPS'=>$arProps);
	}
	
}
?>