<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!$_REQUEST['id']) {?>
<section class="b-section b-section__closed  b-section__gray">

	<header class="b-heading">
		<h2 class="b-heading_subtitle"><?$APPLICATION->ShowTitle(false)?></h2>
  	</header>

	<?foreach($arResult["ITEMS"] as $arItem):?>
	  	<aside class="b-comment b-faq_preview">
	  		<div class="row b-faq_header">
	  			<div class="row-table-lg row-table-md">
	  				<div class="b-comment_header col-lg-8 col-md-8 col-vertical-middle in_v_middle">
	  					<span class="b-comment_header_author"><?=$arItem["NAME"]?></span>
	  					<time datetime="<?=$arItem["ACTIVE_FROM"]["SHORT"]?>"><?=$arItem["ACTIVE_FROM"]["RUS"]?></time>
	  				</div>

					<?if ($arItem['PROPERTIES']['USLUGI_TAGS']['VALUE_USLUGA_LINK']) {?>
		  				<div class="b-faq_tags col-lg-16 col-md-16 col-vertical-middle in_v_middle">
		  					<?foreach ($arItem['PROPERTIES']['USLUGI_TAGS']['VALUE_USLUGA_LINK'] as $key => $value) { ?>
		  						<a href="<?=$value['LINK']?>" class="item_tag"><?=$value['NAME']?></a>
		  					<?}?>
		  				</div>
					<?}?>
	  			</div>
	  		</div>
	  		<p><?=$arItem["PREVIEW_TEXT"]?></p>
	  		<p><a href="?id=<?=$arItem["ID"]?>">подробнее...</a></p>

	  	</aside>
	<?endforeach;?>

  	<footer class="b-service_faq_footer">
  		<div class="row">
  			<div class="row-table-lg row-table-md row-table-sm">
  				<div class="col-xs-24 col-md-12 col-vertical-middle in_v_middle">

  					<div class="b-section_footer">

  						<?if ($arParams["DISPLAY_BOTTOM_PAGER"]):?>
							<? echo $arResult["NAV_STRING"];?>
						<?endif;?>

  					</div>

  				</div>
  				<div class="col-xs-24 col-md-12 col-vertical-middle g-right faq_bottom_button in_v_middle">

  					<a class="e-btn e-btn_md e-btn_green_outline js-popup" href="/forms/ask/?pushivent=zadat_vopros">Задать вопрос</a>

  				</div>
  			</div>
  		</div>
  	</footer>
</section>
<!-- White :: end -->
<?}else{?>
	
	<header class="b-heading">
		<div class="row">
  			<div class="row-table-lg row-table-md row-table-sm">
  				<div class="col-xs-24 col-md-12 col-vertical-middle in_v_middle">

  					<div class="b-section_footer">

  						

  					</div>

  				</div>
  				<div class="col-xs-24 col-md-12 col-vertical-middle g-right faq_bottom_button in_v_middle">

  					<a class="e-btn e-btn_md e-btn_green_outline js-popup" href="/forms/ask/?pushivent=zadat_vopros">Задать вопрос</a>

  				</div>
  			</div>
  		</div>
  	</header>

	<section class="b-service_faq b-section b-section__closed" id="service-faq">

		<div class="b-service_faq_list" id="service-faq-list">
			
			
			<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/vopros_otvet_item.php"), Array("arItem" => $arResult["ITEMS"][$_REQUEST['id']]), Array("MODE"=>"php") );?>
			
		</div>


	</section>
	<?
	$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/simptomi.php"), Array("isGray" => true), Array("MODE"=>"php") );	
	?>
	<section class="b-service_articles b-section b-section__closed b-section__last">

		<header class="b-heading">
		  	<div class="row">
			  	<div class="row-table">
				  	<div class="col-xs-12 col-md-8 col-vertical-middle">
					  	<h2 class="b-heading_subtitle">Статьи</h2>
				  	</div>
				  	<div class="col-xs-12 col-md-4 col-vertical-middle hidden-xs hidden-sm g-right">
					  	<a href="/articles.html" class="b-heading_more i-icon i-pages-articles"><span>Все статьи</span></a>
				  	</div>
			  	</div>
		  	</div>
	  	</header>

	  	<div class="b-service_articles_carousel b-carousel" id="service-articles-carousel" data-auto-height="false" data-nav="true" data-nav-container="#service-articles-carousel-controls" data-dots="true" data-dots-container="#service-articles-carousel-controls" data-dots-each="true" data-auto="false" data-interval="10" data-margin="21" data-xs="1" data-sm="2" data-md="3" data-lg="3">
		  
		  	<?foreach($arResult['STATI'] as $arItem):?>
			  	<div class="b-carousel_item">
					<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/stati_item.php"), Array("arItem" => $arItem['FIELDS']), Array("MODE"=>"php") );?> 
			  	</div>
		  	<?endforeach?>		  
	  	</div>
		<?if(count($arResult['STATI']) > 1):?>
		  	<footer class="b-service_articles_carousel_footer ">
			  	<div class="b-carousel_controls" id="service-articles-carousel-controls"></div>
		  	</footer>
		<?endif;?>
	</section>
	<?
	$APPLICATION->AddChainItem($arResult["ITEMS"][$_REQUEST['id']]['~NAME'], $_SERVER['REQUEST_URI']);
	?>
	
<?}?>
<?
// print("<pre>");
// print_r($arResult["ITEMS"]);
// print("</pre>");

?>






