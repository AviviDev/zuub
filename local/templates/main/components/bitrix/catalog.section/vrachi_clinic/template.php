<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult["ITEMS"])):?>
<section class="b-clinic_dentists b-section b-section__closed b-section__last" id="clinic-dentists">
    <header class="b-heading">
        <h2 class="b-heading_subtitle">Врачи</h2>
    </header>
    <?
    foreach($arResult["ITEMS"] as $arItem)
    {
        $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/doctor_list_item.php"), Array("arItem" => $arItem), Array("MODE"=>"php") );
    }
    ?>
</section>
<?endif;?>
<?
/*
print("<pre>");
print_r($arResult);
print("</pre>");
*/
?>
