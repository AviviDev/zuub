<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$newPrices = array();
$insidePrices = array();
$morePrices = array();
foreach($arResult["ITEMS"] as $key => $arItem)
{
	$arItem["PROPERTIES"]["PRICE_OLD"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($arItem["PROPERTIES"]["PRICE_OLD"]["VALUE"], 0, '', ' '));
	$arItem["PROPERTIES"]["PRICE"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($arItem["PROPERTIES"]["PRICE"]["VALUE"], 0, '', ' '));
	$arItem["PROPERTIES"]["PRICE_TO"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($arItem["PROPERTIES"]["PRICE_TO"]["VALUE"], 0, '', ' '));
	if(!empty($arItem["PROPERTIES"]["USLUGA"]["VALUE"]))
		$newPrices[] = $arItem; 
	foreach($arItem["PROPERTIES"]["INSIDE_PRICE"]["VALUE"] as $insidePriceId)
	{
		$insidePrices[$insidePriceId][] = $arItem; 
	}
	foreach($arItem["PROPERTIES"]["MORE_PRICE"]["VALUE"] as $morePriceId)
	{
		$morePrices[$morePriceId][] = $arItem; 
	}
}
unset($arResult["ITEMS"]);

foreach ($insidePrices as $key_price => $arPrices) {
	foreach ($arPrices as $key => $value) {
		if ($value['PROPERTIES']['NO_VIEW_IN_PRICE']['VALUE'] == 'Y'){
			unset($insidePrices[$key_price][$key]);
		}
	}	
}
foreach ($morePrices as $key_price => $arPrices) {
	foreach ($arPrices as $key => $value) {
		if ($value['PROPERTIES']['NO_VIEW_IN_PRICE']['VALUE'] == 'Y'){
			unset($morePrices[$key_price][$key]);
		}
	}	
}

$arResult["ITEMS"] = array();
foreach($newPrices as $price)
{
	$price["INSIDE_PRICE"] = $insidePrices[$price["ID"]];
	$price["MORE_PRICE"] = $morePrices[$price["ID"]];
	$arResult["ITEMS"][] = $price;
}



$arAllServices = array();

$arSelect = Array("ID", "IBLOCK_ID", "NAME","PROPERTY_ICON_CLASS","PROPERTY_PARENT");
$arFilter = Array("IBLOCK_ID"=>BX_INFOBLOCK_USLUGI, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(array("ID"=>"ASC"), $arFilter, false, false, $arSelect);
while($ob = $res->Fetch())
{
	$arNamesServices[$ob['ID']]['NAME'] = $ob['NAME'];
	$arNamesServices[$ob['ID']]['PARENT'] = $ob["PROPERTY_PARENT_VALUE"];
	$arNamesServices[$ob['ID']]['ICON_CLASS'] = $ob["PROPERTY_ICON_CLASS_VALUE"];
}
foreach ($arResult['ITEMS'] as $key => $price) {
	foreach ($arNamesServices as $key_name => $NamesService) {
		if ($price['PROPERTIES']['USLUGA']['VALUE']){
			foreach ($price['PROPERTIES']['USLUGA']['VALUE'] as $service) {
				if ($service == $key_name) {
					if ($NamesService['PARENT']) {
						$arAllServices[$NamesService['PARENT']]['ITEMS'][] = $price;	
						$arAllServices[$NamesService['PARENT']]['SERVICE_NAME'] = $arNamesServices[$NamesService['PARENT']]['NAME'];	
						$arAllServices[$NamesService['PARENT']]['ICON_CLASS'] = $arNamesServices[$NamesService['PARENT']]['ICON_CLASS'];	
					}else{
						$arAllServices[$service]['ITEMS'][] = $price;
						$arAllServices[$service]['SERVICE_NAME'] = $NamesService['NAME'];
						$arAllServices[$service]['ICON_CLASS'] = $NamesService['ICON_CLASS'];
					}
				}
			}
		}
	}
}
$arResult["ITEMS"] = $arAllServices;

// echo "<pre>"; print_r($arResult["ITEMS"]); echo "</pre>";
?>