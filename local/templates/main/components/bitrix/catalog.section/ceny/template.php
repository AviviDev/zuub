<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach ($arResult["ITEMS"] as $keyService => $arService) : ?>
	<div class="b-header_services_title i-icon <?=$arService["ICON_CLASS"]?> js-spoiler-toggle">
		<a href="#<?=$keyService?>"><?=$arService["SERVICE_NAME"]?></a>
	</div>
<?endforeach;?>

<?foreach ($arResult["ITEMS"] as $keyService => $arService) : ?>
	<header class="b-heading" id="<?=$keyService?>">
    	<h2><?=$arService['SERVICE_NAME']?></h2>
  	</header>
  	<?foreach ($arService['ITEMS'] as $keyItem => $price) : 
  		if ($price['PROPERTIES']['NO_VIEW_IN_PRICE']['VALUE'] == 'Y') continue;
		?>
		<article class="b-price js-spoiler">

			<header class="b-price_heading">
				<div class="row">
					<div class="row-table-md row-table-lg">
						<div class="col-xs-24 col-md-14 col-md-vertical-middle col-lg-vertical-middle">

							<h2 class="b-price_heading_title"><?=$price["~NAME"]?> <button type="button" class="b-price_heading_toggle e-btn i-icon i-chevron-down js-spoiler-toggle"></button></h2>
							
						</div>
						<div class="col-xs-24 col-md-10 col-md-vertical-middle col-lg-vertical-middle g-right-md g-right-lg">

							<div class="b-price_heading_cost">
								<div class="b-price_heading_cost_wrap">
									<?if(!empty($price["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
									<?elseif(!empty($price["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<span class="b-price_heading_cost_strike">до <?=$price["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</span>
									<?elseif(!empty($price["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<s class="b-price_details_main_cost_strike"><?=$price["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
									<?else:?>
										<span class="b-price_details_main_cost_actual"><?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</span>
									<?endif;?>
								</div>
							</div>

						</div>
					</div>
				</div>
			</header>

			<div class="b-price_details js-spoiler-box">

				<!-- Main details -->
				<div class="b-price_details_main">

					<div class="b-price_details_main_text">
						<div class="row">
							<?=$price["~DETAIL_TEXT"]?>
							<div class="col-xs-24 col-md-6 col-lg-6 g-center-xs g-center-sm g-right-md g-right-lg">

								<div class="b-price_details_main_cost">
									<?if(!empty($price["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
									<?elseif(!empty($price["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<span class="b-price_details_main_cost_strike">до <?=$price["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</span>
									<?elseif(!empty($price["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<s class="b-price_details_main_cost_strike"><?=$price["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
									<?else:?>
										<span class="b-price_details_main_cost_actual"><?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</span>
									<?endif;?>
								</div>

							</div>
						</div>
					</div>

					<div class="b-price_details_main_footer">
						<div class="row">
							<div class="b-price_details_main_footer_grid row-table-md row-table-lg">
								<div class="col-xs-24 col-md-16 col-md-vertical-middle col-lg-vertical-middle">
									<?=$price["~PREVIEW_TEXT"]?>
								</div>
								<div class="col-xs-24 col-md-8 col-md-vertical-middle col-lg-vertical-middle g-center-xs g-center-sm g-right-md g-right-lg">

									<a href="/forms/?form_id=1&pushivent=zapis_na_priem_ceny" class="b-service_prices_details_footer_btn e-btn e-btn_md e-btn_green js-popup zapis_na_priem_ceny" data-box-width="350">Записаться на прием</a>

								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- Main details end -->
				<?if(!empty($price["INSIDE_PRICE"])):?>
				<!-- Additional info -->
				<table class="b-table b-table__rounded b-table__collapsed">

					<tr class="b-table_header">
						<th colspan="2">
							<div class="b-table_padding">Услуги</div>
						</th>
					</tr>
					<?foreach($price["INSIDE_PRICE"] as $arPrice):?>
						<tr>
							<td><?=$arPrice["~NAME"]?></td>
							<td class="g-right-md g-right-lg">
								  <span class="b-table_price b-table_price__lg">
									  <?if(!empty($arPrice["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
										  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
									  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
										  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										  <i class="b-price_details_main_cost_strike"> <i>до <?=$arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</i>
									  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
										  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
										  <s><?=$arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
									  <?else:?>
										  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
									  <?endif;?>
								  </span>
							</td>
						</tr>
					<?endforeach;?>
				</table>
				<?endif;?>
				<?if(!empty($price["MORE_PRICE"])):?>
					<!-- Additional info -->
					<table class="b-table b-table__rounded b-table__collapsed">

						<tr class="b-table_header">
							<th colspan="2">
								<div class="b-table_padding">Дополнительные услуги</div>
							</th>
						</tr>
						<?foreach($price["MORE_PRICE"] as $arPrice):?>
							<tr>
								<td><?=$arPrice["~NAME"]?></td>
								<td class="g-right-md g-right-lg">
									  <span class="b-table_price b-table_price__lg">
										  <?if(!empty($arPrice["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
											  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
										  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
										  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										  <i class="b-price_details_main_cost_strike"> <i>до <?=$arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</i>
											  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
												  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
												  <s><?=$arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
											  <?else:?>
												  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
											  <?endif;?>
									  </span>
								</td>
							</tr>
						<?endforeach;?>
					</table>
				<?endif;?>
			</div>
		</article>
  	<?endforeach?>
  	<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/trigger.php"), Array("pushivent"=>"zapis_na_priem_ceny_trigger"), Array("MODE"=>"php") );?>
<?endforeach;?>
		<?/*foreach($arResult["ITEMS"] as $keyItem=>$price):			
			if ($price['PROPERTIES']['NO_VIEW_IN_PRICE']['VALUE'] == 'Y') continue;
			if($keyItem > 0 && $keyItem%7 == 0)
			$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/trigger.php"), Array("pushivent"=>"zapis_na_priem_ceny_trigger"), Array("MODE"=>"php") );?>
			<article class="b-price js-spoiler">

				<header class="b-price_heading">
					<div class="row">
						<div class="row-table-md row-table-lg">
							<div class="col-xs-24 col-md-14 col-md-vertical-middle col-lg-vertical-middle">

								<h2 class="b-price_heading_title"><?=$price["~NAME"]?> <button type="button" class="b-price_heading_toggle e-btn i-icon i-chevron-down js-spoiler-toggle"></button></h2>
								
							</div>
							<div class="col-xs-24 col-md-10 col-md-vertical-middle col-lg-vertical-middle g-right-md g-right-lg">

								<div class="b-price_heading_cost">
									<div class="b-price_heading_cost_wrap">
										<?if(!empty($price["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
											<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<?elseif(!empty($price["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
											<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
											<span class="b-price_heading_cost_strike">до <?=$price["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</span>
										<?elseif(!empty($price["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
											<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
											<s class="b-price_details_main_cost_strike"><?=$price["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
										<?else:?>
											<span class="b-price_details_main_cost_actual"><?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</span>
										<?endif;?>
									</div>
								</div>

							</div>
						</div>
					</div>
				</header>

				<div class="b-price_details js-spoiler-box">

					<!-- Main details -->
					<div class="b-price_details_main">

						<div class="b-price_details_main_text">
							<div class="row">
								<?=$price["~DETAIL_TEXT"]?>
								<div class="col-xs-24 col-md-6 col-lg-6 g-center-xs g-center-sm g-right-md g-right-lg">

									<div class="b-price_details_main_cost">
										<?if(!empty($price["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
											<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<?elseif(!empty($price["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
											<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
											<span class="b-price_details_main_cost_strike">до <?=$price["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</span>
										<?elseif(!empty($price["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
											<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
											<s class="b-price_details_main_cost_strike"><?=$price["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
										<?else:?>
											<span class="b-price_details_main_cost_actual"><?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</span>
										<?endif;?>
									</div>

								</div>
							</div>
						</div>

						<div class="b-price_details_main_footer">
							<div class="row">
								<div class="b-price_details_main_footer_grid row-table-md row-table-lg">
									<div class="col-xs-24 col-md-16 col-md-vertical-middle col-lg-vertical-middle">
										<?=$price["~PREVIEW_TEXT"]?>
									</div>
									<div class="col-xs-24 col-md-8 col-md-vertical-middle col-lg-vertical-middle g-center-xs g-center-sm g-right-md g-right-lg">

										<a href="/forms/?form_id=1&pushivent=zapis_na_priem_ceny" class="b-service_prices_details_footer_btn e-btn e-btn_md e-btn_green js-popup zapis_na_priem_ceny" data-box-width="350">Записаться на прием</a>

									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- Main details end -->
					<?if(!empty($price["INSIDE_PRICE"])):?>
					<!-- Additional info -->
					<table class="b-table b-table__rounded b-table__collapsed">

						<tr class="b-table_header">
							<th colspan="2">
								<div class="b-table_padding">Услуги</div>
							</th>
						</tr>
						<?foreach($price["INSIDE_PRICE"] as $arPrice):?>
						<tr>
							<td><?=$arPrice["~NAME"]?></td>
							<td class="g-right-md g-right-lg">
								  <span class="b-table_price b-table_price__lg">
									  <?if(!empty($arPrice["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
										  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
									  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
										  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										  <i class="b-price_details_main_cost_strike"> <i>до <?=$arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</i>
									  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
										  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
										  <s><?=$arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
									  <?else:?>
										  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
									  <?endif;?>
								  </span>
							</td>
						</tr>
						<?endforeach;?>
					</table>
					<?endif;?>
					<?if(!empty($price["MORE_PRICE"])):?>
						<!-- Additional info -->
						<table class="b-table b-table__rounded b-table__collapsed">

							<tr class="b-table_header">
								<th colspan="2">
									<div class="b-table_padding">Дополнительные услуги</div>
								</th>
							</tr>
							<?foreach($price["MORE_PRICE"] as $arPrice):?>
								<tr>
									<td><?=$arPrice["~NAME"]?></td>
									<td class="g-right-md g-right-lg">
										  <span class="b-table_price b-table_price__lg">
											  <?if(!empty($arPrice["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
												  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
											  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
											  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
											  <i class="b-price_details_main_cost_strike"> <i>до <?=$arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</i>
												  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
													  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
													  <s><?=$arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
												  <?else:?>
													  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
												  <?endif;?>
										  </span>
									</td>
								</tr>
							<?endforeach;?>
						</table>
					<?endif;?>
				</div>
			</article>
		<?endforeach;*/?>
		<article class="b-price">
			<!-- Payment info -->
			<footer class="b-price_payment">
				<div class="row">
					<div class="row-table-md row-table-lg">

						<div class="col-xs-24 col-md-6 col-lg-7 col-md-vertical-middle col-lg-vertical-middle">

							<div class="b-price_payment_label">

								<p>К оплате принимаются карты</p>

							</div>

						</div>

						<div class="col-xs-24 col-md-11 col-lg-11 col-md-vertical-middle col-lg-vertical-middle">
							<div class="row">
								<div class="row-table">

									<div class="col-xs-13 col-md-13 col-vertical-middle">

										<div class="b-price_payment_logotypes">

											<img src="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/visa.png" alt="Visa" height="24" />
											<img src="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/mastercard.png" alt="Mastercard" height="41" />

										</div>

									</div>

									<div class="col-xs-11 col-md-11 col-vertical-middle">

										<div class="b-price_payment_credit">

											<p>Доступно лечение<br /><a href="/stomatologiya-v-kredit.html">в&nbsp;рассрочку</a></p>

										</div>

									</div>

								</div>
							</div>
						</div>

						<div class="col-xs-24 col-md-7 col-lg-6 g-center-xs g-center-sm g-right-md g-right-lg col-md-vertical-middle col-lg-vertical-middle">

							<a href="/forms/?form_id=1&pushivent=zapis_na_priem_ceny" class="e-btn e-btn_md e-btn_green e-btn_block_md e-btn_block_lg js-popup zapis_na_priem_ceny" data-box-width="350">Записаться на прием</a>

						</div>

					</div>
				</div>
			</footer>
			<!-- Payment info end -->

		</article>


<?/*
print("<pre>");
print_r($arResult);
print("</pre>");
*/
?>
