<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<section class="b-service_reviews b-section b-section__closed" id="service-reviews">
	<div class="b-service_reviews_tabs js-tabs">

		<div class="b-service_reviews_tabs_buttons b-tabs js-tabs-toggle">
			<a href="#service-reviews-site">Отзывы на сайте</a>
			<a href="#service-reviews-vk">ВКонтакте</a>
			<a href="#service-reviews-facebook">Facebook</a>
		</div>
		<div class="js-tabs-wrapper">
			<div class="js-tabs-page" id="service-reviews-site">
				<div id="service-reviews-list">
					<?foreach($arResult["ITEMS"] as $arItem):?>
						<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/review_item.php"), Array("arItem" => $arItem, "isGray" => $isGray), Array("MODE"=>"php") );?>
					<?endforeach;?>
				</div>
				<footer class="b-service_faq_footer">
					<div class="row">
						<div class="row-table">
							<div class="col-xs-10 col-md-12 col-vertical-middle">

								<div class="b-section_footer">

									<?if ($arParams["DISPLAY_BOTTOM_PAGER"]):?>
										<? echo $arResult["NAV_STRING"];?>
									<?endif;?>

								</div>

							</div>
							<div class="col-xs-14 col-md-12 col-vertical-middle g-right">

								<a class="e-btn e-btn_md e-btn_green_outline js-popup" href="/forms/review/?pushivent=ostavit_otzyv">Оcтавить отзыв</a>

							</div>
						</div>
					</div>
				</footer>
			</div>
			<div class="js-tabs-page" id="service-reviews-vk">

				<div class="b-widget" id="vk-thread"></div>

			</div>
			<div class="js-tabs-page" id="service-reviews-facebook">

				<div id="fb-root"></div>
				<div class="b-widget fb-comments" data-href="http://zuub.ru" data-numposts="10"></div>

			</div>
		</div>




</section>

<?/*
print("<pre>");
print_r($arResult);
print("</pre>");
*/
?>
