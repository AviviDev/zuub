<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
foreach($arResult["ITEMS"] as $key=>$arItem)
{
	if(!empty($arItem["PROPERTIES"]["VRACH"]["VALUE"]))
	{
		$elementObj = CIBlockElement::GetByID($arItem["PROPERTIES"]["VRACH"]["VALUE"]);
		if($element = $elementObj->GetNextElement())
		{
			$arItem["ANSWER_DOCTOR"] = $element->GetFields();
			if(empty($arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]))
				$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"] = $arItem["ANSWER_DOCTOR"]["DETAIL_PICTURE"];

			if(!empty($arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]))
			{
				$img = CFile::ResizeImageGet(
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"],
					array("width"=>290, "height"=>600),
					BX_RESIZE_IMAGE_PROPORTIONAL,
					false
				);

				if(!empty($img))
				{
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"] = array();
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["SRC"] = $img["src"];
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
					$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
				}
			}
			$arItem["ANSWER_DOCTOR"]["PROPERTIES"] = $element->GetProperties();
		}
	}
	if(!empty($arItem["ACTIVE_FROM"]))
	{
		$arItem["ACTIVE_FROM"] = array(
			"SHORT"	=> $DB->FormatDate($arItem["ACTIVE_FROM"], CSite::GetDateFormat("FULL"), "YYYY-MM-DD"),
			"RUS"	=> FormatDateFromDB($arItem["ACTIVE_FROM"], 'DD MMMM YYYY')
		);
	}

	$arResult["ITEMS"][$key] = $arItem;
}

?>