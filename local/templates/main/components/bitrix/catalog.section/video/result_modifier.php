<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
global $DB;
foreach($arResult["ITEMS"] as $key => $arItem)
{
	//Обработка изображения. Начало
	if(empty($arItem["PREVIEW_PICTURE"]))
		$arItem["PREVIEW_PICTURE"] = $arItem["DETAIL_PICTURE"];
	if(!empty($arItem["PREVIEW_PICTURE"]))
	{
		$img = CFile::ResizeImageGet(
			$arItem["PREVIEW_PICTURE"]["ID"],
			array("width"=>385, "height"=>236),
			BX_RESIZE_IMAGE_EXACT,
			false
		);
		if(!empty($img))
		{
			$arItem["PREVIEW_PICTURE"]["SRC"] = $img["src"];
			$arItem["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
			$arItem["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
		}
	}
	$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arItem["PREVIEW_PICTURE"];
	//Обработка изображения. Конец

	//Подготовка к выводу даты начала активности. Начало
	$arResult["ITEMS"][$key]["ACTIVE_FROM"] = array(
		"SHORT"	=> $DB->FormatDate($arItem["ACTIVE_FROM"], CSite::GetDateFormat("FULL"), "YYYY-MM-DD"),
		"RUS"	=> FormatDateFromDB($arItem["ACTIVE_FROM"], 'DD MMMM YYYY')
	);
	//Подготовка к выводу даты начала активности. Конец
}

?>