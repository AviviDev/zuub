<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="b-announces flex">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="col-xs-24 col-sm-12 col-md-8">
		<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/video_item.php"), Array("arItem" => $arItem), Array("MODE"=>"php") );?>
	</div>
<?endforeach;?>
</div>
<?if ($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<? echo $arResult["NAV_STRING"];?>
<?endif;?>

<?/*
print("<pre>");
print_r($arResult);
print("</pre>");
*/
?>
