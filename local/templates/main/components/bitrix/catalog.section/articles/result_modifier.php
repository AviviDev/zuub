<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
global $DB;
foreach($arResult["ITEMS"] as $key => $arItem)
{
	if(empty($arItem["~PREVIEW_TEXT"]) && !empty($arItem["~DETAIL_TEXT"]))
	{
		$text = strip_tags($arItem["~DETAIL_TEXT"]);
		$strPos = strpos($text, ".", 150);
		if(intVal($strPos) > 0)
			$arResult["ITEMS"][$key]["~PREVIEW_TEXT"] = substr($text, 0, $strPos+1);
		else
			$arResult["ITEMS"][$key]["~PREVIEW_TEXT"] = $text;
	}
	if($arItem["PROPERTIES"]["ALTER_LINK"]["VALUE"])
	{
		$arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = $arItem["PROPERTIES"]["ALTER_LINK"]["VALUE"];
		$arResult["ITEMS"][$key]["~DETAIL_PAGE_URL"] = $arItem["PROPERTIES"]["ALTER_LINK"]["VALUE"];
	}
	//Обработка изображения. Начало
	if(empty($arItem["PREVIEW_PICTURE"]))
		$arItem["PREVIEW_PICTURE"] = $arItem["DETAIL_PICTURE"];
	if(empty($arItem["DETAIL_PICTURE"]))
		$arItem["DETAIL_PICTURE"] = $arItem["PREVIEW_PICTURE"];

	if(!empty($arItem["PREVIEW_PICTURE"]))
	{
		$img = CFile::ResizeImageGet(
			$arItem["PREVIEW_PICTURE"]["ID"],
			array("width"=>300, "height"=>245),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			false
		);
		if(!empty($img))
		{
			$arItem["PREVIEW_PICTURE"]["SRC"] = $img["src"];
		}
		$img = CFile::ResizeImageGet(
			$arItem["DETAIL_PICTURE"]["ID"],
			array("width"=>800, "height"=>600),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			false
		);
		if(!empty($img))
		{
			$arItem["DETAIL_PICTURE"]["SRC"] = $img["src"];
		}
	}
	$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arItem["PREVIEW_PICTURE"];
	//Обработка изображения. Конец

}
?>