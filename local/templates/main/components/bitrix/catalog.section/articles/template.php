<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<article class="b-announce b-announce__gray clear">
		<?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])):?>
		<a class="b-announce_pic" href="<?=$arItem["~DETAIL_PAGE_URL"]?>">
			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" />
		</a>
		<?endif;?>
		<div class="b-announce_text">

			<h2 class="b-announce_title"><a href="<?=$arItem["~DETAIL_PAGE_URL"]?>"><?=$arItem["~NAME"]?></a></h2>
			<p><?=$arItem["~PREVIEW_TEXT"]?></p>

			<footer class="b-announce_footer">
				<a class="b-announce_footer_more g-solid" href="<?=$arItem["~DETAIL_PAGE_URL"]?>">Подробнее</a>
			</footer>

		</div>

	</article>
<?endforeach;?>

<?if ($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<? echo $arResult["NAV_STRING"];?>
<?endif;?>
<?
/*
print("<pre>");
print_r($arResult);
print("</pre>");
*/
?>
