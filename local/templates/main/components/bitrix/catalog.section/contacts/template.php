<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<section class="b-contacts b-section" id="contacts-tabs">

    <div class="js-tabs-wrapper">
        <div class="js-tabs-page" id="contacts-list">
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                <article class="b-contacts_card <?if($key%2 == 0):?>b-contacts_card__gray <?endif;?>js-spoiler">

                    <header class="b-contacts_card_header">
                        <div class="row">
                            <div class="row-table-lg">
                                <div class="col-xs-24 col-sm-12 col-md-17 col-lg-18 col-lg-vertical-middle">

                                    <div class="row">
                                        <div class="row-table-lg">
                                            <div class="col-xs-24 col-lg-9 col-lg-vertical-middle">

                                                <h2 class="b-contacts_card_header_title i-icon i-location"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?if(!empty($arItem["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"])):?><?=$arItem["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"]?><?else:?><?=$arItem["~NAME"]?><?endif;?></a></h2>

                                            </div>
                                            <div class="col-xs-24 col-lg-8 col-lg-vertical-middle">
                                                <?if(!empty($arItem["DISPLAY_PROPERTIES"]["ADDRESS"]["DISPLAY_VALUE"])):?>
                                                <div class="b-contacts_card_address">
                                                    <p><?=$arItem["DISPLAY_PROPERTIES"]["ADDRESS"]["~VALUE"]?></p>
                                                </div>
                                                <?endif;?>
                                            </div>
                                            <div class="col-xs-24 col-lg-7 col-lg-vertical-middle">
                                                <?if(!empty($arItem["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"])):?>
                                                <ul class="b-contacts_card_metro">
                                                    <?foreach($arItem["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"] as $keyMetro=>$name):
                                                    $color = $arItem["DISPLAY_PROPERTIES"]["HLB_METRO"]["COLOR"][$keyMetro];
                                                    ?>
                                                    <li><span class="e-metro e-metro__<?=$color?> i-icon i-metro"><?=$name?></span></li>
                                                    <?endforeach;?>
                                                </ul>
                                                <?endif;?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-24 col-sm-12 col-md-7 col-lg-6 col-lg-vertical-middle g-right-sm g-right-md g-right-lg">

                                    <a href="/forms/?form_id=1&pushivent=<?=$arParams["PUSHIVENT"]?>" class="b-contacts_card_appointment e-btn e-btn_md e-btn_green e-btn_block_lg js-popup" data-box-width="350">Записаться на прием</a>

                                </div>
                            </div>
                        </div>
                    </header>

                    <div class="b-contacts_card_box js-spoiler-box">

                        <figure class="b-contacts_card_map">
                            <div class="b-contacts_card_map_canvas b-map" id="contacts-map-<?=$key+1?>" data-coordinates="[[<?=$arItem["DISPLAY_PROPERTIES"]["YANDEX_ADDRESS"]["VALUE"]?>]]" data-zoom="16" data-custom-mark="true" data-custom-mark-img="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/placemark.svg"></div>
                        </figure>

                        <div class="b-wysiwyg">

                            <header class="b-heading">
                                <div class="row">
                                    <div class="col-xs-24 col-md-12 col-right g-right-md g-right-lg">

                                        <a class="b-contacts_card_box_clinic" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Перейти на страницу клиники</a>

                                    </div>
                                    <div class="col-xs-24 col-md-12">

                                        <h3>Как добраться</h3>

                                    </div>
                                </div>
                            </header>

                            <div class="b-contacts_card_text">
                                <?=$arItem["~PREVIEW_TEXT"]?>
                            </div>

                        </div>

                    </div>

                    <footer class="b-contacts_card_footer">

                        <button class="b-contacts_card_footer_toggle g-dashed js-spoiler-toggle" type="button" data-closed="Показать" data-opened="Свернуть">Показать</button>

                    </footer>

                    <aside class="b-contacts_map_card" data-coordinates="[[<?=$arItem["DISPLAY_PROPERTIES"]["YANDEX_ADDRESS"]["VALUE"]?>]]" data-id="contacts-map-card-<?=$key+1?>" data-custom-mark="true" data-custom-mark-img="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/placemark.svg">
                        <div class="b-contacts_map_card_wrap">
                            <div class="row">
                                <div class="col-xs-24 col-md-14">

                                    <h2 class="b-contacts_map_card_title i-icon i-location"><?if(!empty($arItem["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"])):?><?=$arItem["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"]?><?else:?><?=$arItem["~NAME"]?><?endif;?></h2>
                                    <p><?=$arItem["DISPLAY_PROPERTIES"]["WORK_TIME"]["DISPLAY_VALUE"]?></p>

                                    <p class="b-contacts_map_card_phone"><a href="tel:+<?=$arItem["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]?>"><?=$arItem["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"]?></a></p>
                                    <p><?=strip_tags($arItem["DISPLAY_PROPERTIES"]["ADDRESS"]["~VALUE"])?></p>

                                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"])):?>
                                        <ul class="b-contacts_map_card_metro">
                                            <?foreach($arItem["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"] as $keyMetro=>$name):
                                                $color = $arItem["DISPLAY_PROPERTIES"]["HLB_METRO"]["COLOR"][$keyMetro];
                                                ?>
                                                <li><span class="e-metro e-metro__<?=$color?> i-icon i-metro"><?=$name?></span></li>
                                            <?endforeach;?>
                                        </ul>
                                    <?endif;?>

                                    <div class="b-contacts_map_card_details">
                                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Перейти на страницу клиники</a>
                                    </div>

                                </div>
                                <div class="col-xs-24 col-md-10">
                                    <?if(!empty($arItem["PREVIEW_PICTURE"])):?>
                                    <a class="b-contacts_map_card_pic" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?if(!empty($arItem["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"])):?><?=$arItem["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"]?><?else:?><?=$arItem["~NAME"]?><?endif;?>" />
                                    </a>
                                    <?endif;?>
                                    <div class="b-contacts_map_card_appointment">
                                        <a href="/forms/?form_id=1&pushivent=<?=$arParams["PUSHIVENT"]?>" class="e-btn e-btn_md e-btn_green e-btn_block_md e-btn_block_lg js-popup" data-box-width="350">Записаться на прием</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </aside>

                </article>
            <?endforeach;?>
        </div>
        <div class="js-tabs-page" id="contacts-map">

            <div class="b-contacts_map">
                <div class="b-contacts_map_canvas b-map" id="contacts-map-general" data-cards=".b-contacts_map_card"></div>
            </div>

        </div>
    </div>

</section>

<?
/*
print("<pre>");
print_r($arResult);
print("</pre>");
*/
?>
