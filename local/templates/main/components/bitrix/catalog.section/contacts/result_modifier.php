<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
if(empty($arParams["PUSHIVENT"]))
	$arParams["PUSHIVENT"] = "zapis_na_priem";

$entity_data_class = GetEntityDataClass(5);
$rsData = $entity_data_class::getList(array(
	'select' => array('*')
));
$metro = array();
while($el = $rsData->fetch())
{
	$metro[$el["UF_XML_ID"]] = $el["UF_COLOR"];
}
foreach($arResult["ITEMS"] as $key => $arItem)
{
	if(!is_array($arItem["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"]))
		$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"] = array($arItem["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"]);
	foreach($arItem["DISPLAY_PROPERTIES"]["HLB_METRO"]["VALUE"] as $keyMetro=>$value)
	{
		$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["HLB_METRO"]["COLOR"][$keyMetro] = $metro[$value];
	}
	//Обработка изображения. Начало
	if(empty($arItem["PREVIEW_PICTURE"]))
		$arItem["PREVIEW_PICTURE"] = $arItem["DETAIL_PICTURE"];
	if(!empty($arItem["PREVIEW_PICTURE"]))
	{
		$img = CFile::ResizeImageGet(
			$arItem["PREVIEW_PICTURE"]["ID"],
			array("width"=>180, "height"=>180),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			false
		);
		if(!empty($img))
		{
			$arItem["PREVIEW_PICTURE"]["SRC"] = $img["src"];
			$arItem["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
			$arItem["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
		}
	}
	$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arItem["PREVIEW_PICTURE"];
	//Определяем стаж работы. Начало
	if(!empty($arItem["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"]))
	{
		preg_match_all("/\d/", $arItem["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"], $m);
		$arItem["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"] = implode ($m[0]);
	}
	$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["PHONE"] = $arItem["DISPLAY_PROPERTIES"]["PHONE"];
}

?>