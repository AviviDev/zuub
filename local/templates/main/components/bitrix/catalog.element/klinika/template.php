<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<section class="b-clinic_general b-section b-section__closed b-section__gray" id="clinic-general">

    <article class="b-clinic_general_info">
        <div class="row">
            <?if(!empty($arResult["PREVIEW_PICTURE"]["SRC"])):?>
			<div class="col-xs-24 col-sm-12 col-md-9 col-lg-9">

                <a class="b-clinic_general_info_pic b-fig js-lightBox" href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" title="<?=$arResult["NAME"]?>">
                    <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>" />
                </a>

            </div>
			<?endif;?>
            <div class="col-xs-24 <?if(!empty($arResult["PREVIEW_PICTURE"]["SRC"])) echo "col-sm-12 col-md-9 col-lg-9"; else echo "col-sm-24 col-md-18 col-lg-18";?>">

                <h2 class="b-clinic_general_info_location i-icon i-location"><?if(!empty($arResult["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"])):?><?=$arResult["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"]?><?else:?><?=$arResult["~NAME"]?><?endif;?></h2>
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["ADDRESS"]["VALUE"])):?><p><?=$arResult["DISPLAY_PROPERTIES"]["ADDRESS"]["~VALUE"]?></p><?endif;?>

                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"])):?>
                    <ul class="b-clinic_general_info_metro">
                        <?foreach($arResult["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"] as $keyMetro=>$name):
                            $color = $arResult["DISPLAY_PROPERTIES"]["HLB_METRO"]["COLOR"][$keyMetro];
                            ?>
                            <li><span class="e-metro e-metro__<?=$color?> i-icon i-metro"><?=$name?></span></li>
                        <?endforeach;?>
                    </ul>
                <?endif;?>

            </div>
            <div class="col-xs-24 col-sm-24 col-md-6 col-lg-6 g-center">

                <a href="/forms/?form_id=1&pushivent=zapis_na_priem_clinica" class="b-clinic_general_info_appointment e-btn e-btn_md e-btn_green e-btn_block_md e-btn_block_lg js-popup" data-box-width="350">Записаться на прием</a>

            </div>
        </div>
    </article>

    <aside class="b-clinic_general_map">
        <div class="b-clinic_general_map_canvas b-map" id="clinic-map" data-coordinates="[[<?=$arResult["DISPLAY_PROPERTIES"]["YANDEX_ADDRESS"]["VALUE"]?>]]" data-zoom="16" data-custom-mark="true" data-custom-mark-img="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/placemark.svg"></div>
    </aside>
    <?if(!empty($arResult["PREVIEW_TEXT"])):?>
    <header class="b-heading">
        <h2 class="b-heading_subtitle">Как добраться</h2>
    </header>

    <article class="b-clinic_general_text b-wysiwyg">

        <?=$arResult["~PREVIEW_TEXT"]?>

    </article>
    <?endif;?>
</section>
<?if(!empty($arResult["DETAIL_TEXT"])):?>
<section class="b-clinic_about b-section b-section__closed" id="clinic-about">

    <?=$arResult["~DETAIL_TEXT"]?>

</section>
<?endif;?>
<?if(!empty($arResult["MORE_PHOTO"])):?>
<section class="b-clinic_gallery b-section b-section__closed b-section__gray" id="clinic-gallery">

    <header class="b-heading">
        <h2 class="b-heading_subtitle">Фото клиники</h2>
    </header>

    <div class="b-dentist_certificates b-gallery_carousel b-carousel" id="certificates-carousel" data-auto-height="false" data-nav="true" data-nav-container="#certificates-carousel-controls" data-dots="true" data-dots-container="#certificates-carousel-controls" data-dots-each="true" data-auto="false" data-interval="10" data-margin="4" data-xs="1" data-sm="2" data-md="3" data-lg="3">
        <?foreach($arResult["MORE_PHOTO"] as $key=>$photo):?>
        <div class="b-carousel_item">

            <a class="b-fig js-lightBox" href="<?=$photo["BIG_SRC"]?>" title="<?=$photo["DESCRIPTION"]?>" data-group="photoclinic">
                <img src="<?=$photo["SMALL_SRC"]?>" alt="<?=$photo["DESCRIPTION"]?>" />
            </a>

        </div>
        <?endforeach;?>
    </div>

    <footer class="b-dentist_certificates_footer b-gallery_carousel_footer">

        <div class="b-carousel_controls" id="certificates-carousel-controls"></div>

    </footer>

</section>
<?endif?>
<?if(!empty($arResult["DISPLAY_PROPERTIES"]["DOCTORS"])):?>
<section class="b-clinic_dentists b-section b-section__closed b-section__last" id="clinic-dentists">

    <header class="b-heading">
        <h2 class="b-heading_subtitle">Врачи</h2>
    </header>
    <?$isGray = true;
    foreach($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["DISPLAY_VALUE"] as $key=>$arItem)
    {
        $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/doctor_list_item.php"), Array("arItem" => $arItem, "isGray" => $isGray), Array("MODE"=>"php") );
        $isGray = !$isGray;
    }
    ?>

</section>
<?endif;?>
