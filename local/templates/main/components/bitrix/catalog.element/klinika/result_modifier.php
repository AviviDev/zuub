<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
//Обработка изображения. Начало
if(!empty($arResult["DETAIL_PICTURE"]))
    $arResult["PREVIEW_PICTURE"] = $arResult["DETAIL_PICTURE"];
else
    $arResult["DETAIL_PICTURE"] = $arResult["PREVIEW_PICTURE"];
if(!empty($arResult["PREVIEW_PICTURE"]))
{
    $img = CFile::ResizeImageGet(
        $arResult["PREVIEW_PICTURE"]["ID"],
        array("width"=>180, "height"=>180),
        BX_RESIZE_IMAGE_EXACT,
        true
    );
    if(!empty($img))
    {
        $arResult["PREVIEW_PICTURE"]["SRC"] = $img["src"];
        $arResult["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
        $arResult["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
    }
}
$arResult["PREVIEW_PICTURE"] = $arResult["PREVIEW_PICTURE"];
//Обработка изображения. Конец
$entity_data_class = GetEntityDataClass(5);
$rsData = $entity_data_class::getList(array(
    'select' => array('*')
));
$metro = array();
while($el = $rsData->fetch())
{
    $metro[$el["UF_XML_ID"]] = $el["UF_COLOR"];
}
if(!is_array($arResult["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"]))
    $arResult["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"] = array($arResult["DISPLAY_PROPERTIES"]["HLB_METRO"]["DISPLAY_VALUE"]);
foreach($arResult["DISPLAY_PROPERTIES"]["HLB_METRO"]["VALUE"] as $keyMetro=>$value)
{
    $arResult["DISPLAY_PROPERTIES"]["HLB_METRO"]["COLOR"][$keyMetro] = $metro[$value];
}
unset($arResult["MORE_PHOTO"]);
foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $key=>$value)
{
    $img = CFile::ResizeImageGet(
        $value,
        array("width"=>264, "height"=>146),
        BX_RESIZE_IMAGE_EXACT,
        false
    );
    $imgBig = CFile::ResizeImageGet(
        $value,
        array("width"=>800, "height"=>600),
        BX_RESIZE_IMAGE_EXACT,
        false
    );
    if(!empty($img))
    {
        $arResult["MORE_PHOTO"][] = array(
            "BIG_SRC" => $imgBig["src"],
            "SMALL_SRC" => $img["src"],
            "DESCRIPTION" => (!empty($arResult["PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$key]))?$arResult["PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$key]:$arResult["NAME"]
        );
    }
}




//Получаем количество Врачей для данной услуги и подготавливаем две карточки
unset($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]);
$arResult["DISPLAY_PROPERTIES"]["DOCTORS"] = array();
if(!empty($arResult["PROPERTIES"]["HLB_KLINIKI"]["VALUE"]))
{
    $arResult["DISPLAY_PROPERTIES"]["DOCTORS"] = ZuubItems::GetDoctors(array("PROPERTY_HLB_KLINIKI" => $arResult["PROPERTIES"]["HLB_KLINIKI"]["VALUE"]), array(), array("SECOND_NAME", "FIRST_NAME", "LAST_NAME"));
}

?>

