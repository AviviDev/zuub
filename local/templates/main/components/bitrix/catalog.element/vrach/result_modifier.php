<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
//��������� �����������. ������
if(!empty($arResult["DETAIL_PICTURE"]))
    $arResult["PREVIEW_PICTURE"] = $arResult["DETAIL_PICTURE"];
else
    $arResult["DETAIL_PICTURE"] = $arResult["PREVIEW_PICTURE"];
if(!empty($arResult["PREVIEW_PICTURE"]))
{
    $img = CFile::ResizeImageGet(
        $arResult["PREVIEW_PICTURE"]["ID"],
        array("width"=>308, "height"=>436),
        BX_RESIZE_IMAGE_EXACT,
        true
    );
    if(!empty($img))
    {
        $arResult["PREVIEW_PICTURE"]["SRC"] = $img["src"];
        $arResult["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
        $arResult["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
    }
}
$arResult["PREVIEW_PICTURE"] = $arResult["PREVIEW_PICTURE"];
//��������� �����������. �����

//���������� ���� ������. ������
$year = 0;
if(!empty($arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))
    $year = (time() - MakeTimeStamp($arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))/(365*86400);
$arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"] = intVal($year);
//���������� ���� ������. �����

//���������� ������ ������������� �����. ������
$specializatsiya = "";
if(!empty($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]) &&
    !is_array($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]))
    $arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] = array($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]);
foreach($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] as $keyProf=>$profession)
{
    if($keyProf > 0) $specializatsiya .= ", ";
    $specializatsiya .= strtolower($profession);
}

$arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] = ucfirst_utf8(strtolower($specializatsiya));
//���������� ������ ������������� �����. �����

//���������� �������� ������� ��� �����. ������
if(!empty($arResult["PROPERTIES"]["HLB_KLINIKI"]["VALUE"]))
{
    $arResult["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"] = array();
    $elObj = CIBlockElement::GetList(
        array("SORT" => "ASC"),
        array("PROPERTY_HLB_KLINIKI" => $arResult["PROPERTIES"]["HLB_KLINIKI"]["VALUE"], "IBLOCK_ID" => 8),
        false,
        false,
        array("ID", "NAME", "PROPERTY_SHORT_NAME", "DETAIL_PAGE_URL")
    );
    while($element = $elObj->GetNext())
    {
        /*if(!empty($arResult["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"]))
            $arResult["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"].=" / ";*/
        $arResult["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"][] = (!empty($element["PROPERTY_SHORT_NAME_VALUE"]))?$element["PROPERTY_SHORT_NAME_VALUE"]:$element["NAME"];
    }
}

//���������� �������� ������� ��� �����. �����


unset($arResult["DISPLAY_PROPERTIES"]["RABOTI"]);
$arResult["DISPLAY_PROPERTIES"]["RABOTI"] = array();
if(!empty($arResult["PROPERTIES"]["HLB_VRACHI"]["VALUE"]))
{
    $elementsObj = CIBlockElement::GetList(
        array("SORT"=>"ASC"),
        array("ACTIVE" => "Y", "IBLOCK_ID" => 16, "PROPERTY_HLB_VRACHI" => $arResult["PROPERTIES"]["HLB_VRACHI"]["VALUE"], "!PROPERTY_NOT_SHOW_IN_LIST" => 11),
        false,
        array("nTopCount" => 5)
    );
    while($element = $elementsObj->GetNext())
    {
        //��������� �����������. ������
        if(!empty($element["PREVIEW_PICTURE"]))
        {
            $img = CFile::ResizeImageGet(
                $element["PREVIEW_PICTURE"],
                array("width"=>340, "height"=>254),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            $element["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
        }

        if(!empty($element["DETAIL_PICTURE"]))
        {
            $img = CFile::ResizeImageGet(
                $element["DETAIL_PICTURE"],
                array("width"=>340, "height"=>254),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            $element["DETAIL_PICTURE"] = array_change_key_case($img, CASE_UPPER);
        }
        $arResult["DISPLAY_PROPERTIES"]["RABOTI"]["DISPLAY_VALUE"][$element["ID"]] = $element;
    }
}

//�������� ���������� ������� ��� ������ ������ � �������������� ��� ��� ������
unset($arResult["DISPLAY_PROPERTIES"]["OTZYVI"]);
$arResult["DISPLAY_PROPERTIES"]["OTZYVI"] = array();
if(!empty($arResult["PROPERTIES"]["HLB_VRACHI"]["VALUE"]))
{
    $arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"] = "#";
    $iblockObj = CIBlock::GetByID(BX_INFOBLOCK_OTZYV);
    if($iblock = $iblockObj->GetNext())
        $arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"] = $iblock["~LIST_PAGE_URL"];
    $elementsObj = CIBlockElement::GetList(
        array("DATE_ACTIVE_FROM"=>"DESC"),
        array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_OTZYV, "PROPERTY_HLB_VRACHI" => $arResult["PROPERTIES"]["HLB_VRACHI"]["VALUE"])
    );
    $elementsObj->NavStart(2);
    $arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"] = intVal($elementsObj->SelectedRowsCount());
    $curCount = 0;
    while($element = $elementsObj->GetNext())
    {
        $curCount++;

        if(!empty($element["ACTIVE_FROM"]))
        {
            $element["ACTIVE_FROM"] = array(
                "SHORT"	=> $DB->FormatDate($element["ACTIVE_FROM"], CSite::GetDateFormat("FULL"), "YYYY-MM-DD"),
                "RUS"	=> FormatDateFromDB($element["ACTIVE_FROM"], 'DD MMMM YYYY')
            );
        }

        $arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["DISPLAY_VALUE"][$element["ID"]] = $element;
        if($curCount > 2) break;
    }
}

if(!empty($arResult["PROPERTIES"]["CERTIFICATIONS"]["VALUE"]))
{
    foreach($arResult["PROPERTIES"]["CERTIFICATIONS"]["VALUE"] as $keyPhoto=>$photo)
    {
        $img = CFile::ResizeImageGet(
            $photo,
            array("width"=>528, "height"=>292),
            BX_RESIZE_IMAGE_EXACT,
            false
        );
        $imgBig = CFile::ResizeImageGet(
            $photo,
            array("width"=>800, "height"=>600),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            false
        );
        $arResult["PROPERTIES"]["CERTIFICATIONS"]["VALUE"][$keyPhoto] = array(
            "SMALL" => array_change_key_case($img, CASE_UPPER),
            "BIG" => array_change_key_case($imgBig, CASE_UPPER),
            "DESCRIPTION" => $arResult["PROPERTIES"]["CERTIFICATIONS"]["~DESCRIPTION"][$keyPhoto]
        );
    }
}
if(!empty($arResult["DISPLAY_PROPERTIES"]["EDUCATION"]["DISPLAY_VALUE"]) && !is_array($arResult["DISPLAY_PROPERTIES"]["EDUCATION"]["DISPLAY_VALUE"]))
    $arResult["DISPLAY_PROPERTIES"]["EDUCATION"]["DISPLAY_VALUE"] = array($arResult["DISPLAY_PROPERTIES"]["EDUCATION"]["DISPLAY_VALUE"]);
?>