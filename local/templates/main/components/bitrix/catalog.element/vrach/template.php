<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$isGray = true;
?>
<section class="b-section b-section__closed b-section__gray">

	<article class="b-dentist_profile">
		<div class="row">
			<div class="col-xs-24 col-md-8 col-lg-9">

				<div class="b-dentist_profile_pic">
					<a class="b-dentist_profile_pic_capsule js-lightBox" href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>">

						<img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arResult["PREVIEW_PICTURE"]["ALT"]?>">

					</a>
				</div>

			</div>
			<div class="col-xs-24 col-md-16 col-lg-15">

				<header class="b-dentist_profile_header">
					<h1 class="b-dentist_profile_header_name"><?=$arResult["DISPLAY_PROPERTIES"]["LAST_NAME"]["DISPLAY_VALUE"]?> <?=$arResult["DISPLAY_PROPERTIES"]["FIRST_NAME"]["DISPLAY_VALUE"]?><br class="hidden-xs hidden-sm" /><?=$arResult["DISPLAY_PROPERTIES"]["SECOND_NAME"]["DISPLAY_VALUE"]?></h1>
				</header>

				<ul class="b-dentist_profile_list">
					<?if(!empty($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"])):?>
						<li>Специализация:&nbsp;<span><?=$arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]?></span></li>
					<?endif;?>
					<?if(!empty($arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"])):?>
						<li>Опыт:&nbsp;<span><?=$arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]?> лет</span></li>
					<?endif;?>
				</ul>
				<?foreach($arResult["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"] as $klinika):?>
					<div class="b-dentist_profile_clinic i-icon i-location"><?=$klinika?></div>
				<?endforeach;?>
				<?if(!empty($arResult["DISPLAY_PROPERTIES"]["EDUCATION"]["DISPLAY_VALUE"])):?>
					<div class="b-dentist_profile_text b-wysiwyg">

						<h6>Образование:</h6>
						<?foreach($arResult["DISPLAY_PROPERTIES"]["EDUCATION"]["DISPLAY_VALUE"] as $value):?>
							<p><?=$value?></p>
						<?endforeach;?>

					</div>
				<?endif;?>
				<footer class="b-dentist_profile_footer">
					<div class="row">
						<div class="col-xs-16 col-xs-offset-4 col-md-24 col-md-offset-0">

							<a href="/forms/?form_id=1&pushivent=zapis_na_priem" class="b-dentist_appointment e-btn e-btn_md e-btn_green e-btn_block js-popup" data-box-width="350">Записаться на прием</a>

						</div>
						<div class="col-xs-16 col-xs-offset-4 col-md-24 col-md-offset-0">

							<a class="b-service_works_footer_btn e-btn e-btn_md e-btn_green_outline e-btn_block js-popup" href="/forms/ask/">Задать вопрос</a>

						</div>
					</div>
				</footer>

			</div>
		</div>
	</article>
</section>
<?$isGray = !$isGray;
if(!empty($arResult["DISPLAY_PROPERTIES"]["COURSES"]["DISPLAY_VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["SKILLS"]["DISPLAY_VALUE"])):?>
<section class="b-section b-section__closed <?if($isGray):?>b-section__gray"<?endif;?>">

  <article class="b-dentist_qualification b-wysiwyg">
	  <div class="row">
	  <?$colMd12=false;
	  if(!empty($arResult["DISPLAY_PROPERTIES"]["SKILLS"]["DISPLAY_VALUE"])):
		if(!empty($arResult["DISPLAY_PROPERTIES"]["COURSES"]["DISPLAY_VALUE"]))
			$colMd12=true;
	  ?>
		  <div class="col-xs-24 <?if($colMd12):?>col-md-12<?endif;?>">

			  <h4>Специализация и профессиональные навыки</h4>

			  <ul>
				  <?foreach($arResult["DISPLAY_PROPERTIES"]["SKILLS"]["DISPLAY_VALUE"] as $value):?>
				  <li><?=$value?></li>
				  <?endforeach;?>
			  </ul>

		  </div>
	<?endif;
	$colMd12=false;
	 if(!empty($arResult["DISPLAY_PROPERTIES"]["COURSES"]["DISPLAY_VALUE"])):
		if(!empty($arResult["DISPLAY_PROPERTIES"]["SKILLS"]["DISPLAY_VALUE"]))
			$colMd12=true;
	?>
		  <div class="col-xs-24 col-md-12">

			  <h4>Курсы повышения квалификации</h4>

			  <ul>
				  <?foreach($arResult["DISPLAY_PROPERTIES"]["COURSES"]["DISPLAY_VALUE"] as $value):?>
				  <li><?=$value?></li>
				  <?endforeach;?>
			  </ul>

		  </div>
	<?endif;?>
	  </div>
  </article>

</section>
<?endif;
$isGray = !$isGray;
if(!empty($arResult["PROPERTIES"]["CERTIFICATIONS"]["VALUE"])):
?>
<section class="b-section b-section__closed <?if($isGray):?>b-section__gray"<?endif;?>">

  <header class="b-heading">
	  <h2 class="b-heading_title">Сертификаты</h2>
  </header>

  <div class="b-dentist_certificates b-gallery_carousel b-carousel" id="certificates-carousel" data-auto-height="false" data-nav="true" data-nav-container="#certificates-carousel-controls" data-dots="true" data-dots-container="#certificates-carousel-controls" data-dots-each="true" data-auto="false" data-interval="10" data-margin="4" data-xs="1" data-sm="2" data-md="3" data-lg="3">
	  <?foreach($arResult["PROPERTIES"]["CERTIFICATIONS"]["VALUE"] as $photo):?>
	  <div class="b-carousel_item">

		  <a class="b-fig js-lightBox" href="<?=$photo["BIG"]["SRC"]?>" title="<?=$photo["DESCRIPTION"]?>" data-group="certificates">
			  <img src="<?=$photo["SMALL"]["SRC"]?>" alt="<?=$photo["DESCRIPTION"]?>" />
		  </a>

	  </div>
	  <?endforeach;?>
  </div>
<?if(count($arResult["PROPERTIES"]["CERTIFICATIONS"]["VALUE"]) > 1):?>
  <footer class="b-dentist_certificates_footer b-gallery_carousel_footer">

	  <div class="b-carousel_controls" id="certificates-carousel-controls"></div>

  </footer>
<?endif;?>
</section>
<?
$isGray = !$isGray;
endif;?>
<?if(!empty($arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["DISPLAY_VALUE"])):?>
	<section class="b-service_faq b-section b-section__closed<?if($isGray):?>  b-section__gray<?endif;?>" id="doctors-reviews">
		<header class="b-heading">
			<h2 class="b-heading_subtitle">Отзывы</h2>
		</header>

		<div class="b-service_faq_list" id="service-faq-list">
			<?foreach($arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["DISPLAY_VALUE"] as $arItem):?>
				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/review_item.php"), Array("arItem" => $arItem, "isGray" => $isGray), Array("MODE"=>"php") );?>
			<?endforeach;?>
		</div>

		<footer class="b-service_faq_footer">
			<div class="row">
				<div class="row-table">
					<div class="col-xs-10 col-md-12 col-vertical-middle">

						<div class="b-section_footer">

							<?if($arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"] > 2):?>
								<a class="g-dashed js-loading-on-require" href="/ajax/review.php?vrachi_list=<?=implode(",",$arResult["PROPERTIES"]["HLB_VRACHI"]["VALUE"])?>&count=<?=$arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"]?>" data-loading-target="#service-faq-list" data-method="get" data-page="3">Показать еще</a>
							<?else:?>
								<a class="g-dashed js-loading-on-require" data-loading-target="#service-faq-list" href="<?=$arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"]?>">Читать все отзывы</a>
							<?endif;?>

						</div>

					</div>
					<div class="col-xs-14 col-md-12 col-vertical-middle g-right">

						<a class="e-btn e-btn_md e-btn_green_outline js-popup" href="/forms/review/?doctor_xml_id=<?=implode(",",$arResult["PROPERTIES"]["HLB_VRACHI"]["VALUE"])?>">Оставить отзыв</a>

					</div>
				</div>
			</div>
		</footer>

	</section>
	<?
	$isGray = !$isGray;
endif;?>

