<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
//��������� �����������. ������
if(empty($arResult["PREVIEW_PICTURE"]))
    $arResult["PREVIEW_PICTURE"] = $arResult["DETAIL_PICTURE"];
if(!empty($arResult["PREVIEW_PICTURE"]))
{
    $img = CFile::ResizeImageGet(
        $arResult["PREVIEW_PICTURE"]["ID"],
        array("width"=>600, "height"=>1000),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );
    if(!empty($img))
    {
        $arResult["PREVIEW_PICTURE"]["SRC"] = $img["src"];
        $arResult["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
        $arResult["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
    }
}
$arResult["PREVIEW_PICTURE"] = $arResult["PREVIEW_PICTURE"];
//��������� �����������. �����

//���������� ���� ������. ������
$year = 0;
if(!empty($arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))
    $year = (time() - MakeTimeStamp($arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))/(365*86400);
$arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"] = intVal($year);
//���������� ���� ������. �����

//���������� ������ ������������� �����. ������
$specializatsiya = "";
if(!empty($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]) &&
    !is_array($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]))
    $arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] = array($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]);
foreach($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] as $keyProf=>$profession)
{
    if($keyProf > 0) $specializatsiya .= ", ";
    $specializatsiya .= strtolower($profession);
}
$arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] = ucfirst_utf8(strtolower($specializatsiya));
//���������� ������ ������������� �����. �����

//���������� ������ ��������� �����. ������
if(is_array($arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"]))
    $arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"] = array_shift($arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"]);
$arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"] = ucfirst_utf8(strtolower($arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"]));
//���������� ������ ��������� �����. �����

//���������� �������� ������� ��� �����. ������
if(!empty($arResult["PROPERTIES"]["KLINIKI"]["VALUE"]))
{
    $elObj = CIBlockElement::GetList(
        array("SORT" => "ASC"),
        array("ID" => $arResult["PROPERTIES"]["KLINIKI"]["VALUE"]),
        false,
        array("nTopCount" => 1),
        array("ID", "NAME", "PROPERTY_SHORT_NAME", "DETAIL_PAGE_URL")
    );
    if($element = $elObj->GetNext())
    {
        $arResult["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"] = (!empty($element["PROPERTY_SHORT_NAME_VALUE"]))?$element["PROPERTY_SHORT_NAME_VALUE"]:$element["NAME"];
    }
}

//���������� �������� ������� ��� �����. �����
?>