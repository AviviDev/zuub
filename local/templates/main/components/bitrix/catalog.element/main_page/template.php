<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<?if(!empty($arResult["DISPLAY_PROPERTIES"]["SLIDER"])):?>
	<section class="b-mp-promo b-section" id="mp-promo">

		<div class="b-mp-promo_carousel b-carousel" id="mp-promo-carousel" data-resolutions="md" data-nav="true" data-dots="true" data-dots-each="true" data-auto="false" data-interval="10" data-margin="30" data-xs="1" data-sm="1" data-md="1" data-lg="1">
			<?foreach($arResult["DISPLAY_PROPERTIES"]["SLIDER"]["DISPLAY_VALUE"] as $slider):?>
			<div class="b-carousel_item">

				<div class="b-mp-promo_slide b-mp-promo_slide__album">
					<a class="b-mp-promo_slide_wrap" href="<?=$slider["PROPERTY_LINK_VALUE"]?>">

						<img src="<?=$slider["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$slider["~NAME"]?>&hellip;" />

					    <span class="b-mp-promo_slide_text">
							<?=$slider["~PREVIEW_TEXT"]?>
						</span>

					</a>
				</div>

			</div>
			<?endforeach;?>
		</div>

	</section>
<?endif;?>

<?
$isGray = false;
if(!empty($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["DISPLAY_VALUE"])):
	$arAnchors["service-dentists"]["SHOW"] = true;
	?>
	<section class="b-service_dentists b-section b-section__closed<?if($isGray):?> b-section__gray<?endif;?>" id="service-dentists">

		<header class="b-heading">
			<div class="row">
				<div class="row-table">
					<div class="col-xs-12 col-md-8 col-vertical-middle">

						<h2 class="b-heading_subtitle">Врачи</h2>

					</div>
					<div class="col-xs-12 col-md-4 col-vertical-middle hidden-xs hidden-sm g-right">

						<a href="<?=$arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["LINK"]?>" class="b-heading_more i-icon i-pages-dentists"><span>Все врачи</span></a>

					</div>
				</div>
			</div>
		</header>

		<div class="b-service_dentists_carousel b-carousel" id="service-dentists-carousel" data-auto-height="false" data-nav="true" data-nav-container="#service-dentists-carousel-controls" data-dots="true" data-dots-container="#service-dentists-carousel-controls" data-dots-each="false" data-auto="false" data-interval="10" data-margin="28" data-xs="2" data-sm="2" data-md="3" data-lg="4">
			<?foreach($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["DISPLAY_VALUE"] as $arItem):?>
				<div class="b-carousel_item">

					<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/doctor_item.php"), Array("arItem" => $arItem), Array("MODE"=>"php") );?>

				</div>
			<?endforeach?>
		</div>
		<?if(count($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["DISPLAY_VALUE"]) > 1):?>
			<footer class="b-service_dentists_carousel_footer ">

				<div class="b-carousel_controls" id="service-dentists-carousel-controls"></div>

			</footer>
		<?endif;?>
	</section>
	<?
	$isGray = !$isGray;
endif;?>

<!-- Main page :: pays -->
<section class="b-mp-pays b-section__closed b-section__gray" id="mp-pays">
	<header class="b-heading">
		<h2 class="b-heading_title">Оплачивайте услуги стоматологии так, как удобно для вас</h2>
	</header>

	<div class="b-mp-pays-box ">
		<article class="b-mp_ras container">
			<div class="row-table">
				<div class="b-mp_ras_img col-lg-10 col-md-8 col-sm-10 hidden-xs col-vertical-middle">
					<img src="<?=SITE_TEMPLATE_PATH?>/include_areas/temp/sand_clock.jpg">
				</div>

				<div class="b-mp_ras_text col-lg-14 col-md-12 col-sm-14 col-xs-24 col-vertical-middle">
					<h3 class="h3-uppercase">Стоматология в рассрочку!</h3>
					<p>Вы можете оплатить услуги по выставленным счетам клиники как наличными, так и банковскими картами</p>
					<p><b>Все подробности по оплате и лечению в рассрочку Вам расскажут по телефону:</b></p>

					<div class="row b-mp_ras_phone-box">
						<div class="row-table-lg row-table-md">
							<div class="col-lg-12  b-header_phone in_v_middle col-vertical-middle">

								<a href="tel:+74951351064">+7 (495) 135-10-64</a>

							</div>
							<div class="col-lg-12 col-vertical-middle b-mp_ras_send-button">
								<a href="/forms/?form_id=1&pushivent=zapis_na_priem" class="b-contacts_card_appointment e-btn e-btn_md e-btn_green e-btn_block_lg js-popup" data-box-width="350">Записаться на прием</a>
							</div>
						</div>
					</div>

				</div>
			</div>
		</article>

		<article class="b-mp_pay-system row">
			<div class="row-table-lg row-table-md row-table-sm">
				<div class="col-lg-17 col-md-17 col-sm-16 col-xs-24 col-vertical-middle b-mp_pay-system b-mp_pay-system_text">
					<h3 class="h3-uppercase">Мы принимаем к оплате банковские карты</h3>
					<p>Вы можете оплатить услуги по выставленным счетам клиники как наличными, так и банковскими картами</p>
				</div>

				<div class="col-lg-7 col-md-7 col-sm-8 col-xs-24 col-vertical-middle in_v_middle b-price_payment_logotypes ">


					<img src="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/visa.png" alt="Visa" height="24">
					<img src="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/mastercard.png" alt="Mastercard" height="41">


				</div>
			</div>
		</article>

	</div>

</section>
<!-- Main page :: pays :: end -->
<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/service_benefits.php"), Array(), Array("MODE"=>"php") );?>
<?if(!empty($arResult["DETAIL_TEXT"]) || !empty($arResult["PREVIEW_TEXT"])):?>
<section class="b-mp-about b-section__closed" id="mp-about">

	<?if(!empty($arResult["DETAIL_TEXT"])):?>
	<header class="b-heading">
		<!--<h2 class="b-heading_title">Почему пациенты выбирают нас</h2>-->
	</header>

	<div class="js-pocket faded active" data-resolutions="xs,sm,md,lg" data-lines="5,6,3,3">

		<div class="js-pocket-box">
			<div class="js-pocket-box-inner">

				<article class="b-wysiwyg">

					<?=$arResult["DETAIL_TEXT"]?>

				</article>

			</div>
		</div>

		<footer class="b-section_footer">

			<span class="js-pocket-toggle" data-opened="Свернуть" data-closed="Читать дальше">Читать дальше</span>

		</footer>

	</div>
	<?endif;?>
	<?=$arResult["~PREVIEW_TEXT"]?>

</section>
<?endif;?>
<section class="b-mp-clinics b-section__closed b-section__last b-section__gray js-tabs js-init" id="mp-clinics">
<div class="js-tabs">
	<header class="b-heading">
		<div class="row">
			<div class="row-table">
				<div class="col-xs-12 col-sm-10 col-md-16 col-vertical-middle">

					<h2 class="b-heading_title">Отделения</h2>

				</div>
				<div class="col-xs-12 col-sm-14 col-md-8 col-vertical-middle g-right">

					<div class="b-contacts_tabs js-tabs-toggle">
						<a href="#contacts-list" class="i-icon i-view-list current"><span class="hidden-xs">Списком</span></a>
						<a href="#contacts-map" class="i-icon i-view-map"><span class="hidden-xs">На карте</span></a>
					</div>

				</div>
			</div>
		</div>
	</header>
<?
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"contacts",
	array(
		"PUSHIVENT" => "zapis_na_priem_otdeleniya_home",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "8",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"PAGE_ELEMENT_COUNT" => "10",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => array(
			0 => "HLB_METRO",
			1 => "ADDRESS",
			2 => "SHORT_NAME",
			3 => "YANDEX_ADDRESS",
			4 => "WORK_TIME"
		),
		"OFFERS_LIMIT" => "5",
		"BACKGROUND_IMAGE" => "-",
		"TEMPLATE_THEME" => "blue",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_URL" => "",
		"DETAIL_URL" => "/#ELEMENT_CODE#.html",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "N",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Страницы",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"FILE_404" => ""
	),
	false
);?>
</div>
</section>