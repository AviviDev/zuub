<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
//��������� �����������. ������
if(empty($arResult["PREVIEW_PICTURE"]))
    $arResult["PREVIEW_PICTURE"] = $arResult["DETAIL_PICTURE"];
if(!empty($arResult["PREVIEW_PICTURE"]))
{
    $img = CFile::ResizeImageGet(
        $arResult["PREVIEW_PICTURE"]["ID"],
        array("width"=>600, "height"=>1000),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );
    if(!empty($img))
    {
        $arResult["PREVIEW_PICTURE"]["SRC"] = $img["src"];
        $arResult["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
        $arResult["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
    }
}
$arResult["PREVIEW_PICTURE"] = $arResult["PREVIEW_PICTURE"];
//��������� �����������. �����

//���������� ���� ������. ������
$year = 0;
if(!empty($arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))
    $year = (time() - MakeTimeStamp($arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))/(365*86400);
$arResult["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"] = intVal($year);
//���������� ���� ������. �����

//���������� ������ ������������� �����. ������
$specializatsiya = "";
if(!empty($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]) &&
    !is_array($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]))
    $arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] = array($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]);
foreach($arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] as $keyProf=>$profession)
{
    if($keyProf > 0) $specializatsiya .= ", ";
    $specializatsiya .= strtolower($profession);
}
$arResult["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] = ucfirst_utf8(strtolower($specializatsiya));
//���������� ������ ������������� �����. �����

//���������� ������ ��������� �����. ������
if(is_array($arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"]))
    $arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"] = array_shift($arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"]);
$arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"] = ucfirst_utf8(strtolower($arResult["DISPLAY_PROPERTIES"]["HLB_POSITION"]["DISPLAY_VALUE"]));
//���������� ������ ��������� �����. �����

//���������� �������� ������� ��� �����. ������
if(!empty($arResult["PROPERTIES"]["KLINIKI"]["VALUE"]))
{
    $elObj = CIBlockElement::GetList(
        array("SORT" => "ASC"),
        array("ID" => $arResult["PROPERTIES"]["KLINIKI"]["VALUE"]),
        false,
        array("nTopCount" => 1),
        array("ID", "NAME", "PROPERTY_SHORT_NAME", "DETAIL_PAGE_URL")
    );
    if($element = $elObj->GetNext())
    {
        $arResult["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"] = (!empty($element["PROPERTY_SHORT_NAME_VALUE"]))?$element["PROPERTY_SHORT_NAME_VALUE"]:$element["NAME"];
    }
}

//���������� �������� ������� ��� �����. �����

//�������� ���������� ������ ��� ������ ������ � �������������� ��� ��������
unset($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]);
$arResult["DISPLAY_PROPERTIES"]["DOCTORS"] = array();

$arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["LINK"] = "#";
$iblockObj = CIBlock::GetByID(BX_INFOBLOCK_VRACHI);
if($iblock = $iblockObj->GetNext())
    $arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["LINK"] = $iblock["~LIST_PAGE_URL"];
$elementsObj = CIBlockElement::GetList(
    array("SORT"=>"ASC"),
    array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_VRACHI),
    false,
    array("nTopCount" => 10)
);
while($elementObj = $elementsObj->GetNextElement())
{
    $element = $elementObj->GetFields();
    //��������� �����������. ������
    if(empty($element["PREVIEW_PICTURE"]))
        $element["PREVIEW_PICTURE"] = $element["DETAIL_PICTURE"];
    if(!empty($element["PREVIEW_PICTURE"]))
    {
        $img = CFile::ResizeImageGet(
            $element["PREVIEW_PICTURE"],
            array("width"=>308, "height"=>436),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            false
        );
        $element["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
    }
    //���������� ������ ������������� �����. ������
    $hlb_spetsializatsiya_vracha = $elementObj->GetProperty("HLB_SPETSIALIZATSIYA_VRACHA");
    if(!empty($hlb_spetsializatsiya_vracha["VALUE"]))
        $hlb_spetsializatsiya_vracha = CIBlockFormatProperties::GetDisplayValue($element, $hlb_spetsializatsiya_vracha, "catalog_out");
    $specializatsiya = "";
    if(!empty($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"]) &&
        !is_array($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"]))
        $hlb_spetsializatsiya_vracha["DISPLAY_VALUE"] = array($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"]);
    foreach($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"] as $keyProf=>$profession)
    {
        if($keyProf > 0) $specializatsiya .= ", ";
        $specializatsiya .= strtolower($profession);
    }
    $element["HLB_SPETSIALIZATSIYA_VRACHA"] = ucfirst_utf8(strtolower($specializatsiya));
    $arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["DISPLAY_VALUE"][$element["ID"]] = $element;
    //���������� ������ ������������� �����. �����
}

//�������� �������
unset($arResult["DISPLAY_PROPERTIES"]["SLIDER"]);
$arResult["DISPLAY_PROPERTIES"]["SLIDER"] = array();
$elementsObj = CIBlockElement::GetList(
    array("SORT"=>"ASC"),
    array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_SLIDER),
    false,
    array("nTopCount" => 4),
    array("ID", "PREVIEW_PICTURE", "NAME", "PREVIEW_TEXT", "PROPERTY_LINK")
);

while($elementObj = $elementsObj->GetNextElement())
{
    $element = $elementObj->GetFields();
    $element["PROPERTIES"] = $elementObj->GetProperties();
    //��������� �����������. ������
    if(empty($element["PREVIEW_PICTURE"]))
        $element["PREVIEW_PICTURE"] = $element["DETAIL_PICTURE"];
    if(!empty($element["PREVIEW_PICTURE"]))
    {
        $img = CFile::ResizeImageGet(
            $element["PREVIEW_PICTURE"],
            array("width"=>658, "height"=>439),
            BX_RESIZE_IMAGE_EXACT,
            false
        );
        $element["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
    }

    $arResult["DISPLAY_PROPERTIES"]["SLIDER"]["DISPLAY_VALUE"][$element["ID"]] = $element;
    //���������� ������ ������������� �����. �����
}
?>