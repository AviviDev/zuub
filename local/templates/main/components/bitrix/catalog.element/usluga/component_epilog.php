<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;
$arPath = GetElementsPathById($arResult["ID"],'PARENT');
foreach($arPath as $path)
{
    $APPLICATION->AddChainItem($path["NAME"], $path["DETAIL_PAGE_URL"]);
}
?>