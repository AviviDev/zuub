<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
if(empty($arResult["PREVIEW_PICTURE"]))
    $arResult["PREVIEW_PICTURE"] = $arResult["DETAIL_PICTURE"];
if(empty($arResult["DETAIL_PICTURE"]))
    $arResult["DETAIL_PICTURE"] = $arResult["PREVIEW_PICTURE"];

if(!empty($arResult["PREVIEW_PICTURE"]))
{
    $img = CFile::ResizeImageGet(
        $arResult["PREVIEW_PICTURE"]["ID"],
        array("width"=>300, "height"=>245),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        false
    );
    if(!empty($img))
    {
        $arResult["PREVIEW_PICTURE"]["SRC"] = $img["src"];
    }
    $img = CFile::ResizeImageGet(
        $arResult["DETAIL_PICTURE"]["ID"],
        array("width"=>800, "height"=>600),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        false
    );
    if(!empty($img))
    {
        $arResult["DETAIL_PICTURE"]["SRC"] = $img["src"];
    }
}


//�������� ������� ������
unset($arResult["DISPLAY_PROPERTIES"]["RELATED_SERVICES"]);
$arResult["DISPLAY_PROPERTIES"]["RELATED_SERVICES"] = array();
$relatedServices = getCurLevel($arResult["IBLOCK_ID"], $arResult["ID"]);
if(intVal($arResult["PROPERTIES"]["PARENT"]["VALUE"]) > 0 && count($relatedServices) < 1)
{
	$relatedServices = getCurLevel($arResult["IBLOCK_ID"], $arResult["PROPERTIES"]["PARENT"]["VALUE"]);
}
unset($relatedServices[$arResult["ID"]]);
$relatedServices = array_chunk($relatedServices, ceil(count($relatedServices)/3));
$arResult["DISPLAY_PROPERTIES"]["RELATED_SERVICES"]["DISPLAY_VALUE"] = $relatedServices;


//�������� ������
unset($arResult["DISPLAY_PROPERTIES"]["RABOTI"]);
$arResult["DISPLAY_PROPERTIES"]["RABOTI"] = array();
if(!empty($arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]))
{
	$arResult["DISPLAY_PROPERTIES"]["RABOTI"]["LINK"] = "#";
	$iblockObj = CIBlock::GetByID(BX_INFOBLOCK_RABOTI);
	if($iblock = $iblockObj->GetNext())
		$arResult["DISPLAY_PROPERTIES"]["RABOTI"]["LINK"] = $iblock["~LIST_PAGE_URL"];
    $elementsObj = CIBlockElement::GetList(
        array("SORT"=>"ASC"),
        array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_RABOTI, "PROPERTY_HLB_USLUGI" => $arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]),
        false,
        array("nTopCount" => 20)
    );
    while($element = $elementsObj->GetNext())
    {
        //��������� �����������. ������
        if(!empty($element["PREVIEW_PICTURE"]))
        {
            $img = CFile::ResizeImageGet(
                $element["PREVIEW_PICTURE"],
                array("width"=>978, "height"=>588),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            $element["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
        }

        if(!empty($element["DETAIL_PICTURE"]))
        {
            $img = CFile::ResizeImageGet(
                $element["DETAIL_PICTURE"],
                array("width"=>978, "height"=>588),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            $element["DETAIL_PICTURE"] = array_change_key_case($img, CASE_UPPER);
        }
        $arResult["DISPLAY_PROPERTIES"]["RABOTI"]["DISPLAY_VALUE"][$element["ID"]] = $element;
    }
}

//�������� ���������� Video ��� ������ ������ � �������������� ��� ��� ������
unset($arResult["DISPLAY_PROPERTIES"]["VIDEO"]);
$arResult["DISPLAY_PROPERTIES"]["VIDEO"] = array();
if(!empty($arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]))
{
	$arResult["DISPLAY_PROPERTIES"]["VIDEO"]["LINK"] = "#";
	$iblockObj = CIBlock::GetByID(BX_INFOBLOCK_VIDEO);
	if($iblock = $iblockObj->GetNext())
		$arResult["DISPLAY_PROPERTIES"]["VIDEO"]["LINK"] = $iblock["~LIST_PAGE_URL"];
	$elementsObj = CIBlockElement::GetList(
		array("DATE_ACTIVE_FROM"=>"DESC"),
		array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_VIDEO, "PROPERTY_HLB_USLUGI" => $arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]),
		false,
		array("nTopCount" => 20)
	);
	while($element = $elementsObj->GetNext())
	{
		if(!empty($element["PREVIEW_PICTURE"]))
		{
			$img = CFile::ResizeImageGet(
				$element["PREVIEW_PICTURE"],
				array("width"=>268, "height"=>182),
				BX_RESIZE_IMAGE_EXACT,
				false
			);
			$element["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
		}
		$arResult["DISPLAY_PROPERTIES"]["VIDEO"]["DISPLAY_VALUE"][$element["ID"]] = $element;
	}
}

//�������� ���������� ������ ��� ������ ������ � �������������� ��� ��������
unset($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]);
$arResult["DISPLAY_PROPERTIES"]["DOCTORS"] = array();
if(!empty($arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]))
{
	$arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["LINK"] = "#";
	$iblockObj = CIBlock::GetByID(BX_INFOBLOCK_VRACHI);
	if($iblock = $iblockObj->GetNext())
		$arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["LINK"] = $iblock["~LIST_PAGE_URL"];
	$elementsObj = CIBlockElement::GetList(
		array("SORT"=>"ASC"),
		array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_VRACHI, "PROPERTY_HLB_USLUGI" => $arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]),
		false,
		array("nTopCount" => 20)
	);
	while($elementObj = $elementsObj->GetNextElement())
	{
		$element = $elementObj->GetFields();
		//��������� �����������. ������
		if(empty($element["PREVIEW_PICTURE"]))
			$element["PREVIEW_PICTURE"] = $element["DETAIL_PICTURE"];
		if(!empty($element["PREVIEW_PICTURE"]))
		{
			$img = CFile::ResizeImageGet(
				$element["PREVIEW_PICTURE"],
				array("width"=>308, "height"=>436),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				false
			);
			$element["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
		}
		//���������� ������ ������������� �����. ������
		$hlb_spetsializatsiya_vracha = $elementObj->GetProperty("HLB_SPETSIALIZATSIYA_VRACHA");
		if(!empty($hlb_spetsializatsiya_vracha["VALUE"]))
			$hlb_spetsializatsiya_vracha = CIBlockFormatProperties::GetDisplayValue($element, $hlb_spetsializatsiya_vracha, "catalog_out");
		$specializatsiya = "";
		if(!empty($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"]) &&
			!is_array($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"]))
			$hlb_spetsializatsiya_vracha["DISPLAY_VALUE"] = array($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"]);
		foreach($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"] as $keyProf=>$profession)
		{
			if($keyProf > 0) $specializatsiya .= ", ";
			$specializatsiya .= strtolower($profession);
		}
		$element["HLB_SPETSIALIZATSIYA_VRACHA"] = ucfirst_utf8(strtolower($specializatsiya));
		$arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["DISPLAY_VALUE"][$element["ID"]] = $element;
		//���������� ������ ������������� �����. �����
	}
}


//�������� ����
$elementsObj = CIBlockElement::GetList(
	array("SORT"=>"ASC"),
	array(
		"IBLOCK_ID" => BX_INFOBLOCK_PRICE,
		"ACTIVE" => "Y",
		array("PROPERTY_USLUGA" => $arResult["ID"])	
	)
);
unset($arResult["DISPLAY_PROPERTIES"]["PRICES"]);
$arResult["DISPLAY_PROPERTIES"]["PRICES"] = array();
while($elementObj = $elementsObj->GetNextElement())
{
	$element = $elementObj->GetFields();
	$element["PROPERTIES"] = $elementObj->GetProperties();
	$element["PROPERTIES"]["PRICE_OLD"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($element["PROPERTIES"]["PRICE_OLD"]["VALUE"], 0, '', ' '));
	$element["PROPERTIES"]["PRICE"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($element["PROPERTIES"]["PRICE"]["VALUE"], 0, '', ' '));
	$element["PROPERTIES"]["PRICE_TO"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($element["PROPERTIES"]["PRICE_TO"]["VALUE"], 0, '', ' '));


	$elsObj = CIBlockElement::GetList(
		array("SORT"=>"ASC"),
		array(
			"IBLOCK_ID" => BX_INFOBLOCK_PRICE,
			"PROPERTY_INSIDE_PRICE" => $element["ID"],
			"ACTIVE" => "Y"
		)
	);
	while($elObj = $elsObj->GetNextElement())
	{
		$el = $elObj->GetFields();
		$el["PROPERTIES"] = $elObj->GetProperties();
		$el["PROPERTIES"]["PRICE_OLD"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($el["PROPERTIES"]["PRICE_OLD"]["VALUE"], 0, '', ' '));
		$el["PROPERTIES"]["PRICE"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($el["PROPERTIES"]["PRICE"]["VALUE"], 0, '', ' '));
		$el["PROPERTIES"]["PRICE_TO"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($el["PROPERTIES"]["PRICE_TO"]["VALUE"], 0, '', ' '));
		$element["INSIDE_PRICE"][] = $el;
	}
	
	foreach ($element['INSIDE_PRICE'] as $key_price => $arPrices) {
		if ($arPrices['PROPERTIES']['NO_VIEW_IN_SERVICES']['VALUE'] == 'Y'){
			unset($element['INSIDE_PRICE'][$key_price]);
		}			
	}
	$elsObj = CIBlockElement::GetList(
		array("SORT"=>"ASC"),
		array(
			"IBLOCK_ID" => BX_INFOBLOCK_PRICE,
			"PROPERTY_MORE_PRICE" => $element["ID"],
			"ACTIVE" => "Y"
		)
	);
	while($elObj = $elsObj->GetNextElement())
	{
		$el = $elObj->GetFields();
		$el["PROPERTIES"] = $elObj->GetProperties();
		$el["PROPERTIES"]["PRICE_OLD"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($el["PROPERTIES"]["PRICE_OLD"]["VALUE"], 0, '', ' '));
		$el["PROPERTIES"]["PRICE"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($el["PROPERTIES"]["PRICE"]["VALUE"], 0, '', ' '));
		$el["PROPERTIES"]["PRICE_TO"]["VALUE"] = str_replace(" ", "&nbsp;", number_format($el["PROPERTIES"]["PRICE_TO"]["VALUE"], 0, '', ' '));
		$element["MORE_PRICE"][] = $el;
	}
	foreach ($element['MORE_PRICE'] as $key_price => $arPrices) {
		if ($arPrices['PROPERTIES']['NO_VIEW_IN_SERVICES']['VALUE'] == 'Y'){
			unset($element['MORE_PRICE'][$key_price]);
		}			
	}
	$arResult["DISPLAY_PROPERTIES"]["PRICES"]["DISPLAY_VALUE"][$element["ID"]] = $element;
}

//�������� ���������� ��������-������� ��� ������ ������ � �������������� ��� ��� ������
unset($arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]);
$arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"] = array();
if(!empty($arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]))
{
	$arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]["LINK"] = "#";
	$iblockObj = CIBlock::GetByID(BX_INFOBLOCK_VOPROS_OTVET);
	if($iblock = $iblockObj->GetNext())
		$arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]["LINK"] = $iblock["~LIST_PAGE_URL"];
	$elementsObj = CIBlockElement::GetList(
		array("DATE_ACTIVE_FROM"=>"DESC"),
		array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_VOPROS_OTVET, "PROPERTY_HLB_USLUGI" => $arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"])
	);
	$elementsObj->NavStart(2);
	$arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]["COUNT"] = intVal($elementsObj->SelectedRowsCount());
	$curCount = 0;
	while($element = $elementsObj->GetNext())
	{
		$curCount++;

		if(!empty($element["ACTIVE_FROM"]))
		{
			$element["ACTIVE_FROM"] = array(
				"SHORT"	=> $DB->FormatDate($element["ACTIVE_FROM"], CSite::GetDateFormat("FULL"), "YYYY-MM-DD"),
				"RUS"	=> FormatDateFromDB($element["ACTIVE_FROM"], 'DD MMMM YYYY')
			);
		}

		$arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]["DISPLAY_VALUE"][$element["ID"]] = $element;
		if($curCount > 2) break;
	}
}

//�������� ���������� ������� ��� ������ ������ � �������������� ��� ��� ������
unset($arResult["DISPLAY_PROPERTIES"]["OTZYVI"]);
$arResult["DISPLAY_PROPERTIES"]["OTZYVI"] = array();
if(!empty($arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]))
{
	$arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"] = "#";
	$iblockObj = CIBlock::GetByID(BX_INFOBLOCK_OTZYV);
	if($iblock = $iblockObj->GetNext())
		$arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"] = $iblock["~LIST_PAGE_URL"];
	$elementsObj = CIBlockElement::GetList(
		array("DATE_ACTIVE_FROM"=>"DESC"),
		array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_OTZYV, "PROPERTY_HLB_USLUGI" => $arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"])
	);
	$elementsObj->NavStart(2);
	$arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"] = intVal($elementsObj->SelectedRowsCount());
	$curCount = 0;
	while($element = $elementsObj->GetNext())
	{
		$curCount++;

		if(!empty($element["ACTIVE_FROM"]))
		{
			$element["ACTIVE_FROM"] = array(
				"SHORT"	=> $DB->FormatDate($element["ACTIVE_FROM"], CSite::GetDateFormat("FULL"), "YYYY-MM-DD"),
				"RUS"	=> FormatDateFromDB($element["ACTIVE_FROM"], 'DD MMMM YYYY')
			);
		}

		$arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["DISPLAY_VALUE"][$element["ID"]] = $element;
		if($curCount > 2) break;
	}
}


//�������� ���������� ������ ��� ������ ������ � �������������� ��� ������
unset($arResult["DISPLAY_PROPERTIES"]["STATI"]);
$arResult["DISPLAY_PROPERTIES"]["STATI"] = array();
if(!empty($arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]))
{
	$arResult["DISPLAY_PROPERTIES"]["STATI"]["LINK"] = "#";
	$iblockObj = CIBlock::GetByID(BX_INFOBLOCK_STATI);
	if($iblock = $iblockObj->GetNext())
		$arResult["DISPLAY_PROPERTIES"]["STATI"]["LINK"] = $iblock["~LIST_PAGE_URL"];
	$elementsObj = CIBlockElement::GetList(
		array("DATE_ACTIVE_FROM"=>"DESC"),
		array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_STATI, "PROPERTY_HLB_USLUGI" => $arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"]),
		false,
		array("nTopCount" => 5)
	);
	
	while($element = $elementsObj->GetNext())
	{
		//��������� �����������. ������
		if(!empty($element["DETAIL_PICTURE"]))
			$element["PREVIEW_PICTURE"] = $element["DETAIL_PICTURE"];
		if(!empty($element["PREVIEW_PICTURE"]))
		{
			$img = CFile::ResizeImageGet(
				$element["PREVIEW_PICTURE"],
				array("width"=>268, "height"=>182),
				BX_RESIZE_IMAGE_EXACT,
				false
			);
			$element["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
		}

		$arResult["DISPLAY_PROPERTIES"]["STATI"]["DISPLAY_VALUE"][$element["ID"]] = $element;
	}
}
?>
