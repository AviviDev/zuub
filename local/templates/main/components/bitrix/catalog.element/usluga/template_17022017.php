<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arAnchors = array(
	"service-prices" => array("TEXT" => "Цены", "SHOW" => false),
	"service-intro" => array("TEXT" => "Почему мы", "SHOW" => false),
	"service-benefits" => array("TEXT" => "Преимущества", "SHOW" => true),
	"service-works" => array("TEXT" => "Примеры работ", "SHOW" => false),
	"service-video" => array("TEXT" => "Видео", "SHOW" => false),
	"service-dentists" => array("TEXT" => "Врачи", "SHOW" => false),
	"service-reviews" => array("TEXT" => "Отзывы", "SHOW" => false),
	"service-faq" => array("TEXT" => "Вопрос-ответ", "SHOW" => false),
);
?>
<?$isGray = true;?>
<?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICES"]["DISPLAY_VALUE"])):
	$arAnchors["service-prices"]["SHOW"] = true;
	?>
	<section class="b-service_prices b-section b-section__closed<?if($isGray):?> b-section__gray<?endif;?>" id="service-prices">

		<header class="b-heading">
			<h2 class="b-heading_subtitle">Цены</h2>
		</header>
		<?foreach($arResult["DISPLAY_PROPERTIES"]["PRICES"]["DISPLAY_VALUE"] as $price):?>
		<article class="b-price js-spoiler">

			<header class="b-price_heading">
				<div class="row">
					<div class="row-table-md row-table-lg">
						<div class="col-xs-24 col-md-14 col-md-vertical-middle col-lg-vertical-middle">

							<h2 class="b-price_heading_title"><?=$price["~NAME"]?> <button type="button" class="b-price_heading_toggle e-btn i-icon i-chevron-down js-spoiler-toggle"></button></h2>
							
						</div>
						<div class="col-xs-24 col-md-10 col-md-vertical-middle col-lg-vertical-middle g-right-md g-right-lg">

							<div class="b-price_heading_cost">
								<div class="b-price_heading_cost_wrap">
									<?if(!empty($price["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
									<?elseif(!empty($price["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<span class="b-price_heading_cost_strike">до <?=$price["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</span>
									<?elseif(!empty($price["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<s class="b-price_details_main_cost_strike"><?=$price["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
									<?else:?>
										<span class="b-price_details_main_cost_actual"><?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</span>
									<?endif;?>
								</div>
							</div>

						</div>
					</div>
				</div>
			</header>

			<div class="b-price_details js-spoiler-box">

				<!-- Main details -->
				<div class="b-price_details_main">

					<div class="b-price_details_main_text">
						<div class="row">
							<?=$price["~DETAIL_TEXT"]?>
							<div class="col-xs-24 col-md-6 col-lg-6 g-center-xs g-center-sm g-right-md g-right-lg">

								<div class="b-price_details_main_cost">
									<?if(!empty($price["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
									<?elseif(!empty($price["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual">от <?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<span class="b-price_details_main_cost_strike">до <?=$price["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</span>
									<?elseif(!empty($price["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
										<strong class="b-price_details_main_cost_actual"><?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										<s class="b-price_details_main_cost_strike"><?=$price["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
									<?else:?>
										<span class="b-price_details_main_cost_actual"><?=$price["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</span>
									<?endif;?>
								</div>

							</div>
						</div>
					</div>

					<div class="b-price_details_main_footer">
						<div class="row">
							<div class="b-price_details_main_footer_grid row-table-md row-table-lg">
								<div class="col-xs-24 col-md-16 col-md-vertical-middle col-lg-vertical-middle">
									<?=$price["~PREVIEW_TEXT"]?>
								</div>
								<div class="col-xs-24 col-md-8 col-md-vertical-middle col-lg-vertical-middle g-center-xs g-center-sm g-right-md g-right-lg">

									<a href="/forms/?form_id=1&pushivent=zapis_na_priem" class="b-service_prices_details_footer_btn e-btn e-btn_md e-btn_green js-popup" data-box-width="350">Записаться на прием</a>

								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- Main details end -->
				<?if(!empty($price["INSIDE_PRICE"])):?>
				<!-- Additional info -->
				<table class="b-table b-table__rounded b-table__collapsed">

					<tr class="b-table_header">
						<th colspan="2">
							<div class="b-table_padding">Услуги</div>
						</th>
					</tr>
					<?foreach($price["INSIDE_PRICE"] as $arPrice):?>
					<tr>
						<td><?=$arPrice["~NAME"]?></td>
						<td class="g-right-md g-right-lg">
							  <span class="b-table_price b-table_price__lg">
								  <?if(!empty($arPrice["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
									  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
								  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
									  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
									  <i class="b-price_details_main_cost_strike"> <i>до <?=$arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</i>
								  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
									  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
									  <s><?=$arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
								  <?else:?>
									  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
								  <?endif;?>
							  </span>
						</td>
					</tr>
					<?endforeach;?>
				</table>
				<?endif;?>
				<?if(!empty($price["MORE_PRICE"])):?>
					<!-- Additional info -->
					<table class="b-table b-table__rounded b-table__collapsed">

						<tr class="b-table_header">
							<th colspan="2">
								<div class="b-table_padding">Дополнительные услуги</div>
							</th>
						</tr>
						<?foreach($price["MORE_PRICE"] as $arPrice):?>
							<tr>
								<td><?=$arPrice["~NAME"]?></td>
								<td class="g-right-md g-right-lg">
									  <span class="b-table_price b-table_price__lg">
										  <?if(!empty($arPrice["PROPERTIES"]["IS_PRICE_FROM"]["VALUE"])):?>
											  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
										  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"])):?>
										  от <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р</strong>
										  <i class="b-price_details_main_cost_strike"> <i>до <?=$arPrice["PROPERTIES"]["PRICE_TO"]["VALUE"]?>&nbsp;р</i>
											  <?elseif(!empty($arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"])):?>
												  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
												  <s><?=$arPrice["PROPERTIES"]["PRICE_OLD"]["VALUE"]?>&nbsp;р</s>
											  <?else:?>
												  <?=$arPrice["PROPERTIES"]["PRICE"]["VALUE"]?>&nbsp;р
											  <?endif;?>
									  </span>
								</td>
							</tr>
						<?endforeach;?>
					</table>
				<?endif;?>
			</div>
		</article>
		<?endforeach;?>
		<article class="b-price">
			<!-- Payment info -->
			<footer class="b-price_payment">
				<div class="row">
					<div class="row-table-md row-table-lg">

						<div class="col-xs-24 col-md-6 col-lg-7 col-md-vertical-middle col-lg-vertical-middle">

							<div class="b-price_payment_label">

								<p>К оплате принимаются карты</p>

							</div>

						</div>

						<div class="col-xs-24 col-md-11 col-lg-11 col-md-vertical-middle col-lg-vertical-middle">
							<div class="row">
								<div class="row-table">

									<div class="col-xs-13 col-md-13 col-vertical-middle">

										<div class="b-price_payment_logotypes">

											<img src="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/visa.png" alt="Visa" height="24" />
											<img src="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/mastercard.png" alt="Mastercard" height="41" />

										</div>

									</div>

									<div class="col-xs-11 col-md-11 col-vertical-middle">

										<div class="b-price_payment_credit">

											<p>Доступно лечение<br /><a href="/stomatologiya-v-kredit.html">в&nbsp;рассрочку</a></p>

										</div>

									</div>

								</div>
							</div>
						</div>

						<div class="col-xs-24 col-md-7 col-lg-6 g-center-xs g-center-sm g-right-md g-right-lg col-md-vertical-middle col-lg-vertical-middle">

							<a href="/forms/?form_id=1&pushivent=zapis_na_priem" class="e-btn e-btn_md e-btn_green e-btn_block_md e-btn_block_lg js-popup" data-box-width="350">Записаться на прием</a>

						</div>

					</div>
				</div>
			</footer>
			<!-- Payment info end -->

		</article>

	</section>

<?
$isGray = !$isGray;
endif;
if(!empty($arResult["DISPLAY_PROPERTIES"]["RELATED_SERVICES"]["DISPLAY_VALUE"])):?>
<section class="b-service_related b-section js-spoiler b-section__closed<?if($isGray):?> b-section__gray<?endif;?>" id="service-related">

    <header class="b-heading">
        <h2 class="b-heading_subtitle">Смежные услуги</h2>
    </header>

    <div class="b-service_related_group flex">
	<?foreach($arResult["DISPLAY_PROPERTIES"]["RELATED_SERVICES"]["DISPLAY_VALUE"] as $relatedServices):?>
        <div class="col-xs-24 col-md-<?=(24/count($arResult["DISPLAY_PROPERTIES"]["RELATED_SERVICES"]["DISPLAY_VALUE"]))?>">

            <ul class="b-service_related_list">
			<?foreach($relatedServices as $relatedService):?>
                <li><a href="<?=$relatedService["DETAIL_PAGE_URL"]?>" class="i-icon i-services-whitening"><?=$relatedService["NAME"]?></a></li>
			<?endforeach;?>
            </ul>

        </div>
	<?endforeach;?>
    </div>
</section>

<?
$isGray = !$isGray;
endif;
if(!empty($arResult["PREVIEW_PICTURE"]["SRC"]) && !empty($arResult["~PREVIEW_TEXT"])):
	$arAnchors["service-intro"]["SHOW"] = true;
	?>
<section class="b-service_intro b-section b-section__closed<?if($isGray):?> b-section__gray<?endif;?>" id="service-intro">

    <article class="b-service_intro_text b-wysiwyg">

        <?if(!empty($arResult["PREVIEW_PICTURE"]["SRC"])):?>
            <a class="b-service_intro_pic b-fig b-fig__intro g-pull-left js-lightBox" href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" data-group="content">
                <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arResult["PREVIEW_PICTURE"]["ALT"]?>" />
            </a>
        <?endif;?>
        <div class="b-wysiwyg_intro clear">
			<header class="b-heading">
				<h2 class="b-heading_subtitle">Почему мы</h2>
			</header>
            <?=$arResult["~PREVIEW_TEXT"]?>
        </div>

    </article>

</section>



<?
$isGray = !$isGray;
endif;
if(!empty($arResult["~DETAIL_TEXT"])):?>
<section class="b-service_about b-section b-section__closed<?if($isGray):?> b-section__gray<?endif;?>" id="service-about">
<?if(!empty($arResult["PROPERTIES"]["TITLE_DESCRIPTION"]["VALUE"])):?>
	<header class="b-heading">
		<h2 class="b-heading_subtitle"><?=$arResult["PROPERTIES"]["TITLE_DESCRIPTION"]["VALUE"]?></h2>
	</header>
<?endif;?>
<div class="js-pocket faded faded-gray" data-resolutions="xs,sm,md,lg" data-lines="8,6,9,7">

  <div class="js-pocket-box">
	  <div class="js-pocket-box-inner">

		<article class="b-wysiwyg">
			<?=$arResult["~DETAIL_TEXT"]?>
		</article>

	  </div>
  </div>

  <footer class="b-section_footer">

	  <span class="js-pocket-toggle" data-opened="Свернуть" data-closed="Читать дальше">Читать дальше</span>

  </footer>

</div>
</section>
<?
	$isGray = !$isGray;
endif;
?>
<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/service_benefits.php"), Array(), Array("MODE"=>"php") );?>
<?
$isGray = !$isGray;
if(!empty($arResult["DISPLAY_PROPERTIES"]["RABOTI"]["DISPLAY_VALUE"])):
	$arAnchors["service-works"]["SHOW"] = true;
	?>
<section class="b-service_works b-section b-section__closed<?if($isGray):?> b-section__gray<?endif;?>" id="service-works">
	<header class="b-heading">
		  <div class="row">
			  <div class="row-table">
				  <div class="col-xs-12 col-md-8 col-vertical-middle">

					  <h2 class="b-heading_subtitle">Примеры работ</h2>

				  </div>
				  <div class="col-xs-12 col-md-4 col-vertical-middle hidden-xs hidden-sm g-right">

					  <a href="<?=$arResult["DISPLAY_PROPERTIES"]["RABOTI"]["LINK"]?>" class="b-heading_more i-icon i-pages-works"><span>Все работы</span></a>

				  </div>
			  </div>
		  </div>
	  </header>
    <div class="b-carousel" id="service-works-first-carousel" data-auto-height="[true, true, false, false]" data-merge-fit="false,false,true,true" data-nav="true" data-nav-container="#service-works-first-carousel-controls" data-dots="true" data-dots-container="#service-works-first-carousel-controls" data-dots-each="true" data-resolutions="xs,sm,md,lg" data-xs="1" data-sm="1" data-md="2" data-lg="2" data-margin="10">
        <?foreach($arResult["DISPLAY_PROPERTIES"]["RABOTI"]["DISPLAY_VALUE"] as $arItem):?>
            <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/work.php"), Array("arItem" => $arItem), Array("MODE"=>"php") );?>
        <?endforeach;?>
    </div>

    <footer class="b-service_works_carousel_footer">
        <div class="row">
            <div class="col-xs-24 col-md-12 g-center-xs g-center-sm">
            <?if(count($arResult["DISPLAY_PROPERTIES"]["RABOTI"]["DISPLAY_VALUE"]) > 1):?>
                <div class="b-carousel_controls" id="service-works-first-carousel-controls"></div>
            <?endif;?>
            </div>
            <div class="col-xs-24 col-md-12 g-center-xs g-center-sm g-right-md g-right-lg">

                <a href="/forms/?form_id=1&pushivent=zapis_na_priem" class="e-btn e-btn_green e-btn_md js-popup">Записаться на прием</a>

            </div>
        </div>

    </footer>

</section>

<?
$isGray = !$isGray;
endif;
if(!empty($arResult["DISPLAY_PROPERTIES"]["VIDEO"]["DISPLAY_VALUE"])):
	$arAnchors["service-video"]["SHOW"] = true;
	?>
  <section class="b-service_video b-section b-section__closed<?if($isGray):?> b-section__gray<?endif;?>" id="service-video">

	  <header class="b-heading">
		  <div class="row">
			  <div class="row-table">
				  <div class="col-xs-12 col-md-8 col-vertical-middle">

					  <h2 class="b-heading_subtitle">Видео</h2>

				  </div>
				  <div class="col-xs-12 col-md-4 col-vertical-middle hidden-xs hidden-sm g-right">

					  <a href="<?=$arResult["DISPLAY_PROPERTIES"]["VIDEO"]["LINK"]?>" class="b-heading_more i-icon i-pages-video"><span>Все видео</span></a>

				  </div>
			  </div>
		  </div>
	  </header>

	  <div class="b-service_video_carousel b-carousel" id="service-video-carousel" data-auto-height="false" data-nav="true" data-nav-container="#service-video-carousel-controls" data-dots="true" data-dots-container="#service-video-carousel-controls" data-dots-each="true" data-auto="false" data-interval="10" data-margin="21" data-xs="1" data-sm="2" data-md="3" data-lg="3">
		  <?foreach($arResult["DISPLAY_PROPERTIES"]["VIDEO"]["DISPLAY_VALUE"] as $arItem):?>
		  <div class="b-carousel_item">
			
			 <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/video_item.php"), Array("arItem" => $arItem), Array("MODE"=>"php") );?> 

		  </div>
		  <?endforeach?>
	  </div>
	<?if(count($arResult["DISPLAY_PROPERTIES"]["VIDEO"]["DISPLAY_VALUE"]) > 1):?>
	  <footer class="b-service_video_carousel_footer">

		  <div class="b-carousel_controls" id="service-video-carousel-controls"></div>

	  </footer>
	<?endif;?>
  </section>

<?
$isGray = !$isGray;
endif;
if(!empty($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["DISPLAY_VALUE"])):
	$arAnchors["service-dentists"]["SHOW"] = true;
	?>
  <section class="b-service_dentists b-section b-section__closed<?if($isGray):?> b-section__gray<?endif;?>" id="service-dentists">

	  <header class="b-heading">
		  <div class="row">
			  <div class="row-table">
				  <div class="col-xs-12 col-md-8 col-vertical-middle">

					  <h2 class="b-heading_subtitle">Врачи</h2>

				  </div>
				  <div class="col-xs-12 col-md-4 col-vertical-middle hidden-xs hidden-sm g-right">

					  <a href="<?=$arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["LINK"]?>" class="b-heading_more i-icon i-pages-dentists"><span>Все врачи</span></a>

				  </div>
			  </div>
		  </div>
	  </header>

	  <div class="b-service_dentists_carousel b-carousel" id="service-dentists-carousel" data-auto-height="false" data-nav="true" data-nav-container="#service-dentists-carousel-controls" data-dots="true" data-dots-container="#service-dentists-carousel-controls" data-dots-each="false" data-auto="false" data-interval="10" data-margin="28" data-xs="2" data-sm="2" data-md="3" data-lg="4">
		  <?foreach($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["DISPLAY_VALUE"] as $arItem):?>
		  <div class="b-carousel_item">
			
			 <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/doctor_item.php"), Array("arItem" => $arItem), Array("MODE"=>"php") );?> 

		  </div>
		  <?endforeach?>
	  </div>
		<?if(count($arResult["DISPLAY_PROPERTIES"]["DOCTORS"]["DISPLAY_VALUE"]) > 1):?>
	  <footer class="b-service_dentists_carousel_footer ">

		  <div class="b-carousel_controls" id="service-dentists-carousel-controls"></div>

	  </footer>
		<?endif;?>
  </section>

<?
$isGray = !$isGray;
endif;
$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/simptomi.php"), Array("isGray" => $isGray), Array("MODE"=>"php") );
$isGray = !$isGray;
?>
<?if(!empty($arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["DISPLAY_VALUE"])):
	$arAnchors["service-reviews"]["SHOW"] = true;
	?>
	<section class="b-service_reviews b-section b-section__closed <?if($isGray):?>  b-section__gray<?endif;?>" id="service-reviews">
		<header class="b-heading">
			<div class="row">
				<div class="row-table">
					<div class="col-xs-12 col-md-8 col-vertical-middle">

						<h2 class="b-heading_subtitle">Отзывы</h2>

					</div>
					<div class="col-xs-12 col-md-4 col-vertical-middle hidden-xs hidden-sm g-right">

						<a href="<?=$arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"]?>" class="b-heading_more i-icon i-pages-reviews"><span>Все отзывы</span></a>

					</div>
				</div>
			</div>
		</header>
		<div class="b-service_reviews_tabs js-tabs">

			<div class="b-service_reviews_tabs_buttons b-tabs js-tabs-toggle">
				<a href="#service-reviews-site">Отзывы на сайте</a>
				<a href="#service-reviews-vk">ВКонтакте</a>
				<a href="#service-reviews-facebook">Facebook</a>
			</div>
			<div class="js-tabs-wrapper">
				<div class="js-tabs-page" id="service-reviews-site">
					<div id="service-reviews-list">
						<?foreach($arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["DISPLAY_VALUE"] as $arItem):?>
							<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/review_item.php"), Array("arItem" => $arItem, "isGray" => $isGray), Array("MODE"=>"php") );?>
						<?endforeach;?>
					</div>
					<footer class="b-service_faq_footer">
						<div class="row">
							<div class="row-table">
								<div class="col-xs-10 col-md-12 col-vertical-middle">

									<div class="b-section_footer">

										<?if($arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"] > 2):?>
											<a class="g-dashed js-loading-on-require" href="/ajax/review.php?uslugi_list=<?=implode(",",$arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"])?>&count=<?=$arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"]?>" data-loading-target="#service-reviews-list" data-method="get" data-page="3">Показать еще</a>
										<?else:?>
											<a class="g-solid" href="<?=$arResult["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"]?>">Читать все отзывы</a>
										<?endif;?>

									</div>

								</div>
								<div class="col-xs-14 col-md-12 col-vertical-middle g-right">

									<a class="e-btn e-btn_md e-btn_green_outline js-popup" href="/forms/review/?pushivent=ostavit_otzyv">Оставить отзыв</a>

								</div>
							</div>
						</div>
					</footer>
				</div>
				<div class="js-tabs-page" id="service-reviews-vk">

					<div class="b-widget" id="vk-thread"></div>

				</div>
				<div class="js-tabs-page" id="service-reviews-facebook">

					<div id="fb-root"></div>
					<div class="b-widget fb-comments" data-href="http://zuub.ru" data-numposts="10"></div>

				</div>
			</div>
			

			

	</section>
	<?
	$isGray = !$isGray;
endif;?>
<?if(!empty($arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]["DISPLAY_VALUE"])):
	$arAnchors["service-faq"]["SHOW"] = true;
	?>
	<section class="b-service_faq b-section b-section__closed<?if($isGray):?>  b-section__gray<?endif;?>" id="service-faq">
		<header class="b-heading">
			<h2 class="b-heading_subtitle">Вопрос-ответ</h2>
		</header>

		<div class="b-service_faq_list" id="service-faq-list">
			<?foreach($arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]["DISPLAY_VALUE"] as $arItem):?>
				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/vopros_otvet_item.php"), Array("arItem" => $arItem, "isGray" => $isGray), Array("MODE"=>"php") );?>
			<?endforeach;?>
		</div>

		<footer class="b-service_faq_footer">
			<div class="row">
				<div class="row-table">
					<div class="col-xs-10 col-md-12 col-vertical-middle">

						<div class="b-section_footer">

							<?if($arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]["COUNT"] > 2):?>
								<a class="g-dashed js-loading-on-require" href="/ajax/faq.php?uslugi_list=<?=implode(",",$arResult["PROPERTIES"]["HLB_USLUGI"]["VALUE"])?>&count=<?=$arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]["COUNT"]?>" data-loading-target="#service-faq-list" data-method="get" data-page="3">Показать еще</a>
							<?else:?>
								<a class="g-solid" href="<?=$arResult["DISPLAY_PROPERTIES"]["VOPROS_OTVET"]["LINK"]?>">Читать все вопросы</a>
							<?endif;?>

						</div>

					</div>
					<div class="col-xs-14 col-md-12 col-vertical-middle g-right">

						<a class="e-btn e-btn_md e-btn_green_outline js-popup" href="/forms/ask/?pushivent=zadat_vopros">Задать вопрос</a>

					</div>
				</div>
			</div>
		</footer>

	</section>
<?
$isGray = !$isGray;
endif;?>
<?if(!empty($arResult["DISPLAY_PROPERTIES"]["STATI"]["DISPLAY_VALUE"])):?>
<section class="b-service_articles b-section b-section__closed<?if($isGray):?> b-section__gray<?endif;?> b-section__last">

  <header class="b-heading">
	  <div class="row">
		  <div class="row-table">
			  <div class="col-xs-12 col-md-8 col-vertical-middle">

				  <h2 class="b-heading_subtitle">Статьи</h2>

			  </div>
			  <div class="col-xs-12 col-md-4 col-vertical-middle hidden-xs hidden-sm g-right">

				  <a href="<?=$arResult["DISPLAY_PROPERTIES"]["STATI"]["LINK"]?>" class="b-heading_more i-icon i-pages-articles"><span>Все статьи</span></a>

			  </div>
		  </div>
	  </div>
  </header>

  <div class="b-service_articles_carousel b-carousel" id="service-articles-carousel" data-auto-height="false" data-nav="true" data-nav-container="#service-articles-carousel-controls" data-dots="true" data-dots-container="#service-articles-carousel-controls" data-dots-each="true" data-auto="false" data-interval="10" data-margin="21" data-xs="1" data-sm="2" data-md="3" data-lg="3">
	  
	  <?foreach($arResult["DISPLAY_PROPERTIES"]["STATI"]["DISPLAY_VALUE"] as $arItem):?>
		  <div class="b-carousel_item">
			
			 <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/stati_item.php"), Array("arItem" => $arItem), Array("MODE"=>"php") );?> 

		  </div>
	  <?endforeach?>
	  
  </div>
<?if(count($arResult["DISPLAY_PROPERTIES"]["STATI"]["DISPLAY_VALUE"]) > 1):?>
  <footer class="b-service_articles_carousel_footer ">

	  <div class="b-carousel_controls" id="service-articles-carousel-controls"></div>

  </footer>
<?endif;?>
</section>
<?endif;?>
<?$this->SetViewTarget('service_anchor_menu');?>
<div class="b-main_anchors_wrap">
	<div class="b-main_anchors_wrap_panel">
		<nav class="b-main_anchors hidden-xs hidden-sm js-scrollSpy" data-offset="43">
		<?$first = true;
		foreach($arAnchors as $key=>$anchor):
			if(!$anchor["SHOW"]) continue;?>
			<a href="#<?=$key?>" class="e-btn<?if($first):?> active<?$first=false;endif;?>" data-threshold="42" ><?=$anchor["TEXT"]?></a>
		<?endforeach;?>
		</nav>
	</div>
</div>
<?$this->EndViewTarget();?>

  
<pre>
<?//print_r($arResult["DISPLAY_PROPERTIES"]["RELATED_SERVICES"]);?>
</pre>

