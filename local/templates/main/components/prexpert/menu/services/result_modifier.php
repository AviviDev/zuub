<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
unset($menu);
$menu = array();
foreach($arResult as $arItem)
{
    if($arItem["DEPTH_LEVEL"] == 1)
    {
        $menu[$arItem["ID"]] = $arItem;
        $lastId = $arItem["ID"];
    }
    else
    {
        $arItem["DEPTH_LEVEL"]--;
        $menu[$lastId]["SUB_MENU"][] = $arItem;
    }
}

$arResult = array_chunk($menu,floor(count($menu)/3));
if(count($arResult) > 3)
{
    foreach($arResult[3] as $value)
    {
        $arResult[2][] = $value;
    }
    array_pop($arResult);
}
//$arResult = array_chunk($arResult,2);

?>
