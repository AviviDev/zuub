<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(!empty($arResult)):
	$showAll = false;
	?>
	<div class="b-header_services js-menu" id="<?=ServiceMenu::GetTarget()?>">

		<div class="b-header_services_wrap js-scrollBar" data-resolutions="xs,sm,md,lg">

			<div class="b-header_services_header">

				<div class="b-header_services_header_title hidden-md hidden-lg">Услуги</div>
				<span class="b-header_services_header_toggle g-dashed i-icon i-chevron-down hidden-xs hidden-sm" data-closed="Развернуть все разделы" data-opened="Свернуть все разделы">Развернуть все разделы</span>

			</div>
			<div class="row">
				<div class="col-xs-24 col-md-12 col-lg-16">
					<div class="row">
						<?$arLast = array_pop($arResult);
						foreach($arResult as $keyMenu=>$arMenu):?>
							<div class="col-xs-24 col-lg-12">
								<?foreach($arMenu as $menu):?>
									<?if($menu["SUB_MENU"]):?>
									<div class="b-header_services_box js-spoiler" data-resolutions="xs,sm">

										<div class="b-header_services_title i-icon <?=$menu["CLASS"]?> js-spoiler-toggle">
											<a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a>
										</div>

										<ul class="b-header_services_list js-spoiler-box js-accordion">
											<?$previousLevel = 0;
											foreach($menu["SUB_MENU"] as $arItem):
											if($arItem["DEPTH_LEVEL"] > 2) $showAll=true;
											?>
											<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
												<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
											<?endif?>

											<?if ($arItem["IS_PARENT"]):?>
											<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a><ul>
											<?else:?>
												<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
											<?endif?>
											<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
											<?endforeach?>
											<?if ($previousLevel > 1)://close last item tags?>
												<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
											<?endif?>
										</ul>

									</div>
									<?else:?>
									<div class="b-header_services_box">

										<div class="b-header_services_title i-icon <?=$menu["CLASS"]?>">
											<a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a>
										</div>

									</div>
									<?endif;?>
								<?endforeach?>
							</div>
						<?endforeach;?>
					</div>
				</div>
				<div class="col-xs-24 col-md-12 col-lg-8">
					<?foreach($arLast as $keyMenu=>$menu):?>
						<?if($menu["SUB_MENU"]):?>
							<div class="b-header_services_box js-spoiler" data-resolutions="xs,sm">

							<div class="b-header_services_title i-icon <?=$menu["CLASS"]?> js-spoiler-toggle">
							<a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a>
							</div>

							<ul class="b-header_services_list js-spoiler-box js-accordion">
								<?$previousLevel = 0;
								foreach($menu["SUB_MENU"] as $arItem):
								if($arItem["DEPTH_LEVEL"] > 2) $showAll=true;
								?>
								<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
									<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
								<?endif?>

								<?if ($arItem["IS_PARENT"]):?>
								<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a><ul>
										<?else:?>
											<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
										<?endif?>
										<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
										<?endforeach?>
										<?if ($previousLevel > 1)://close last item tags?>
											<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
										<?endif?>
									</ul>

							</div>
						<?else:?>
							<div class="b-header_services_box">

								<div class="b-header_services_title i-icon <?=$menu["CLASS"]?>">
									<a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a>
								</div>

							</div>
						<?endif;?>
					<?endforeach;?>
				</div>

			</div>

		</div>

		<button class="b-header_services_close e-btn i-icon i-close js-menu-close" type="button"></button>

	</div>
<?endif;?>

