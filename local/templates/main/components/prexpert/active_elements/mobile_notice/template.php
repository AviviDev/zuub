<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult) > 0):?>
	<div class="b-notice js-headerStrip hidden-lg">
		<div class="container">

			<?foreach($arResult as $arItem):?>
				<p><?=$arItem["~PREVIEW_TEXT"]?></p>
			<?endforeach;?>

		</div>
	</div>
<?endif;?>