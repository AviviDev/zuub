<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult) > 0):?>
<aside class="b-notice js-headerStrip hidden-xs hidden-sm hidden-md">
	<div class="container">
		<?foreach($arResult as $arItem):?>
		<p><?=$arItem["~PREVIEW_TEXT"]?></p>
		<?endforeach;?>
	</div>
</aside>
<?endif;?>