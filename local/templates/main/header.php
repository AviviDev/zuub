<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru-RU">

  <head>

      <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/html/_head.php"), Array(), Array("MODE"=>"php") );?>
	  <!-- Google Tag Manager-->
	  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		  })(window,document,'script','dataLayer','GTM-WRN7V2');</script>
	  <!-- End Google Tag Manager -->
	 
  </head>

  <body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRN7V2"
					height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
<?$APPLICATION->ShowPanel()?>
      <div class="b-page">

			<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/html/_notice.php"), Array(), Array("MODE"=>"php") );?>

          <div class="b-page_container container">

             <?/*if(CUR_IBLOCK_ID == 1)
				$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/html/_header_service_menu_child.php"), Array(), Array("MODE"=>"php") );
				else*/
				$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/html/_header.php"), Array(), Array("MODE"=>"php") );?>
              <!-- Main column -->
              <main class="b-main clear">
                  <div class="b-main_box b-main_box__white">

					<?$APPLICATION->IncludeComponent(
						"bitrix:breadcrumb",
						".default",
						array(
							"COMPONENT_TEMPLATE" => ".default",
							"START_FROM" => "0",
							"PATH" => "",
							"SITE_ID" => "s1"
						),
						false
					);
					?>