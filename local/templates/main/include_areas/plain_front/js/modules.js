var siteModules = (function(window, undefined) {

    function accordions(options) {

        options.selector = !!options.selector ? options.selector : '.js-accordion';
        options.namespace = !!options.namespace ? options.namespace + ' ' : '';

        // Init
        init(options);

        $(window).bind('resize.pocketReInit', function() {

            helpers.delay.call($('body'), init(options), 250);

        });

        function init(options) {

            var $accordion = $(options.namespace + options.selector);

            $accordion.each(function() {

                var $accordion = $(this),
                    screens = !!$accordion.data('resolutions') ? $accordion.data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'];

                $accordion.data('screens', screens);

                $accordion.find('li ul').slideUp({ duration: 0 });
                $accordion.find('.opened').find('> ul').slideDown({ duration: 0 });

                    $accordion
                        .find('a')
                        .unbind('click.accordionClick')
                        .each(function() {

                            var childLevel = !!$(this).closest('li').find('ul').length;

                            $(this)
                                .closest('li')
                                .toggleClass('hasChild', childLevel);

                            if (screens.indexOf(helpers.screen()) >= 0) {

                                $(this)
                                    .bind('click.accordionClick', function(e) {

                                        //if (childLevel) {
                                        if ($(this).closest('li').hasClass('hasChild')) {

                                            e.preventDefault();

                                            // Close neighbors items
                                            if (!!$accordion.data('neighbors')) {

                                                var $siblings = $(this).closest('li').siblings();

                                                $siblings.toggleClass('opened', false);

                                                $siblings
                                                    .find('> ul, > div')
                                                    .slideUp({ duration: 200, easing: 'easeOutQuart' });

                                            }

                                            // Open current item
                                            var $current = $(this).closest('li');

                                            $current.toggleClass('opened');

                                            $current
                                                .find('> ul, > div')
                                                .slideToggle({ duration: 200, easing: 'easeOutQuart' });

                                        }

                                    });

                            } else {

                                $(this)
                                    .closest('li')
                                    .toggleClass('hasChild', false);

                            }

                        });

                $accordion
                    .unbind('accordion.close')
                    .bind('accordion.close', function() {

                        if ($accordion.data('screens').indexOf(helpers.screen()) >= 0) {

                            $(this).find('li').each(function() {

                                $(this).toggleClass('opened', false);

                                $(this)
                                    .find('> ul, > div')
                                    .slideUp({ duration: 200, easing: 'easeOutQuart' });

                            });

                        }

                    });

                $accordion
                    .unbind('accordion.open')
                    .bind('accordion.open', function() {

                        if ($accordion.data('screens').indexOf(helpers.screen()) >= 0) {

                            $(this).find('li').each(function() {

                                $(this).toggleClass('opened', true);

                                $(this)
                                    .find('> ul, > div')
                                    .slideDown({ duration: 200, easing: 'easeOutQuart' });

                            });

                        }

                    });

            });

        }

    }

    function dropDown(options) {

        var $locker = !!options.locker ? options.locker : $('.b-page');

        options.switch = !!options.switch ? options.switch : 'opened';

        $(document).bind((navigator.userAgent.match(/iPhone|iPad|iPod/i)) ? 'touchend.dropDown' + options.selector : 'click.dropDown' + options.selector, function(e) {

            var $target = $(e.target),

                targetIsSwitcher = $target.hasClass(options.selector + '-toggle') || !!$target.closest('.' + options.selector + '-toggle').length,
                isSwipeAction = helpers.touches.touchmove.y > -1 && (Math.abs(helpers.touches.touchstart.y - helpers.touches.touchmove.y) > 5);

            if (targetIsSwitcher) {

                if ($target.is('a')) { e.preventDefault(); }

                var screens = !!$target.data('resolutions') ? $target.data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'];

                if (screens.indexOf(helpers.screen()) >= 0) {

                    e.preventDefault();

                    var $dropDown = $target.closest('.' + options.selector),
                        state = $dropDown.hasClass(options.switch);

                    $('.' + options.selector).toggleClass(options.switch, false);

                    $dropDown.toggleClass(options.switch, !state);
                    $dropDown.find('.' + options.selector + '-box').toggleClass(options.switch, !state);

                    if (!!options.onToggle) {

                        options.onToggle.call($dropDown, !state);

                    }

                    if (!!$dropDown.data('lock')) {

                        if (!state) {

                            _lockPage.call($locker, options.selector);

                        } else {

                            _unLockPage.call($locker);

                        }

                    }

                    if (!!$dropDown.data('xhr')) {

                        _xhr($target, $dropDown, options, state);

                    }

                }

            }
            else if (!isSwipeAction && (!$target.closest('.' + options.selector + '-box').length || $target.hasClass(options.selector + '-close'))) {

                $('.' + options.selector).each(function() {

                    var screens = !!$(this).find(options.selector + '-toggle').data('resolutions') ? $(this).find(options.selector + '-toggle').data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'];

                    if (screens.indexOf(helpers.screen()) >= 0) {

                        $(this).toggleClass(options.switch, false);

                        if (!!$locker.data('namespace') && $locker.data('namespace') === options.selector) {

                            _unLockPage.call($locker);

                        }

                        if (!!options.onToggle) {

                            options.onToggle.call($(this), false);

                        }

                    }

                    _xhr($(this), $(this), options, true);

                });

            }

            helpers.touches = {
                touchstart: {x: -1, y: -1 },
                touchmove: { x: -1, y: -1 }
            };

        });

        $('.' + options.selector)
            .on('dropDown.open', function() {

                $(this).toggleClass(options.switch, true);

                if (!!$(this).data('lock')) {

                    _lockPage.call($locker);

                }

            })
            .on('dropDown.close', function() {

                $(this).toggleClass(options.switch, false);
                _unLockPage.call($locker);

            });

        function _lockPage() {

            $('body').css({ overflowY: 'scroll' });

            this
                .data('scrollState', document.documentElement.scrollTop || document.body.scrollTop)
                .data('isLocked', true)
                .data('namespace', options.selector)
                .css({ position: 'fixed', overflow: 'hidden', left: 0, top: 0, right: 0, marginTop: -this.data('scrollState'), boxSizing: 'border-box' });

        }

        function _unLockPage() {

            if (!!this.data('isLocked')) {

                $('body').css({ overflowY: '' });

                this
                    .css({ position: '', overflow: '', left: '', top: '', right: '', marginTop: '', boxSizing: '' })
                    .data('isLocked', false)
                    .data('namespace', false);

                document.documentElement.scrollTop = 0;
                document.body.scrollTop = 0;
                window.scrollBy(0, this.data('scrollState') || 0);

            }

        }

        function _xhr($target, $dropDown, options, state) {

            if (!!$dropDown.data('xhr')) {

                var url = $target.is('a') ? $target.attr('href') : $dropDown.data('xhr'),

                    $xhrBox = $dropDown.find('.' + options.selector + '-xhr'),
                    $xhrBoxWrapper = $('<div class="' + options.selector + '-xhr-wrapper"></div>'),

                    xhrBoxIsEmpty = $xhrBox.is(':empty');

                if (!state && xhrBoxIsEmpty) {

                    $.ajax({
                        url: url,
                        dataType: 'html',
                        success: function(response) {

                            $xhrBoxWrapper.append(response);
                            $xhrBoxWrapper.css({ opacity: 0, transition: 'opacity 400ms' });

                            $xhrBox.append($xhrBoxWrapper);
                            $xhrBoxWrapper.slideUp(0);

                            setTimeout(function() {

                                $xhrBoxWrapper
                                    .slideDown({ duration: 400, easing: 'easeOutQuart', complete: function() {

                                        $xhrBoxWrapper.find('> *').unwrap();

                                        if (!!options.onComplete) {

                                            options.onComplete(response);

                                        }

                                    }})
                                    .css({ opacity: '' });

                            }, 10);

                        }
                    })

                }

            }

        }

    }

    function spoilers(options) {

        options.toggleClass = !!options.toggleClass ? options.toggleClass : 'opened';

        // Hash check
        var hash = location.hash;

        if (!!hash && hash.match('spoiler')) {

            $(options.selector).each(function() {

                $(this).removeClass('opened');

                if ($(this).attr('id') == hash.split('#')[1]) {

                    $(this).addClass('opened');

                }

            });

        }

        // Init
        init(options);

        $(window).bind('resize.spoilerReInit', function() {

            init(options);

        });

        function init(options) {

            $(options.selector).each(function() {

                var $spoiler = $(this),

                    $body = $spoiler.find(options.selector + '-box').css({ display: '' }),
                    $toggle = $spoiler.find(options.selector + '-toggle').unbind('click.spoiler'),

                    screens = !!$spoiler.data('resolutions') ? $spoiler.data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'];

                if (screens.indexOf(helpers.screen()) >= 0) {

                    $spoiler.not('.' + options.toggleClass).find(options.selector + '-box').slideDown(0).slideUp(0);
                    $spoiler.filter('.' + options.toggleClass).find(options.selector + '-box').slideDown(0);

                    $toggle
                        .bind('click.spoiler', function(e) {

                            e.preventDefault();

                            var item = $(this).closest(options.selector),
                                state = item.hasClass(options.toggleClass) && item.find(options.selector + '-box').is(':visible');

                            if (!state) {

                                spoilerOpen.call(item, options.selector, options.onToggle);

                            } else {

                                spoilerClose.call(item, options.selector, options.onToggle);

                            }

                            // Close neighbors items
                            if (!!item.data('closeNeighbors')) {

                                var $neighbors = typeof item.data('closeNeighbors') === 'boolean' ? item.siblings(options.selector) : item.closest(item.data('closeNeighbors')).find(options.selector);

                                $neighbors.each(function() {
                                    spoilerClose.call($(this), options.selector);
                                });

                            }

                        });

                } else {

                    $spoiler.find(options.selector + '-box').show();

                }

            });

        }

        function spoilerClose(sel, callback) {

            this.toggleClass(options.toggleClass, false);

            var toggle = !!this.find(sel + '-toggle').data('closed') ? this.find(sel + '-toggle').data('closed') : this.find(sel + '-toggle').html();

            if (!!this.find(sel + '-toggle').data('closed')) {

                this.find(sel + '-toggle').html(toggle);

            }

            this.find(sel + '-toggle').toggleClass('active', false);
            this.find(sel + '-box').slideUp({ duration: 250, easing: 'easeOutQuart', complete: function() {

                if(typeof callback != 'undefined' && callback) {

                    callback(this);

                }

            } });

        }

        function spoilerOpen(sel, callback) {

            this.toggleClass(options.toggleClass, true);

            var toggle = !!this.find(sel + '-toggle').data('opened') ? this.find(sel + '-toggle').data('opened') : this.find(sel + '-toggle').html();

            if (!!this.find(sel + '-toggle').data('opened')) {

                this.find(sel + '-toggle').html(toggle);

            }

            this.find(sel + '-toggle').toggleClass('active', true);
            this.find(sel + '-box').slideDown({ duration: 250, easing: 'easeOutQuart', complete: function() {

                if(typeof callback != 'undefined' && callback) {

                    callback(this);

                }

            } });

        }

    }

    function pockets(options) {

        // Init
        init(options);

        $(window).bind('resize.pocketReInit', function() {

            helpers.delay.call($('body'), init(options), 250);

        });

        function init(options) {

            $(options.selector).each(function() {

                var $pocket = $(this),

                    $pocketBody = $pocket.find(options.selector + '-box'),
                    $pocketBodyInner = $pocket.find(options.selector + '-box-inner').css({ overflow: 'hidden' }),
                    $pocketToggle = $pocket.find(options.selector + '-toggle').unbind('click.pocketModule'),

                    screens = !!$pocket.data('resolutions') ? $pocket.data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'],
                    lines = !!$pocket.data('lines') ? $pocket.data('lines').split(',') : [2, 3, 4, 6];

                // Hide toggle btn
                // $pocketToggle.removeClass('hidden-xs hidden-sm hidden-md hidden-lg');
                // $.each(screens, function(key, val) { $pocketToggle.addClass('visible-' + val); });

                if (screens.indexOf(helpers.screen()) >= 0) {

                    $pocket.toggleClass('active', true);
                    $pocketBody.css({ overflow: 'hidden' });

                    $pocket.data('range', {
                        min: _getLineHeight.call($pocketBody) * lines[screens.indexOf(helpers.screen())] - 2,
                        max: $pocketBodyInner.length ? $pocketBodyInner.outerHeight() : $pocketBody.outerHeight()
                    });

                    if (!$pocket.data('noAnimation')) {

                        $pocketBody
                            .css(helpers.pfx + 'transition', 'max-height ' + options.duration + 'ms')
                            .css('transition', 'max-height ' + options.duration + 'ms');

                    }

                    setTimeout(function() {

                        if (typeof $pocket.data('range') !== 'undefined') {

                            $pocket.not('.opened').find(options.selector + '-box').css({ maxHeight: $pocket.data('range').min });

                        }

                        if ($.isFunction($.fn.owlCarousel)) {

                            $pocketBody.closest('.owl-carousel').trigger('refresh.owl.carousel');

                        }

                    }, 10);

                    $pocketToggle
                        .toggleClass('excess', $pocket.data('range').max <= $pocket.data('range').min)
                        .bind('click.pocketModule', function(e) {

                            e.preventDefault();

                            var $this = $(this).closest(options.selector),
                                state = $this.hasClass('opened');

                            _pocketToggle.call($this, options, !state);

                            // Close neighbors items
                            if (!!$this.data('closeNeighbors')) {

                                var $neighbors = typeof $this.data('closeNeighbors') === 'boolean' ? $this.siblings(options.selector) : $this.closest($this.data('closeNeighbors')).find(options.selector);

                                $neighbors.each(function() {

                                    _pocketToggle.call($(this), options, false);

                                });

                            }

                        });

                } else {

                    $pocket.toggleClass('active', false);

                    $pocketBody.css({ maxHeight: '', overflow: 'visible' });
                    $pocketBodyInner.css({ overflow: '' });

                }

            });

        }

        function _getLineHeight() {

            var lineHeight =  parseFloat(this.css('line-height')),
                fontSize = Math.ceil(parseFloat(this.css('font-size')));

            lineHeight = typeof lineHeight !== 'undefined' && lineHeight < fontSize ? lineHeight * fontSize : lineHeight;

            return parseInt(typeof lineHeight !== 'undefined' ? lineHeight : fontSize, 10);

        }

        function _pocketToggle(options, state) {

            if (typeof options.onToggle != 'undefined' && options.onToggle) {

                options.onToggle.call(this, state);

            }

            var $pocket = this,

                $pocketBody = $pocket.find(options.selector + '-box'),
                $pocketBodyInner = $pocket.find(options.selector + '-box-inner'),
                $pocketToggle = $pocket.find(options.selector + '-toggle'),

                stringFlag = state ? 'opened' : 'closed',
                height = state ? $pocketBodyInner.length ? $pocketBodyInner.outerHeight() : $pocket.data('range').max : $pocket.data('range').min;

            $pocket.toggleClass('opened', state);

            $pocketBody.css({ maxHeight: height });
            $pocketToggle.html(!!$pocketToggle.data(stringFlag) ? $pocketToggle.data(stringFlag) : $pocketToggle.html());

            if (!!$pocket.data('noAnimation')) {

                $pocketBody.css({ overflow: state ? '' : 'hidden' });
                $pocketBodyInner.css({ overflow: state ? '' : 'hidden' });

            }

            // Refresh carousel height
            var $parentCarousel = $pocket.closest('.owl-carousel');

            if ($parentCarousel.length) {

                var carouselData = $parentCarousel.data('owl.carousel');

                if (!!carouselData.options.responsive[carouselData._breakpoint].autoHeight) {

                    $parentCarousel.trigger('refresh.owl.carousel');

                }

            }

            setTimeout($.proxy(function() {

                if (typeof options.onToggled != 'undefined' && options.onToggled) {

                    options.onToggled.call(this, state);

                }

            }, this), options.duration);

        }

    }

    function tabs(options) {

        var widget = options.selector,
            toggle = options.selector + '-toggle',
            toggleItem = options.selector + '-toggle-control',
            content = options.selector + '-wrapper',
            page = options.selector + '-page';

        init(options);

        $(window).bind('resize.tabsReInit', function() {

            init(options);

        });

        function init(options) {

            $(options.selector).each(function() {

                var $wrapper = $(this).find(content),
                    screens = !!$(this).data('resolutions') ? $(this).data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'];

                if (screens.indexOf(helpers.screen()) >= 0) {

                    // Init tabs
                    $(this).toggleClass('js-init', true);

                    setTimeout(function() {

                        $(this).toggleClass('js-transition', true);

                    }, 500);

                    if (!$(this).find(toggle + ' a.current').length)
                        $(this).find(toggle + ' a:first').addClass('current');

                    if (!$(this).find(toggleItem + '.current').length)
                        $(this).find(toggleItem + ':first').addClass('current');

                    var hash = $(this).find(toggle + ' a.current, ' + toggleItem + '.current').data('hash') || $(this).find(toggle + ' a.current, ' + toggle + ' ' + toggleItem + '.current').attr('href'),
                        height = $(page + hash).outerHeight(true);

                    $(this)
                        .find(page)
                        .toggleClass('visible', false);

                    $(this)
                        .find(page + hash)
                        .toggleClass('visible', true);

                    // Listening events

                    var $btn = $(this).find(options.selector + '-toggle a[href*="#"], ' + toggleItem);

                    $btn
                        .unbind(!$btn.is('input') ? 'click.switchTabs' : 'change.changeTabs')
                        .bind(!$btn.is('input') ? 'click.switchTabs' : 'change.changeTabs', function(e) {

                            e.preventDefault();

                            $wrapper.css({ height: height });

                            hash = $(this).data('hash') || $(this).attr('href');
                            height = $(page + hash).outerHeight(true);

                            // Off tabs
                            $(this)
                                .closest(widget)
                                .find(toggle + ' a, ' + toggleItem)
                                .toggleClass('current', false);

                            $(this)
                                .closest(widget)
                                .find(page)
                                .toggleClass('visible', false);

                            // On select tab
                            $(this)
                                .toggleClass('current', true);

                            $(this)
                                .closest(widget)
                                .find(page + hash)
                                .toggleClass('visible', true);

                            // Correct wrapper
                            $wrapper.animate({ height: height }, 500, 'easeOutQuart', function() {

                                $(this).css({ height: '' });

                            });

                            // Callback fire
                            if(typeof options.onToggle != 'undefined' && options.onToggle) {

                                options.onToggle($(this), $(this).closest(options.selector).find(options.selector + '-page' + ($(this).data('hash') || $(this).attr('href'))));

                            }

                        });

                } else {

                    $(this).toggleClass('js-init', false);

                    $(this)
                        .find(toggle + ' a')
                        .toggleClass('current', false);

                    $(this)
                        .find(page)
                        .toggleClass('visible', false);

                    $(this)
                        .find(content)
                        .css({ height: '' });

                }

            });

        }

    }

    function hashNav() {

        $('body')
            .on('click', 'a[href^="#"]:not([href="#"], [class*="js-"], [class*="js-tabs"] a, [class*="js-works"] a)', function(e) {

                var url = $(this).attr('href'),
                    threshold = ($(this).data('threshold') || 0) /* + $header.outerHeight()*/;

                if (!!$(this).data('tab')) {

                    $('.js-tabs a[href="' + $(this).data('tab') + '"]').trigger('click.switchTabs')

                }

                if (url.length > 1 && $(url).length) {

                    var $element = !!$(url).length ? $(url) : $('[name="' + (url.substring(1)) + '"]');

                    e.preventDefault();

                    var $header = $('.b-header'),
                        destination = $element.offset().top - threshold;

                    $('html, body').animate({ scrollTop: destination }, 1500, 'easeInOutExpo');

                }

            });

    }

    function tilesAlignment() {

        var $tiles = $('.js-tiles');

        $tiles.each(function() {

            var $this = $(this),
                $goods = $('.js-tiles-good', $this),

                screens = $this.data('resolutions') ? $this.data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'];

            _alignment();

            $(window).bind('load.tiles resize.tiles', function() {

                helpers._delay.call($this, _alignment);

            });

            function _alignment() {

                $goods
                    .height('')
                    .css({ minHeight: '' });

                if (screens.indexOf(helpers.screen()) >= 0) {

                    if ($tiles.data('alignmentMode') == 'strong') {

                        $goods.height(helpers.maxHeight($goods));

                    } else {

                        $goods.css({ minHeight: helpers.maxHeight($goods) });

                    }

                }

            }

        });

    }

    function swipeOfTables(options) {

        var $collection = $(options.collection);

        $(window).bind('load.buildSwipeOfTables resize.refreshSwipeOfTables', _processing);

        function _processing() {

            $collection.each(function() {

                if (!$(this).closest('.b-table_overflow').length) {

                    $(this).wrap('<div class="b-table_overflow"></div>');

                }

                var $container = $(this).closest('.b-table_overflow'),

                    tableWidth = $(this).width(),
                    containerWidth = $container.width();

                if (tableWidth > containerWidth) {

                    $(this)
                        .closest('.b-table_overflow')
                        .addClass('scrollable');

                } else {

                    $(this)
                        .closest('.b-table_overflow')
                        .removeClass('scrollable');
                }

            });

        }

    }

    function imagesOnRetinaDisplays(options) {

        options = !!options ? options : {};
        options.namespace = !!options.namespace ? options.namespace + ' ' : '.b-page ';

        if ('devicePixelRatio' in window && window.devicePixelRatio > 1) {

            var $images = $(options.namespace + 'img.js-2x');

            $images.each(function() {

                var lowRes = $(this).attr('src');

                var highRes = lowRes.replace('.', '@2x.');
                $(this).attr('src', highRes);

                $(this).on('error', function() {
                    $(this).attr('src', lowRes);
                });

            });

            // Set cookies
            document.cookie = 'devicePixelRatio=' + window.devicePixelRatio + ';';

        }

    }

    function loadingOnRequire(options) {

        var $links = $(options.btn),
            $infinityLoadingContainer = $(options.container);

        // Set data
        $links.each(function() {

            $(this).data('page', $(this).data('page') || 1);

        });

        // Loading on require
        $links.on('click.loadingOnRequire', function(e) {

            e.preventDefault();

            var $link = $(this).toggleClass('loading', true),
                $target = $($link.data('loadingTarget'));

            _loadContent.call($target,
                {
                    url: $link.attr('href'),
                    method: $link.data('method') || 'get',
                    data: { page: $link.data('page') || 1 },
                    onComplete: options.onComplete,
                    noWrapping: $(this).data('noWrapping')
                },
                function(response) {

                    setTimeout(function() {

                        $link.toggleClass('loading', false);

                    }, 300);

                    if (!!response.btnText) {

                        $link.text(response.btnText);

                    }

                    $link
                        .parent()
                        .toggleClass('fade', response.last && !response.moreUrl);

                    $link
                        .toggleClass('fade', response.last && !!response.moreUrl)
                        .attr('href', response.nextUrl || $link.attr('href'))
                        .data('page', $link.data('page') + 1);

                    if (!!response.moreUrl) {

                        $link
                            .text(response.moreUrl.text || $link.text())
                            .attr('href', response.moreUrl.url)
                            .toggleClass('g-dashed', false)
                            .toggleClass('g-solid', true);

                        $link.off('click.loadingOnRequire');

                    }

                }
            );

        });

        // Infinity loading
        $infinityLoadingContainer.each(function() {

            var $container = $(this).data('page', $(this).data('page') || 1),
                screens = $container.data('resolutions') ? $container.data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'];

            $(window).bind('scroll.infinityLoadingOnRequire', function() {

                if (screens.indexOf(helpers.screen()) >= 0) {

                    var scroll = document.documentElement.scrollTop || document.body.scrollTop,
                        threshold = typeof options.threshold === 'string' ? parseInt(options.threshold, 10) / 100 * $(window).height() : options.threshold,

                        vpEdge = scroll + $(window).height(),
                        crEdge = $container.offset().top + $container.outerHeight() - threshold;

                    if ( !$container.data('loading') && !$container.data('last') && (vpEdge >= crEdge || $(document).height() <= vpEdge) ) {

                        $container.data('loading', true);

                        _loadContent.call($container,
                            {
                                url: $container.data('url'),
                                method: $container.data('method') || 'get',
                                data: { page: $container.data('page') || 1 },
                                onComplete: options.onComplete
                            },
                            function(response) {

                                $container.data('last', !!response.last);
                                $container.data('page', $container.data('page') + 1);

                                if (!!response.moreUrl) {

                                    $container.data('url', response.nextUrl);

                                }

                                setTimeout(function() {

                                    $container.data('loading', false);

                                }, 100);

                            }
                        );

                    }

                }

            });

        });

        function _loadContent(options, callback) {

            var $target = this,
                $wrapper = $('<div class="js-loading-wrapper"></div>'),

                isMasonry = $target.hasClass('js-masonry') && $.isFunction($.fn.isotope);

            $.ajax($.extend({}, {
                dataType: 'json',
                success: function(response) {

                    if (!options.noWrapping) {

                        $wrapper.append(response.html);
                        $wrapper.css({ opacity: 0, transition: 'opacity 400ms' });

                        $target.append($wrapper);
                        $wrapper.slideUp(0);

                        setTimeout(function() {

                            $wrapper
                                .slideDown({ duration: 400, easing: 'easeOutQuart', complete: function() {

                                    $(window).trigger('pageUp.Refresh');

                                    if (!!options.onComplete) {

                                        options.onComplete.call($wrapper, response);

                                    }

                                    if (isMasonry) {

                                        var $elements = $wrapper.find('> *');

                                        $elements.unwrap();
                                        $target.isotope('appended', $elements);

                                    } else {

                                        $wrapper.find('> *').unwrap();

                                    }

                                }})
                                .css({ opacity: '' });

                        }, 1);

                    } else {

                        var $items = $(response.html);

                        $items = $items.add($target.find('> div:last-child').filter(function() { console.log($(this).is(':hidden')); return $(this).is(':hidden'); }));
                        $items.css({ opacity: 0, transition: 'opacity 400ms' });

                        $target.append($items);
                        $items.slideUp(0);

                        $items
                            .slideDown({ duration: 400, easing: 'easeOutQuart', complete: function() {

                                $(window).trigger('pageUp.Refresh');

                                if (!!options.onComplete) {

                                    options.onComplete.call($wrapper, response);

                                }

                            }})
                            .css({ opacity: '' });

                    }

                    if (!!callback) {

                        callback(response);

                    }

                }
            }, options));

        }

    }

    function notificationBalloon() {

        var $body = $('body');

        $('.js-notice').each(function() {
            $(this).append('<div class="e-close i-icon i-close"></div>');
        });

        $body.on('click', '.js-notice [class*="close"]',function(e) {

            e.preventDefault();

            var $notice = $(this).closest('.js-notice');

            if (!!$notice.data('url')) {

                $.ajax({
                    method: 'get',
                    url: $(this).closest('.js-notice').data('url'),
                    data: $(this).closest('.js-notice').data('params') || {},
                    dataType: 'json',
                    success: function() {}
                });

            }

            $notice
                .next()
                .animate({ marginTop: 0 }, 400, 'easeOutQuart');

            $notice
                .addClass('fade')
                .slideUp({
                    duration: 400,
                    easing: 'easeOutQuart',
                    complete: function(){

                        $(this).remove();

                        //$(window).trigger('resize.asideState');
                        //$(window).trigger('resize.refreshHeaderDimensions');

                    }
                });

        });

    }

    function printer() {

        var $pp = $('.js-print-page'),
            $pt = $('.js-print-target');

        $pp.on('click', function(e) {

            e.preventDefault();
            window.print();

        });

        $pt.on('click', function(e) {

            e.preventDefault();
            targetLoad.call($(this), 'load');

        });

        function targetLoad(method) {

            var $body = $('body'),
                $this = this;

            switch (method) {

                default:
                case 'load':

                    $('#b-print-box').remove();

                    var iFrame = $('<iframe class="b-print-box" id="b-print-box" name="printBox" src="'+ this.attr('href') +'"></iframe>');

                    iFrame[0].onload = function() {

                        $this.toggleClass('processing', false);

                        //window.print();
                        this.contentWindow.focus();
                        this.contentWindow.print();

                        //modules.targetLoad.call($this, 'remove');

                    };

                    $this.toggleClass('processing', true);
                    $body.prepend(iFrame);

                    break;

                case 'complete':

                    $this.toggleClass('processing', false);

                    break;

                case 'remove':

                    $body.find('#b-print-box').remove();

                    break;

            }

        }

    }

    function definitions() {

        var $definitions = $('.e-definition');

        $definitions.each(function() {

            var $item = $(this);

            if (!$item.find('.e-definition_details').length) {

                var text = $item.data('definition'),
                    title = $item.data('definitionTitle'),

                    url = $item.data('definitionUrl'),
                    urlText = $item.data('definitionUrlText') || 'Читать в словаре',

                    $hint = $('<span class="e-definition_details"></span>');

                $hint.append('<strong>' + title + '</strong>' + text);

                if (!!url) {

                    $hint.append('<a href="' + url + '" target="_blank">' + urlText + '</a></span>');

                }

                $item.append($hint);

            }

            $(window).bind('load.definitionDetailsPosition resize.definitionDetailsPosition', function() {

                helpers.delay.call($item, function() {

                    var $hint = $item.find('.e-definition_details');

                    if (['xs', 'sm'].indexOf(helpers.screen()) >= 0) {

                        $hint.css({ left: -$item.offset().left + 15, width: $(window).width() - 30 });

                    } else {

                        $hint.css({ left: '', width: '' });

                    }

                }, 250);

            });

        });

    }

    return {
        accordions: accordions,
        dropDown: dropDown,
        definitions: definitions,
        pockets: pockets,
        printer: printer,
        spoilers: spoilers,
        tabs: tabs,
        hashNav: hashNav,
        tilesAlignment: tilesAlignment,
        swipeOfTables: swipeOfTables,
        imagesOnRetinaDisplays: imagesOnRetinaDisplays,
        loadingOnRequire: loadingOnRequire,
        notificationBalloon: notificationBalloon
    };

})(window);