module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        concat: {
            js: {
                src: [
                    '../plain/js/jquery.1.11.1.min.js',
                    '../plain/js/jquery.ui.1.11.4.js',
                    '../plain/js/jquery.ui.touch.punch.js',
                    '../plain/js/jquery.bootstrap.scrollspy.js',
                    '../plain/js/jquery.easing.1.3.js',
                    '../plain/js/jquery.flexslider.js',
                    '../plain/js/jquery.owl.carousel.js',
                    '../plain/js/jquery.perfect.scrollbar.js',
                    '../plain/js/jquery.form.js',
                    '../plain/js/jquery.placeholder.js',
                    '../plain/js/jquery.inputmask.bundle.js',
                    '../plain/js/jquery.uniform.js',
                    '../plain/js/jquery.selectric.js',
                    '../plain/js/jquery.validate.js',
                    '../plain/js/jquery.leaflet.popup.js',
                    '../plain/js/moment.js',

                    '../plain/js/main.js',
                    '../plain/js/forms.js',
                    '../plain/js/modules.js',
                    '../plain/js/plugins.js',
                    '../plain/js/maps.js',
                    '../plain/js/helpers.js',
                    '../plain/js/responsive.js'
                ],
                dest: '../plain/build/site.js'
            },
            css: {
                src: [
                    '../plain/css/fonts.css',
                    '../plain/css/icons.css',

                    '../plain/css/bootstrap.gs.css',

                    '../plain/css/jquery.uniform.css',
                    '../plain/css/jquery.selectric.css',
                    '../plain/css/jquery.owl.carousel.css',
                    '../plain/css/jquery.leaflet.popup.css',
                    '../plain/css/jquery.perfect.scrollbar.css',

                    '../plain/css/forms.css',
                    '../plain/css/main.css',
                    '../plain/css/pages.css'
                ],
                dest: '../plain/build/site.css'
            }
        },

        uglify: {
            build: {
                src: '../plain/build/site.js',
                dest: '../plain/build/site.min.js'
            },
            modernizr: {
                src: '../plain/js/modernizr.js',
                dest: '../plain/build/modernizr.min.js'
            }
        },

        autoprefixer: {
            options: {
                browsers: ['last 3 versions', 'ie 9'],
                cascade: false
            },
            multiple_files: {
                expand: true,
                flatten: true,
                src: '../plain/build/*.css',
                dest: '../plain/build/'
            }
        },

        csswring: {

            options: {
                map: false
            },

            main: {
                cwd: '../plain/build/',
                dest: '../plain/build/',
                expand: true,
                ext: '.min.css',
                src: [
                    'site.css'
                ]
            },

            ie9: {
                cwd: '../plain/css/',
                dest: '../plain/build/',
                expand: true,
                ext: '.min.css',
                src: [
                    'ie9.css'
                ]
            },

            print: {
                cwd: '../plain/css/',
                dest: '../plain/build/',
                expand: true,
                ext: '.min.css',
                src: [
                    'print.css'
                ]
            }

        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('csswring');

    grunt.registerTask('default', ['concat:js', 'uglify', 'concat:css', 'autoprefixer', 'csswring']);

};