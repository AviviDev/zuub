var sitePlugins = (function(window, undefined) {

    'use strict';

    function carousels(init) {

        init.selector = !!init.selector ? init.selector : '.b-carousel';
        init.namespace = !!init.namespace ? init.namespace + ' ' : '';

        init.options = !!init.options ? init.options : {};

        var $collection = $(init.namespace + init.selector);

        _init.call($collection);

        $(window).bind('resize.owlCarouselReInit', function() {

            helpers.delay.call($(window), function() {

                _init.call($collection);

            });

        });

        _externalControls();

        function _init() {

            if ($.isFunction($.fn.owlCarousel)) {

                this.each(function() {

                    var $carousel = $(this),
                        screens = !!$carousel.data('resolutions') ? $carousel.data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'];

                    if (screens.indexOf(helpers.screen()) >= 0) {

                        $carousel
                            .toggleClass('owl-carousel', true)
                            .owlCarousel($.extend({

                                items: 1,
                                margin: typeof $carousel.data('margin') !== 'undefined' ? $carousel.data('margin') : 20,

                                autoplay: $carousel.data('auto') || false,
                                autoplayTimeout: $carousel.data('interval') * 1000 || 10000,

                                autoHeight: typeof $carousel.data('autoHeight') !== 'object' ? $carousel.data('autoHeight') || false : false,
                                autoWidth: $carousel.data('autoWidth') || false,

                                loop: typeof $carousel.data('loop') !== 'undefined' ? $carousel.data('loop') : true,

                                mouseDrag: !!$carousel.data('mouseDrag') || false,
                                touchDrag: true,
                                pullDrag: false,
                                freeDrag: false,

                                dots: $carousel.data('dots') || false,
                                dotsEach: typeof $carousel.data('dotsEach') !== 'undefined' ? !! $carousel.data('dotsEach') : true,

                                dotsSpeed: typeof $carousel.data('dotsEach') !== 'undefined' ? 250 : 125,

                                dotClass: 'b-carousel_paging_bullet',
                                dotsClass: 'b-carousel_paging',

                                dotsContainer: $carousel.data('dotsContainer') || false,

                                nav: $carousel.data('nav') || false,
                                navText: ['', ''],
                                navClass: ['b-carousel_arrow b-carousel_arrow__prev i-icon i-arrow-prev', 'b-carousel_arrow b-carousel_arrow__next i-icon i-arrow-next'],

                                navContainer: $carousel.data('navContainer') || false,

                                smartSpeed: $carousel.data('smartSpeed') || 250,

                                scrollBar: $carousel.data('scrollBar') || false,
                                scrollBarContainer: $carousel.data('scrollBarContainer') || false,

                                scrollBarClass: 'b-carousel_scroll_bar',
                                scrollBarHandleClass: 'b-carousel_scroll_bar_handle',

                                responsive: {
                                    0: {
                                        autoWidth: !!$carousel.data('xsAuto'),
                                        autoHeight: typeof $carousel.data('autoHeight') === 'object' ? $carousel.data('autoHeight')[0] : false,
                                        items: $carousel.data('xs'),
                                        mergeFit: !!$carousel.data('mergeFit') ? $carousel.data('mergeFit').split(',')[0] : false,
                                        nav: _navShouldBe.call($carousel, $carousel.data('xs')),
                                        dots: _dotsShouldBe.call($carousel, $carousel.data('xs')),
                                        dotsEach: false,
                                        loop: _loopControl.call($carousel, $carousel.data('xs'))
                                    },
                                    480: {
                                        autoWidth: !!$carousel.data('smAuto'),
                                        autoHeight: typeof $carousel.data('autoHeight') === 'object' ? $carousel.data('autoHeight')[1] : false,
                                        items: $carousel.data('sm'),
                                        mergeFit: !!$carousel.data('mergeFit') ? $carousel.data('mergeFit').split(',')[1] : false,
                                        nav: _navShouldBe.call($carousel, $carousel.data('sm')),
                                        dots: _dotsShouldBe.call($carousel, $carousel.data('sm')),
                                        dotsEach: false,
                                        loop: _loopControl.call($carousel, $carousel.data('sm'))
                                    },
                                    768: {
                                        autoWidth: !!$carousel.data('mdAuto'),
                                        autoHeight: typeof $carousel.data('autoHeight') === 'object' ? $carousel.data('autoHeight')[2] : false,
                                        items: $carousel.data('md'),
                                        mergeFit: !!$carousel.data('mergeFit') ? $carousel.data('mergeFit').split(',')[2] : false,
                                        nav: _navShouldBe.call($carousel, $carousel.data('md')),
                                        dots: _dotsShouldBe.call($carousel, $carousel.data('md')),
                                        loop: _loopControl.call($carousel, $carousel.data('md'))
                                    },
                                    1170: {
                                        autoWidth: !!$carousel.data('lgAuto'),
                                        autoHeight: typeof $carousel.data('autoHeight') === 'object' ? $carousel.data('autoHeight')[3] : false,
                                        items: $carousel.data('lg'),
                                        mergeFit: !!$carousel.data('mergeFit') ? $carousel.data('mergeFit').split(',')[3] : false,
                                        nav: _navShouldBe.call($carousel, $carousel.data('lg')),
                                        dots: _dotsShouldBe.call($carousel, $carousel.data('lg')),
                                        loop: _loopControl.call($carousel, $carousel.data('lg'))
                                    }
                                },
                                onInitialized: function() {

                                    // Refresh external controls
                                    _disableExternalControls.call(this.$element);

                                },
                                onRefreshed: function() {

                                    // Refresh external controls
                                    _disableExternalControls.call(this.$element);

                                },
                                onChanged: function() {

                                    // Refresh external controls
                                    _disableExternalControls.call(this.$element);

                                    // Pocket spoilers
                                    var $notCurrent = this.$element.find('.owl-item').not($.proxy(function(index) { return index == this._current }, this));

                                    $notCurrent.each(function() {

                                        var $pocket = $(this).find('.js-pocket');

                                        if ($pocket.length && !!$pocket.data('range')) {

                                            var $pocketBody = $pocket.find('.js-pocket-box'),
                                                $pocketToggle = $pocket.find('.js-pocket-toggle');

                                            $pocket.toggleClass('opened', false);

                                            $pocketBody.css({ maxHeight: $pocket.data('range').min, overflow: 'hidden' });
                                            $pocketToggle.html(!!$pocketToggle.data('closed') ? $pocketToggle.data('closed') : $pocketToggle.html());

                                        }

                                    });

                                }
                            }, init.options));

                    } else {

                        if (!!$carousel.data('owlCarousel') || !!$carousel.data('owl.carousel')) {

                            $carousel.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
                            $carousel.find('.owl-stage-outer').children().unwrap();

                        }

                    }

                });

            } // isFunction end

        }  // end of init scope

        function _navShouldBe(limit) {

            var navOption = typeof this.data('nav') === 'undefined' ? true : !!this.data('nav'),
                itemsEnough = this.find('.b-carousel_item, > div').length > limit;

            return navOption && itemsEnough;

        }

        function _dotsShouldBe(limit) {

            var dotsOption = !!this.data('dots'),
                itemsEnough = this.find('.b-carousel_item, > div').length > limit;

            return dotsOption && itemsEnough;

        }

        function _loopControl(limit) {

            var loopOption = typeof this.data('loop') === 'undefined' ? true : !!this.data('loop'),
                //loopControlOption = !!this.data('loopControl'),

                itemsEnough = this.find('.b-carousel_item, > div').length >= limit;

            return loopOption && itemsEnough/*(loopControlOption ? itemsEnough : true)*/;

        }

        function _externalControls() {

            var $prev = $('.js-carousel-prev'),
                $next = $('.js-carousel-next');

            $prev.bind('click.externalOwlPrev', function(e) {

                e.preventDefault();

                if ($($(this).data('id')).hasClass('owl-carousel')) {

                    $($(this).data('id')).trigger('prev.owl.carousel');

                }

            });

            $next.bind('click.externalOwlNext', function(e) {

                e.preventDefault();

                if ($($(this).data('id')).hasClass('owl-carousel')) {

                    $($(this).data('id')).trigger('next.owl.carousel');

                }

            });

        }

        function _disableExternalControls() {

            var $externalPrev = $('.js-carousel-prev[data-id="#' + this.attr('id') + '"]'),
                $externalNext = $('.js-carousel-next[data-id="#' + this.attr('id') + '"]');

            $externalPrev.toggleClass('disabled', this.find('.b-carousel_arrow__prev').hasClass('disabled'));
            $externalNext.toggleClass('disabled', this.find('.b-carousel_arrow__next').hasClass('disabled'));

        }

    }

    function sliders(init) {

        init.selector = !!init.selector ? init.selector : '.b-slider';
        init.namespace = !!init.namespace ? init.namespace + ' ' : '';

        $(init.selector).each(function() {

            var $slider = $(this);

            if ($.isFunction($.fn.flexslider) && $slider.find(init.selector + '_inner > div').length > 1) {

                $slider.flexslider($.extend({

                    namespace: init.selector.substring(1) + '_',
                    selector: init.selector + '_inner > div',

                    controlsContainer: _defineContainer.call($slider),
                    directionContainer: _defineContainer.call($slider),

                    manualControls: $slider.data('manualControls') || '',

                    animation: $slider.data('animation') || 'slide',
                    animationLoop: $slider.data('animationLoop') || true,
                    animationSpeed: $slider.data('speed') || 400,

                    slideShow: !!$slider.data('slideShow'),
                    slideShowSpeed: $slider.data('slideShowSpeed') * 1000 || 5000,

                    keyboard: !!$slider.data('keyboard') || false,
                    controlNav: $slider.data('controlNav') || false,

                    directionNav: !!$slider.data('directionNav'),
                    directionNavClasses: ['i-icon i-chevron-prev', 'i-icon i-chevron-next'],

                    prevText: '',
                    nextText: '',

                    thumbCarousel: !!$slider.data('thumbCarousel'),
                    thumbCaptions: !!$slider.data('thumbCaptions'),
                    thumbStyle: typeof $slider.data('thumbStyle') !== 'undefined' ? $slider.data('thumbStyle') : 'img',
                    thumbCarouselSize: typeof $slider.data('thumbCarouselSize') !== 'undefined' ? $slider.data('thumbCarouselSize').split(',') : [1, 2, 3, 4, 5, 6],

                    thumbCarouselOptions: {
                        margin: 20,
                        nav: true,
                        navClass: ['b-slider_thumbs_arrow b-slider_thumbs_arrow__prev e-btn e-btn_3d e-btn_blue i-icon i-arrow-prev', 'b-slider_thumbs_arrow b-slider_thumbs_arrow__next e-btn e-btn_3d e-btn_green i-icon i-arrow-next']
                    },

                    pauseOnAction: true,
                    //progressLine: !!$slider.data('progress'),

                    smoothHeight: !!$slider.data('smoothHeight') || false,
                    showNearby: !!$slider.data('showNearby'),

                    after: function($slider) {

                        if (!$slider.playing && $slider.data('auto')) {

                            $slider.play();

                        }

                    }

                }, init.options));

            }

        }); // end of loop

        function _defineContainer() {

            return !!this.data('controlsContainer') ? ( this.data('controlsContainer') == 'parent' ? this.parent() : $(this.data('controlsContainer')) ) : this;

        }

    }

    function popUps() {

        var $page = $('.b-page'),

            $popUps = $('.js-popup'),

            $lightBoxes = $('.js-lightBox'),
            $videoBoxes = $('.js-videoBox'),

            settings = {
                animationStyleOfBox: 'scale',
                animationStyleOfChange: 'slide',

                boxHorizontalGutters: 64,
                boxVerticalGutters: 64,

                closeBtnLocation: 'overlay',
                closeBtnClass: 'i-icon i-close',

                directionBtnLocation: 'overlay',
                directionBtnClass: ['i-icon i-arrow-prev', 'i-icon i-arrow-next'],

                overlayOpacity: .5,
                scrollLocker: $page,
                scrollMode: function() {

                    return !!$page.data('isLocked') ? 'inner' : 'outer';

                }
            };

        if ($.isFunction($.fn.leafLetPopUp)) {

            $popUps.leafLetPopUp($.extend({}, settings, {
                closeBtnLocation: 'box',
                boxWidth: function() {

                    return this.data('boxWidth') || 440

                },
                afterLoad: function() {

                    forms.init('.b-leaflet');

                }
            }));

            $lightBoxes.leafLetPopUp($.extend({}, settings, {
                boxWidth: 1200,
                closeBtnLocation: 'box',
                closeBtnClass: 'i-icon i-close',
                directionBtnLocation: 'box',
                overlayOpacity: .9,
                maxHeight: function() { return $(window).height() * .75 },
                title: function() {

                    var info = false;

                    if (!!this.elements.link.attr('title')) {

                        info = '<p>' + this.elements.link.attr('title') + '</p>';

                    }

                    if (!!this.elements.link.data('time')) {

                        info += '<p><time>' + this.elements.link.data('time') + '</time></p>';

                    }

                    return info;

                },
                beforeLoad: function() {

                    this.addClass('b-leaflet__white');

                }
            }));

            $videoBoxes.leafLetPopUp($.extend({}, settings, {
                boxWidth: 1200,
                content: true,
                contentType: 'iframe',
                closeBtnLocation: 'box',
                closeBtnClass: 'i-icon i-close',
                directionBtnLocation: 'box',
                overlayOpacity: .9,
                title: function() {

                    var info = false;

                    if (!!this.elements.link.attr('title')) {

                        info = '<p>' + this.elements.link.attr('title') + '</p>';

                    }

                    if (!!this.elements.link.data('time')) {

                        info += '<p><time>' + this.elements.link.data('time') + '</time></p>';

                    }

                    return info;

                },
                beforeLoad: function() {

                    this.addClass('b-leaflet__white');

                }
            }));

            $('body').on('click', '.js-popup-close', function(e) {

                e.preventDefault();
                $popUps.leafLetPopUp('hide');

            });

        }

    }

    function onDemandPopUp(settings) {

        var $page = $('.b-page');

        if ($.isFunction($.fn.leafLetPopUp)) {

            $page.leafLetPopUp('show', $.extend({}, {
                animationStyleOfBox: 'scale',
                animationStyleOfChange: 'slide',
                boxWidth: 1000,
                boxHorizontalGutters: 25,
                boxVerticalGutters: 25,
                closeBtnLocation: 'box',
                closeBtnClass: 'i-icon i-close',
                directionBtnLocation: 'overlay',
                directionBtnClass: ['i-icon i-arrow-prev', 'i-icon i-arrow-next'],
                overlayOpacity: .5,
                scrollLocker: $page,
                afterLoad: function () {

                    forms.init('.b-leaflet');

                }
            }, settings));

        }

    }

    function scrollBars() {

        var $collection = $('.js-scrollBar');

        $collection.each(function() {

            var $scrollArea = $(this),
                screens = !!$scrollArea.data('resolutions') ? $scrollArea.data('resolutions').split(',') : ['xs', 'sm', 'md', 'lg'];

            if ($.isFunction($.fn.perfectScrollbar)) {

                _processing();
                $(window).bind('resize.perfectScrollBarRefresh', _processing);

            }

            function _processing() {

                if ($scrollArea.hasClass('ps-container')) {

                    $scrollArea.perfectScrollbar('destroy');

                }

                if (screens.indexOf(helpers.screen()) >= 0) {

                    $scrollArea.perfectScrollbar($scrollArea.data());

                }

            }

        });

    }

    function scrollSpy() {

        var $body = $('body'),
            $nav = $('.js-scrollSpy');

        if ($.isFunction($.fn.scrollspy)) {

            $(window).on('load.scrollSpy', function() {

                $nav.each(function() {

                    var $this = $(this);

                    $body.scrollspy({
                        offset: function() {

                            return $this.data('offset') || (($body.data('hashNavOffset') || 0) + 20);

                        }
                    });

                });

            });

        }

    }

    function toolTips() {

        if ($.isFunction($.fn.tooltip)) {

            $('[data-toggle="tooltip"]').tooltip();

        }

    }

    return {
        carousels: carousels,
        onDemandPopUp: onDemandPopUp,
        popUps: popUps,
        sliders: sliders,
        scrollBars: scrollBars,
        scrollSpy: scrollSpy,
        toolTips: toolTips
    };

})(window);