<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(empty($arItem["~DETAIL_TEXT"]) && empty($arItem["~PREVIEW_TEXT"])):?>
    <div class="b-carousel_item">

        <div class="b-work">
            <div class="b-work_gallery_area js-works">

                <div class="b-work_gallery_area_wrapper js-works-wrapper">
                    <div class="b-work_gallery_area_pic before js-works-page" id="service-work-<?=$arItem["ID"]?>-before" title="До">

                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="До" />

                    </div>
                    <div class="b-work_gallery_area_pic after js-works-page" id="service-work-<?=$arItem["ID"]?>-after" title="После">

                        <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="После" />

                    </div>
                </div>

                <div class="b-work_gallery_area_switch js-works-toggle">
                    <div class="b-work_gallery_area_switch_list">

                        <a href="#service-work-<?=$arItem["ID"]?>-before" class="e-btn">До</a>
                        <a href="#service-work-<?=$arItem["ID"]?>-after" class="e-btn current">После</a>

                    </div>
                </div>

            </div>
        </div>

    </div>
<?else:?>
    <div class="b-carousel_item" data-merge="2">

        <article class="b-work" id="service-work-<?=$arItem["ID"]?>">
            <div class="b-work_container row">

                <div class="b-work_gallery">

                    <div class="b-work_gallery_area js-works">

                        <div class="b-work_gallery_area_wrapper js-works-wrapper">
                            <div class="b-work_gallery_area_pic before js-works-page" id="service-work-<?=$arItem["ID"]?>-before" title="До">

                                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="До" />

                            </div>
                            <div class="b-work_gallery_area_pic after js-works-page" id="service-work-<?=$arItem["ID"]?>-after" title="После">

                                <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="После" />

                            </div>
                        </div>

                        <div class="b-work_gallery_area_switch js-works-toggle">
                            <div class="b-work_gallery_area_switch_list">

                                <a href="#service-work-<?=$arItem["ID"]?>-before" class="e-btn">До</a>
                                <a href="#service-work-<?=$arItem["ID"]?>-after" class="e-btn current">После</a>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="b-work_info js-tabs">

                    <div class="b-work_info_tabs b-tabs js-tabs-toggle">
                        <a href="#service-work-<?=$arItem["ID"]?>-hints" class="e-btn current">Отзыв пациента</a>
                        <a href="#service-work-<?=$arItem["ID"]?>-diagnosis" class="e-btn">Комментарий врача</a>
                    </div>

                    <div class="b-wysiwyg js-tabs-wrapper js-transition">
                        <div class="js-tabs-page" id="service-work-<?=$arItem["ID"]?>-hints">

                            <h6><?=$arItem["PROPERTIES"]["CLIENT"]["VALUE"]?>  <?=$arItem["PROPERTIES"]["CLIENT_DATA"]["VALUE"]?> </h6>

                            <div class="js-pocket faded" data-no-animation="true" data-resolutions="xs,sm,md,lg" data-lines="5,5,5,5">

                                <div class="js-pocket-box">
                                    <div class="js-pocket-box-inner">
                                        <?=$arItem["~PREVIEW_TEXT"]?>
                                    </div>
                                </div>

                                <div class="js-pocket-toggle" data-opened="Свернуть" data-closed="Читать дальше">Читать дальше</div>

                            </div>

                        </div>
                        <div class="js-tabs-page" id="service-work-<?=$arItem["ID"]?>-diagnosis">

                            <h6><?=$arItem["PROPERTIES"]["RESPONDENT"]["VALUE"]?></h6>

                            <div class="js-pocket faded" data-no-animation="true" data-resolutions="xs,sm,md,lg" data-lines="5,5,5,5">

                                <div class="js-pocket-box">
                                    <div class="js-pocket-box-inner">
                                        <?=$arItem["~DETAIL_TEXT"]?>
                                    </div>
                                </div>

                                <div class="js-pocket-toggle" data-opened="Свернуть" data-closed="Читать дальше">Читать дальше</div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </article>

    </div>
<?endif;?>
