<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<section class="b-service_assistant b-section b-section__closed<?if($isGray):?> b-section__gray<?endif;?>" id="service-assistant">

	  <div class="b-service_assistant_buttons">
		  <div class="row">
			  <div class="col-xs-24 col-md-12">

				  <a href="/primenyaemye_tehnologii.html" class="b-service_assistant_buttons_item e-btn e-btn_block">
					  <i class="i-icon i-technologies"></i>
					  <span>Применяемые технологии</span>
				  </a>

			  </div>
			  <div class="col-xs-24 col-md-12">

				  <a href="/bezopasnost_pacienta.html" class="b-service_assistant_buttons_item e-btn e-btn_block">
					  <i class="i-icon i-safety"></i>
					  <span>безопасность пациента</span>
				  </a>

			  </div>
		  </div>
	  </div>

	  <div class="row">
		  <div class="col-xs-24 col-md-20 col-md-offset-2 col-lg-16 col-lg-offset-4">

			  <div class="b-service_assistant_label">Обратитесь к автоматическому помощнику в определении <br class="visible-lg" />своего заболевания по имеющимся симптомам</div>

		  </div>
		  <div class="col-xs-24 col-md-16 col-md-offset-4 col-lg-12 col-lg-offset-6">

			  <div class="b-service_assistant_form b-form b-form__white">
				  <?$APPLICATION->IncludeComponent(
						"bitrix:catalog.smart.filter", 
						"",
						array(
							"COMPONENT_TEMPLATE" => "vrachi",
							"IBLOCK_TYPE" => "content",
							"IBLOCK_ID" => "4",
							"SECTION_ID" => "",
							"SECTION_CODE" => "",
							"FILTER_NAME" => "arrFilter",
							"SEF_MODE" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_GROUPS" => "Y",
							"SAVE_IN_SESSION" => "N",
							"INSTANT_RELOAD" => "N",
							"PAGER_PARAMS_NAME" => "arrPager",
							"XML_EXPORT" => "N",
							"SECTION_TITLE" => "-",
							"SECTION_DESCRIPTION" => "-"
						),
						false
					);?>
			  </div>

		  </div>
	  </div>

  </section>