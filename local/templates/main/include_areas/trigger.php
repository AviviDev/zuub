<aside class="b-trigger clear">

    <img class="b-trigger_picture" src="/upload/trigger.jpg" alt="Запишитесь на консультацию">

    <div class="b-trigger_content">

        <div class="b-trigger_content_wrap">
            <div class="b-trigger_content_capsule">

                <h6 class="b-trigger_content_title">Запишитесь на консультацию <strong>ПРЯМО&nbsp;СЕЙЧАС</strong></h6>

                <ul class="hidden-xs hidden-sm">
                    <li>Новейшее оборудование</li>
                    <li>Уютно как дома</li>
                    <li>Профессиональный персонал</li>
                    <li>Отличные цены</li>
                </ul>

                <footer class="b-trigger_content_footer">

                    <a class="e-btn e-btn_green e-btn_md js-popup <?=$pushivent;?>" href="/forms/?form_id=1<?if(!empty($pushivent)):?>&pushivent=<?=$pushivent;?><?endif;?>">Записаться на прием</a>

                </footer>

            </div>
        </div>

    </div>

</aside>