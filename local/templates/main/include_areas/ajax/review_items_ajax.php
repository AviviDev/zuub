<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$loadCount = 1;
$iblockId = BX_INFOBLOCK_OTZYV;
$page = 1;
if(!empty($_GET["page"]))
    $page = strip_tags($_GET["page"]);
$arFilter = array("ACTIVE" => "Y", "IBLOCK_ID" => $iblockId);
if(!empty($_GET["uslugi_list"]))
    $arFilter["PROPERTY_HLB_USLUGI"] = explode(",",strip_tags($_GET["uslugi_list"]));
if(!empty($_GET["vrachi_list"]))
    $arFilter["PROPERTY_HLB_VRACHI"] = explode(",",strip_tags($_GET["vrachi_list"]));

$count = 0;
if(!empty($_GET["count"]))
    $count = intVal($_GET["count"]);
$arResult = array();
//получаем url страницы списка из настроек инфоблока
$iblockObj = CIBlock::GetByID($iblockId);
$link = "";
if($iblock = $iblockObj->GetNext())
    $link = $iblock["~LIST_PAGE_URL"];
if($count >= ($page-1)*$loadCount)
{


    $elementsObj = CIBlockElement::GetList(
        array("DATE_ACTIVE_FROM"=>"DESC"),
        $arFilter,
        false,
        array("iNumPage" => $page, "nPageSize" => $loadCount)
    );

    while($element = $elementsObj->GetNext())
    {
        if(!empty($element["ACTIVE_FROM"]))
        {
            $element["ACTIVE_FROM"] = array(
                "SHORT"	=> $DB->FormatDate($element["ACTIVE_FROM"], CSite::GetDateFormat("FULL"), "YYYY-MM-DD"),
                "RUS"	=> FormatDateFromDB($element["ACTIVE_FROM"], 'DD MMMM YYYY')
            );
        }
        $arResult[] = $element;
    }
}

ob_start();
foreach($arResult as $arItem)
{
    $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/review_item.php"), Array("arItem" => $arItem), Array("MODE"=>"php") );
}

$articles = ob_get_contents();
ob_end_clean();
$response = Array(

    'html' => $articles,                                            // {обязательный!} html-участок ленты (новостей, акци и т. п.), если лента одна// {обязательный!} html-участок ленты (новостей, акци и т. п.), если лент несколько
    'nextUrl' => '/ajax/review.php?page=' . ($page + 1) . '&uslugi_list='.$_GET["uslugi_list"].'&vrachi_list='.$_GET["vrachi_list"].'&count='.$_GET["count"],   // {опциональный} ссылка для запроса следующей страницы (по аналогии с кнопкой "Next" в стандартной пагинации)
    'last' => $count <= $page*$loadCount,                                    // {обязательный!} выставляется в true, если это последняя "пачка" и материалов больше не будет,

    'moreUrl' => ($count <= $page*$loadCount) ? Array( 'url' => $link, 'text' => 'Читать все отзывы' ) : false
    // {опциональный!} работает совместно с last: передается ссылка для перехода

);

// Ответ строго в json-формате
echo json_encode($response);
?>