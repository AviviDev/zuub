<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$loadCount = 1;
$iblockId = BX_INFOBLOCK_VOPROS_OTVET;
$page = 1;
if(!empty($_GET["page"]))
    $page = strip_tags($_GET["page"]);
$arFilter = array("ACTIVE" => "Y", "IBLOCK_ID" => $iblockId);
if(!empty($_GET["uslugi_list"]))
    $arFilter["PROPERTY_HLB_USLUGI"] = explode(",",strip_tags($_GET["uslugi_list"]));

$count = 0;
if(!empty($_GET["count"]))
    $count = intVal($_GET["count"]);
$arResult = array();
//получаем url страницы списка из настроек инфоблока
$iblockObj = CIBlock::GetByID($iblockId);
$link = "";
if($iblock = $iblockObj->GetNext())
    $link = $iblock["~LIST_PAGE_URL"];
if($count >= ($page-1)*$loadCount)
{


    $elementsObj = CIBlockElement::GetList(
        array("DATE_ACTIVE_FROM"=>"DESC"),
        $arFilter,
        false,
        array("iNumPage" => $page, "nPageSize" => $loadCount)
    );

    while($element = $elementsObj->GetNext())
    {
        if(!empty($element["PROPERTIES"]["VRACH"]["VALUE"]))
        {
            $vrachObj = CIBlockElement::GetByID($element["PROPERTIES"]["VRACH"]["VALUE"]);
            if($vrach = $vrachObj->GetNextElement())
            {
                $element["ANSWER_DOCTOR"] = $vrach->GetFields();
                if(empty($element["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]))
                    $element["ANSWER_DOCTOR"]["PREVIEW_PICTURE"] = $element["ANSWER_DOCTOR"]["DETAIL_PICTURE"];
                if(!empty($element["PREVIEW_PICTURE"]))
                {
                    $img = CFile::ResizeImageGet(
                        $element["ANSWER_DOCTOR"]["PREVIEW_PICTURE"],
                        array("width"=>290, "height"=>600),
                        BX_RESIZE_IMAGE_PROPORTIONAL,
                        false
                    );
                    if(!empty($img))
                    {
                        $element["ANSWER_DOCTOR"]["PREVIEW_PICTURE"] = array();
                        $element["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["SRC"] = $img["src"];
                        $element["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["WIDTH"] = $img["width"];
                        $element["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["HEIGHT"] = $img["height"];
                    }
                }
                $element["ANSWER_DOCTOR"]["PROPERTIES"] = $vrach->GetProperties();
            }
        }
        if(!empty($element["ACTIVE_FROM"]))
        {
            $element["ACTIVE_FROM"] = array(
                "SHORT"	=> $DB->FormatDate($element["ACTIVE_FROM"], CSite::GetDateFormat("FULL"), "YYYY-MM-DD"),
                "RUS"	=> FormatDateFromDB($element["ACTIVE_FROM"], 'DD MMMM YYYY')
            );
        }
        $arResult[] = $element;
    }
}

ob_start();
foreach($arResult as $arItem)
{
    $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/vopros_otvet_item.php"), Array("arItem" => $arItem), Array("MODE"=>"php") );
}

$articles = ob_get_contents();
ob_end_clean();
$response = Array(

    'html' => $articles,                                            // {обязательный!} html-участок ленты (новостей, акци и т. п.), если лента одна// {обязательный!} html-участок ленты (новостей, акци и т. п.), если лент несколько
    'nextUrl' => '/ajax/faq.php?page=' . ($page + 1) . '&uslugi_list='.$_GET["uslugi_list"].'&count='.$_GET["count"],   // {опциональный} ссылка для запроса следующей страницы (по аналогии с кнопкой "Next" в стандартной пагинации)
    'last' => $count <= $page*$loadCount,                                    // {обязательный!} выставляется в true, если это последняя "пачка" и материалов больше не будет,

    'moreUrl' => ($count <= $page*$loadCount) ? Array( 'url' => $link, 'text' => 'Читать все вопросы' ) : false
    // {опциональный!} работает совместно с last: передается ссылка для перехода

);

// Ответ строго в json-формате
echo json_encode($response);
?>