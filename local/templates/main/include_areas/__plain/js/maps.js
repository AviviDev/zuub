var YandexMaps = (function(window, undefined) {

    'use strict';

    function init() {

        // Simple map
        YandexMaps.regular({
            selector: '.b-map'
        });

    }

    function regular(settings) {

        if (!!ymaps) {

            ymaps.ready(function () {

                // Custom prototype functions
                ymaps.Map.prototype.setCenterWithOffset = function(horizontal, vertical) {

                    var projection = this.options.get('projection'),
                        pixels = projection.toGlobalPixels(this.getCenter(), this.getZoom());

                    pixels[0] = pixels[0] - horizontal;
                    pixels[1] = pixels[1] - vertical;

                    this.setCenter(projection.fromGlobalPixels(pixels, this.getZoom()));

                };

                ymaps.Map.prototype.addCustomPlaceMark = function(data, collection, isLast) {

                    var map = this,

                        properties = {},
                        options = {
                            hideIconOnBalloonOpen: false,
                            balloonShadow: false,
                            balloonAutoPan: false
                        };

                    // Set place mark properties
                    properties.el = data.el;
                    $.extend(properties, data.properties);

                    // Placemark layout
                    if (data.mark) {

                        /*data.markLayout =
                            '<div class="b-map_marker[if properties.active] active[endif][if properties.labelPosition] $[properties.labelPosition][endif]"[if properties.label] data-label="$[properties.label]"[endif]>' +
                                '<svg width="38px" height="38px" viewBox="0 0 38 38" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">' +
                                    '<defs>' +
                                    '<filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-1">' +
                                        '<feOffset dx="1" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>' +
                                        '<feGaussianBlur stdDeviation="1.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>' +
                                        '<feColorMatrix values="0 0 0 0 0.419607843   0 0 0 0 0.133333333   0 0 0 0 0.270588235  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>' +
                                        '<feMerge> <feMergeNode in="shadowMatrixOuter1"></feMergeNode> <feMergeNode in="SourceGraphic"></feMergeNode></feMerge>' +
                                    '</filter>' +
                                    '</defs>' +
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">' +
                                    '<g id="logo" filter="url(#filter-1)" transform="translate(2.000000, 2.000000)"> <path d="M31.690928,15.7974865 C31.7102734,22.2106124 27.8619879,28.0030246 21.9425755,30.4706378 C16.0231631,32.9382511 9.20000689,31.5944209 4.65837663,27.0664905 C0.116746385,22.5385602 -1.24769089,15.7194949 1.2020282,9.7926547 C3.65174729,3.86581447 9.4325071,4.72809774e-05 15.8456622,1.81031156e-05 C20.0417511,-0.00632502465 24.0684984,1.65448395 27.0400632,4.617085 C30.011628,7.57968604 31.6845945,11.6013975 31.690928,15.7974865 L31.690928,15.7974865 L31.690928,15.7974865 Z" id="Shape" fill="#C54080"></path> <path d="M31.3676875,15.7974865 C31.3873613,22.0799456 27.6180404,27.7547403 21.8194421,30.1725777 C16.0208439,32.5904151 9.33658714,31.2744414 4.88723475,26.8390222 C0.437882378,22.4036031 -0.899049584,15.7235067 1.50058908,9.9173536 C3.90022774,4.11120053 9.56317229,0.324099541 15.8456622,0.324068736 C24.4047295,0.310859433 31.3540519,7.23841985 31.3676875,15.7974865 L31.3676875,15.7974865 L31.3676875,15.7974865 Z" id="Shape" fill="#FD9FBF"></path> <path d="M29.7506749,15.7974865 C29.767561,21.425305 26.3904406,26.5083678 21.195853,28.6737514 C16.0012654,30.839135 10.0136413,29.6598015 6.02818009,25.6862986 C2.04271889,21.7127956 0.8453981,15.7287424 2.99516235,10.5276713 C5.14492658,5.32660022 10.2178183,1.93422065 15.8456622,1.93419532 C19.5279859,1.9286048 23.0617057,3.38607053 25.669414,5.98595447 C28.2771222,8.58583842 29.7452001,12.1151626 29.7506749,15.7974865 L29.7506749,15.7974865 L29.7506749,15.7974865 Z" id="Shape" fill="#C54080"></path> <path d="M19.9538141,7.80882823 C19.8603453,9.89221954 19.3216786,11.9314182 18.3740673,13.7891827 C18.2339154,14.0626004 18.0933584,14.4466004 17.7154344,14.3886763 C17.3375103,14.3307523 17.3103711,13.9204232 17.2467761,13.6190561 C17.066523,12.7591067 16.9433837,11.8866004 16.7546242,11.026651 C16.6707761,10.6454865 16.5492572,10.1829042 16.0449534,10.1861447 C15.5406496,10.1893852 15.4304723,10.6596637 15.3527002,11.039613 C15.1817635,11.8740434 15.0853584,12.7234611 14.9188774,13.5587017 C14.8492065,13.9090814 14.8078901,14.3894865 14.3574597,14.4308029 C13.9750799,14.4652333 13.8073837,14.0528789 13.6481939,13.7551573 C12.531973,11.7704713 11.9358676,9.53570333 11.9153331,7.25875228 C11.9258647,5.27394216 13.0900166,4.51120798 14.9128015,5.27839785 C15.494055,5.55590692 16.1643933,5.5798161 16.7639407,5.34442317 C18.8483964,4.52011937 19.9133078,5.24194216 19.9534091,7.48558773 C19.9554344,7.59535988 19.9538141,7.70189152 19.9538141,7.80882823 L19.9538141,7.80882823 L19.9538141,7.80882823 Z M20.6100166,23.4309042 C18.9348427,22.1942248 17.5450148,20.6119677 16.5346749,18.7913092 C16.3860166,18.5223472 16.1462192,18.1918156 16.4038394,17.9086763 C16.6614597,17.6255371 17.0167002,17.829689 17.3002445,17.9439168 C18.1128015,18.271613 18.9038901,18.6527776 19.7192825,18.9719675 C20.081004,19.1137396 20.5318394,19.2688789 20.8093078,18.8459928 C21.0867761,18.4231067 20.7582698,18.0698915 20.4852572,17.7928282 C19.8894091,17.1852333 19.2392825,16.6339422 18.6401939,16.031208 C18.3890546,15.7784485 18.013966,15.4766763 18.2298647,15.0784991 C18.4133584,14.7398662 18.8479913,14.8285751 19.1829787,14.8609801 C21.4461775,15.0309464 23.6288012,15.7745189 25.5250546,17.0215877 C27.1627255,18.1322713 27.1481432,19.5269042 25.5003458,20.6234105 C24.9472344,20.9562964 24.5557283,21.5023829 24.4180166,22.1330814 C23.9428774,24.3313599 22.7536116,24.8202713 20.8737129,23.6075118 C20.7862192,23.551208 20.6995356,23.4904485 20.6100166,23.4309042 L20.6100166,23.4309042 L20.6100166,23.4309042 Z M11.4049534,23.4309042 C13.0801359,22.1942341 14.4699656,20.611975 15.4802951,18.7913092 C15.6289534,18.5223472 15.8687508,18.1918156 15.6111306,17.9086763 C15.3535103,17.6255371 14.9982698,17.829689 14.7147255,17.9439168 C13.9021685,18.271613 13.1110799,18.6527776 12.2956875,18.9719675 C11.933561,19.1137396 11.4831306,19.2688789 11.2056622,18.8459928 C10.9281939,18.4231067 11.2567002,18.0698915 11.5297129,17.7928282 C12.125561,17.1852333 12.7752825,16.6339422 13.3747761,16.031208 C13.6259154,15.7784485 14.001004,15.4766763 13.7851053,15.0784991 C13.6016116,14.7398662 13.1669787,14.8285751 12.8319913,14.8609801 C10.5687853,15.0309112 8.38615299,15.7744866 6.48991538,17.0215877 C4.8522445,18.1322713 4.86642171,19.5269042 6.51421918,20.6234105 C7.06733051,20.9562964 7.45883658,21.5023829 7.59654829,22.1330814 C8.07168753,24.3313599 9.26095336,24.8202713 11.1408521,23.6075118 C11.2291559,23.551208 11.3166496,23.4904485 11.4053584,23.4309042 L11.4049534,23.4309042 L11.4049534,23.4309042 Z" id="Shape" fill="#FFFFFF"></path>' +
                                    '</g>' +
                                    '</g>' +
                                '</svg>' +
                            '</div>';*/

                        data.markLayout =
                            '<div class="b-map_marker[if properties.active] active[endif][if properties.labelPosition] $[properties.labelPosition][endif]"[if properties.label] data-label="$[properties.label]"[endif]>' +
                                '<img src="' + data.markImg + '" alt="" />' +
                            '</div>';

                    }

                    // Set place mark options
                    if (data.mark && !!data.markLayout) {

                        var markerLayoutId = 'ZuubMarker#marker' + helpers.randomString(4);

                        ymaps.layout.storage.add(markerLayoutId, ymaps.templateLayoutFactory.createClass(data.markLayout));

                        $.extend(options, {
                            iconLayout: markerLayoutId,
                            iconShape: {
                                type: 'Rectangle',
                                coordinates: [
                                    [-18, -18], [20, 20]
                                ]
                            }
                        });

                    } else if (data.mark && data.markImg) {

                        $.extend(options, {
                            iconLayout: 'default#image',
                            iconImageHref: ('devicePixelRatio' in window && window.devicePixelRatio > 1) ? data.markImg.replace('.png', '@2x.png') : data.markImg,
                            iconImageClipRect: [[214, 0], [249, 49]],
                            iconImageSize: [35, 49],
                            iconImageOffset: [-18, -49]
                        });

                    } else if (!data.mark) {

                        $.extend(options, {
                            preset: 'islands#blueDotIcon'
                        });

                    }

                    var placeMark = new ymaps.Placemark(data.coordinates, properties, options);

                    placeMark.events.add('click', function () {

                        map.panTo(placeMark.geometry.getCoordinates(), { delay: 0 });

                    });

                    placeMark.events.add(['balloonopen'], function() {

                        placeMark.properties.set('active', true);

                    });

                    placeMark.events.add(['balloonclose'], function() {

                        placeMark.properties.set('active', false);

                    });

                    // Add collection
                    collection.add(placeMark);

                    map.geoObjects.add(collection);
                    map.setBounds(collection.getBounds());

                    // Set zoom
                    if (collection.getLength() == 1)  {

                        map.setZoom(map.el.data('zoom') || 14);

                    } else {

                        if (typeof map.el.data('timer') != 'undefined') {

                            clearTimeout(map.el.data('timer'));

                        }

                        map.el.data('timer', setTimeout(function() {

                            map.setZoom(map.getZoom() - 1);

                        }, 100));

                    }

                    // View on map triggers
                    //$('.js-view-on-map').toggleClass('show', isLast);

                    $('body')
                        .off('click.viewOnMap')
                        .on('click.viewOnMap', '.js-view-on-map', function() {

                            var id = $(this).data('id') || '';

                            collection.each(function(item) {

                                item.properties.set('active', false);

                                if (item.properties.get('id') == id) {

                                    item.properties.set('active', true);
                                    item.balloon.open();

                                    setTimeout(function() {

                                        map.setCenter(item.geometry.getCoordinates(), 16);

                                    }, 300);

                                }

                            });

                    });

                };

                $(settings.selector).each(function() {

                    var $map = $(this).empty(),

                        map = new ymaps.Map($map.attr('id'), {
                            center: [0, 0],
                            zoom: $map.data('zoom') || 16,
                            controls: $map.data('controls') || ['zoomControl'/*, 'typeSelector', 'trafficControl'*/]
                        }),

                        collection = new ymaps.GeoObjectCollection(),
                        placeMarksStorage = [];

                    map.el = $map;

                    $map.data('yMapInst', map);

                    if (typeof $map.data('controls') === 'undefined') {

                        map.controls.add('zoomControl', {
                            position: {
                                top: ($map.height() - 206) / 2,
                                right: 20
                            }
                        });

                    }

                    map.behaviors.disable('scrollZoom');

                    // Get points data
                    if (!!$map.data('address') || !!$map.data('coordinates')) {

                        postProcessingPoints(processingPoint.call($map), placeMarksStorage);

                    } else if (!!$map.data('cards')) {

                        $($map.data('cards')).each(function() {

                            placeMarksStorage.push(processingPoint.call($(this)));

                        });

                    }

                    // Add place marks on map
                    addPointsOnMap.call(map, placeMarksStorage, collection);

                });

            }); //ymaps.ready end scope

        }

    }

    function processingPoint() {

        return {
            address: !!this.data('address') ? this.data('address') : null,
            coordinates: !!this.data('coordinates') ? this.data('coordinates') : null,
            el: this,
            mark: !!this.data('customMark'),
            markImg: this.data('customMarkImg') || '/img/icons/placemark.svg',
            markLayout: !!this.data('customMarkClass') ? '<div class="' + this.data('customMarkClass') + ' b-map_marker i-icon"></div>' : null,
            properties: {
                id: this.data('id') || null,
                url: this.data('url') || null,
                label: this.data('label') || false,
                labelPosition: this.data('labelPosition') || 'right',
                balloonContent: $('<div />').append(this.clone().addClass('b-map_balloon')).html(),
                active: false
            }
        };

    }

    function postProcessingPoints(data, storage) {

        data.properties = null;

        var i = 0;

        if (data.coordinates) {

            for (i = 0; i < data.coordinates.length; i++) {

                storage.push($.extend({}, data, {
                    address: null,
                    coordinates: data.coordinates[i]
                }));

            }

        }

        if (data.address) {

            for (i = 0; i < data.address.length; i++) {

                storage.push($.extend({}, data, {
                    address: data.address[i],
                    coordinates: null
                }));

            }

        }

    }

    function addPointsOnMap(objects, collection) {

        var map = this;

        $.each(objects, function(i, item) {

            // Objects with coordinates
            if (item.coordinates) {

                if (typeof item.coordinates[0] === 'object') {

                    item.coordinates = item.coordinates[0];

                }

                map.addCustomPlaceMark(item, collection, objects.length == i + 1);

            }

            // Add objects with addresses only (via geoCoder)
            else if (item.address) {

                ymaps
                    .geocode(item.address, { results: 1 })
                    .then(function(response) {

                        var coordinates = response.geoObjects.get(0).geometry.getCoordinates();

                        item.address = null;
                        item.coordinates = coordinates;

                        map.addCustomPlaceMark(item, collection, objects.length == i + 1);

                    });

            }

        });

    }

    function fitAllMaps() {

        $('.b-map').each(function() {

            $(this).data('yMapInst').container.fitToViewport();

        });

    }

    return {
        init: init,
        regular: regular,
        fitAllMaps: fitAllMaps
    };

})(window);
