$(document).ready(function() {

    // User interface
    site.init();

    // UI modules
    siteModules.printer();
    siteModules.hashNav();
    siteModules.tilesAlignment();
    siteModules.notificationBalloon();

    siteModules.dropDown({
        selector: 'js-dropDown'
    });

    siteModules.loadingOnRequire({
        btn: '.js-loading-on-require',
        container: '.js-infinity-loading',
        threshold: '15%',
        onComplete: function() {

            // Some action

        }
    });

    // Init plugins
    sitePlugins.popUps();
    sitePlugins.toolTips();

    sitePlugins.sliders({
        selector: '.b-slider'
    });

    sitePlugins.carousels({
        selector: '.b-carousel'
    });

    // Init UI modules
    siteModules.definitions();

    siteModules.accordions({
        selector: '.js-accordion'
    });

    siteModules.tabs({
        selector: '.js-tabs'
    });

    siteModules.tabs({
        selector: '.js-works'
    });

    siteModules.spoilers({
        selector: '.js-spoiler'
    });

    siteModules.pockets({
        selector: '.js-pocket',
        duration: 250
    });

    sitePlugins.scrollBars();
    sitePlugins.scrollSpy();

    // Init forms handlers
    forms.init('.b-page');

    // Init responsive helpers
    siteResponsive.init('.b-page');

});

// UI
var site = (function(window, undefined) {

    'use strict';

    function onLoadPage() {

        $(window).bind('load.pageReady', function() {

            // VK API
            helpers.async('//vk.com/js/api/openapi.js?122');

            // Facebook API
            helpers.async('//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.6');

            // reCaptcha API
            helpers.async('https://www.google.com/recaptcha/api.js?onload=reCaptchaOnLoadCallback&render=explicit');

            // YMaps API
            helpers.async('http://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU&onload=YandexMaps.init');

        });

    }

    function targetBlank() {

        $('a[data-target="_blank"]')
            .on('click', function() {

                return !window.open($(this).attr('href'));

            });

    }

    function orderedLists() {

        $('ol[start]').each(function() {

            $(this).css({ counterReset: 'list ' + (parseInt($(this).attr('start')) - 1) });

        });

    }

    function mainMenu() {

        // Main menu toggle
        siteModules.dropDown({
            selector: 'js-mainNav',
            switch: 'showNav',
            onToggle: function() {

                this.closest('.js-mainNav').data('currentScrollPosition', document.documentElement.scrollTop || document.body.scrollTop);

            }
        });

        var $menuTriggers = $('.js-menu-trigger'),

            touches = {
                touchstart: {x: -1, y: -1 },
                touchmove: { x: -1, y: -1 }
            };

        // Swipe listeners
        $(document).bind('touchstart.menuSwipe touchmove.menuSwipe', function(e) {

            var touch = e.originalEvent.touches[0];

            touches[e.type].x = touch.pageX;
            touches[e.type].y = touch.pageY;

        });

        // Main nav levels
        $menuTriggers.each(function() {

            var $item = $(this),
                $target = $('.js-menu' + $item.data('target') || '.js-menu#default');

            $item.on('click', function(e) {

                e.preventDefault();

                var $targetTrigger = $('[data-target="#' + $target.attr('id') + '"]').parent();
                    $targetTrigger = $targetTrigger.closest('.js-menu').length ? $('[data-target="#' + $targetTrigger.closest('.js-menu').attr('id') + '"]').parent() : $targetTrigger;

                $('.js-menu').not($target).each(function() {

                    $(this).toggleClass('opened', false);

                });

                $('.js-menu-trigger').not($item).each(function() {

                    $(this).parent().toggleClass('opened', false);

                });

                if (!$item.data('changed')) {

                    $target.toggleClass('opened');
                    $targetTrigger.toggleClass('opened', $target.hasClass('opened'));

                    $target.data('currentScrollPosition', document.documentElement.scrollTop || document.body.scrollTop);

                } else {

                    $item.data('changed', false);
                    $targetTrigger.toggleClass('opened', false);

                }

                $('.js-mainNav-box').toggleClass('off');

            });

            $target.find('.js-menu-change').on('click', function(e) {

                e.preventDefault();

                $item.data('changed', true);
                $target.toggleClass('opened', false);

            });

            if (!!$item.data('targetPosition')) {

                _menuPosition.call($item);

                $(window).bind('resize.menuPosition', function() {

                    helpers.delay.call($item, function() {

                        _menuPosition.call($item);

                    }, 250);

                });

            }

        });

        function _menuPosition() {

            var $menu = this.closest('.js-menu'),

                offset = this.position().top,
                parentOffset = $menu.length ? parseInt($menu.css('top'), 10) : false,

                $target = $('.js-menu' + this.data('target') || '.js-menu#default');

            $target.css({
                top: ['md', 'lg'].indexOf(helpers.screen()) >= 0 ? parentOffset || offset : '',
                bottom: 'auto',
                marginTop: ''
            });

            if (['lg'].indexOf(helpers.screen()) >= 0 && ($target.position().top + $target.outerHeight()) > ($(window).height() - 70)) {

                $target.css({
                    top: 'auto',
                    bottom: 0,
                    marginTop: 'auto'
                });

            }

        }

        /* deprecated: Позиционирование меню всегда по верху
        function _menuPosition() {

            var $menu = this.closest('.js-menu'),

                offset = $('.js-nav').position().top,*//*this.position().top,*//*
                parentOffset = $menu.length ? parseInt($menu.css('top'), 10) : false;

            //parentOffset = !!this.data('targetPositionParent') ? parseInt($(this.data('targetPositionParent')).css('top'), 10) : false;

            $('.js-menu' + this.data('target') || '.js-menu#default').css({
                top: ['md', 'lg'].indexOf(helpers.screen()) >= 0 ? parentOffset || offset : '',
                marginTop: 'auto'
            });

        }
        */

        $(document).bind((navigator.userAgent.match(/iPhone|iPad|iPod/i)) ? 'touchend.menu' : 'click.menu', function(e) {

            var $target = $(e.target),

                targetIsMenu = $target.hasClass('js-menu') || $target.closest('.js-menu').length,
                targetIsTrigger = $target.hasClass('js-menu-trigger') || $target.closest('.js-menu-trigger').length,
                targetIsClose = $target.hasClass('js-menu-close') || $target.closest('.js-menu-close').length,

                isSwipeAction = touches.touchmove.y > -1 && (Math.abs(touches.touchstart.y - touches.touchmove.y) > 5);

            if ((!targetIsMenu || targetIsClose) && !targetIsTrigger && !isSwipeAction) {

                $('.js-menu').each(function() {

                    $(this).toggleClass('opened', false);

                });

                $('.js-menu-trigger').each(function() {

                    $(this).data('changed', false);
                    $(this).parent().toggleClass('opened', false);

                });

                $('.js-mainNav-box').each(function() {

                    $(this).toggleClass('off', false);

                });

            }

            touches = {
                touchstart: {x: -1, y: -1 },
                touchmove: { x: -1, y: -1 }
            };

        });

        // Close on scroll
        $(window).on('scroll.closeDesktopMenu', function() {

            if (!helpers.mobile() && ['xs', 'sm', 'md', 'lg'].indexOf(helpers.screen()) >= 0) {

                var $nav = $('.js-mainNav.showNav'),

                    $openedMenu = $nav.length ? $nav : $('.js-menu.opened'),
                    startingPosition = $openedMenu.data('currentScrollPosition'),

                    threshold = 10;

                if (typeof startingPosition !== 'undefined' && Math.abs(startingPosition - (document.documentElement.scrollTop || document.body.scrollTop)) > threshold) {

                    $nav.trigger('dropDown.close');

                    $('.js-menu').each(function() {

                        $(this).toggleClass('opened', false);

                    });

                    $('.js-menu-trigger').each(function() {

                        $(this).data('changed', false);
                        $(this).parent().toggleClass('opened', false);

                    });

                    $('.js-mainNav-box').each(function() {

                        $(this).toggleClass('off', false);

                    });

                }

            }

        });

        // Correct mobile scroll on search focus
        $('body').on('focus', '.b-header_search_field input', function() {

            if (!!helpers.mobile() && ['xs', 'sm', 'md'].indexOf(helpers.screen()) >= 0) {

                var $headerNav = $('.b-header_nav'),
                    height = $headerNav[0].scrollHeight;

                $headerNav.animate({ scrollTop: height }, 250, function() {
                    $headerNav.scrollTop(height);
                });

                setTimeout(function() {

                    $headerNav.animate({ scrollTop: height }, 10, function() {
                        $headerNav.scrollTop(height);
                    });

                }, 1000);

            }

        });

    }

    function servicesMenu() {

        var $accordionsToggle = $('.b-header_services_header_toggle');

        $accordionsToggle.on('click', function(e) {

            e.preventDefault();

            var $toggle = $(this),
                $services = $toggle.closest('.b-header_services');

            $services
                .find('.js-accordion')
                .trigger(!$toggle.hasClass('active') ? 'accordion.open' : 'accordion.close');

            $toggle
                .text(!$toggle.hasClass('active') ? $toggle.data('opened') : $toggle.data('closed'))
                .toggleClass('active', !$toggle.hasClass('active'));

        });

    }

    function header() {

        var $page = $('.b-page_container'),

            $header = $('.b-header'),
            $notice = $('.b-notice'),

            $mainBox = $('.b-main_box'),
            $services = $('.b-header_services');

        _processing();

        $header.on('header.refresh', _processing);

        $(window).bind('load.refreshHeaderDimensions resize.refreshHeaderDimensions', _processing);

        function _processing() {

            if (['lg'].indexOf(helpers.screen()) >= 0) {

                $page.css({ paddingTop: $notice.outerHeight() });
                $header.css({ paddingTop: $notice.outerHeight() + parseInt($mainBox.closest('.b-main').css('padding-top'), 10), width: $('.b-main').css('margin-left')/*, height: $(window).height() - $notice.outerHeight()*/ });

            } else {

                $page.css({ paddingTop: $header.outerHeight() });
                $header.css({ paddingTop: '', width: ''/*, height: ''*/ });

            }

            if (['lg'].indexOf(helpers.screen()) >= 0) {

                $services.css({ width: $mainBox.outerWidth(), height: '' });

            }
            else {

                $services.css({ width: $mainBox.width(), height: $(window).height() - $header.outerHeight() - (parseInt($services.css('margin-top'), 10) * 2) });

            }

            if (['lg'].indexOf(helpers.screen()) >= 0) {

                $('.js-mainNav-box, .js-menu').css({ overflowY: '', overflowX: '', maxHeight: '' });

            }
            else {

                $('.js-mainNav-box, .js-menu').each(function() {

                    $(this).css({ overflowY: 'auto', overflowX: 'hidden', maxHeight: $(window).height() - $header.outerHeight() - 30 });

                });

            }

        }

        $(window).bind('load.asideState scroll.asideState resize.asideState', function() {

            var pageHasLeaflet = $('.b-leaflet').length,

                scroll = pageHasLeaflet ? -parseInt($('.b-leaflet_locker').css('margin-top'), 10) : document.documentElement.scrollTop || document.body.scrollTop,
                offset = $notice.outerHeight() - scroll;

            offset = offset > 0 ? offset : 0;

            if (['lg'].indexOf(helpers.screen()) >= 0) {

                //$header.css({ height: $(window).height() - offset });
                //$header.toggleClass('b-header__fixed', offset == 0);

                $header.css({ paddingTop: offset + parseInt($mainBox.closest('.b-main').css('padding-top'), 10) });

            } else {

                $header.css({ paddingTop: '', width: '', height: '' });

            }

        });

    }

    function headerStrip() {

        var $body = $('body');

        $('.js-headerStrip').each(function() {
            $(this).append('<div class="e-close i-icon i-close"></div>');
        });

        $body.on('click', '.js-headerStrip [class*="close"]',function(e) {

            e.preventDefault();

            var $page = $('.b-page_container'),
                $header = $('.b-header'),

                $mainBox = $('.b-main_box'),
                $footer = $('.b-footer', $page),

                $notice = $(this).closest('.js-headerStrip');

            $notice.css({ maxHeight: $notice.outerHeight(), overflow: 'hidden' });

            if (!!$notice.data('url')) {

                $.ajax({
                    method: 'get',
                    url: $(this).closest('.js-notice').data('url'),
                    data: $(this).closest('.js-notice').data() || {},
                    dataType: 'json',
                    success: function() {}
                });

            }

            setTimeout(function() {

                if ($notice.closest('.b-header').length) {

                    $page
                        .css(helpers.pfx + 'transition', 'padding-top 400ms')
                        .css('transition', 'padding-top 400ms')
                        .css('padding-top', ['xs', 'sm', 'md'].indexOf(helpers.screen()) >= 0 ? $('.b-header_container').outerHeight() : 0);

                } else {

                    $page
                        .css(helpers.pfx + 'transition', 'padding-top 400ms')
                        .css('transition', 'padding-top 400ms')
                        .css('padding-top', 0);

                    $mainBox
                        .css(helpers.pfx + 'transition', 'min-height 400ms')
                        .css('transition', 'min-height 400ms')
                        .css({ minHeight: $page.outerHeight() <= $(window).height() ? $(window).height() - $footer.outerHeight() - (['lg'].indexOf(helpers.screen()) >= 0 ? parseInt($('.b-main').css('padding-top'), 10) : 0): 0 });

                    /*$header
                        .css(helpers.pfx + 'transition', 'height 400ms')
                        .css('transition', 'height 400ms')
                        .css('height', $(window).height() );*/

                    $header
                        .css(helpers.pfx + 'transition', 'padding-top 400ms')
                        .css('transition', 'padding-top 400ms')
                        .css('padding-top', parseInt($mainBox.closest('.b-main').css('padding-top'), 10) );

                }

                $notice
                    .addClass('fade')
                    .css(helpers.pfx + 'transition', 'max-height 400ms, opacity 250ms, visibility 250ms')
                    .css('transition', 'max-height 400ms, opacity 250ms, visibility 250ms')
                    .css({ maxHeight: 0 });

                setTimeout(function() {

                    $('.js-headerStrip').remove();

                    $page
                        .css(helpers.pfx + 'transition', '')
                        .css('transition', '');

                    $mainBox
                        .css(helpers.pfx + 'transition', '')
                        .css('transition', '');

                    $header
                        .css(helpers.pfx + 'transition', '')
                        .css('transition', '');

                    $(window).trigger('resize.asideState');
                    $(window).trigger('resize.refreshHeaderDimensions');

                }, 450);

            }, 25);

        });

    }

    function footerBottom() {

        var $page = $('.b-page_container'),
            $mainBox = $('.b-main_box'),
            $footer = $('.b-footer', $page),

            screens = ['xs', 'sm', 'md', 'lg'];

        _processing();

        $footer.on('footer.refresh', _processing);

        $(window).bind('resize.refreshFooterPosition', _processing);

        function _processing() {

            if (screens.indexOf(helpers.screen()) >= 0) {

                $page.css({ paddingBottom: $footer.outerHeight() });
                $mainBox.css({ minHeight: $page.outerHeight() <= $(window).height() ? $page.height() - (['lg'].indexOf(helpers.screen()) >= 0 ? parseInt($('.b-main').css('padding-top'), 10) : 0) : 0 });
                $footer.css({ position: 'absolute', zIndex: 5, left: 0, bottom: 0, right: 0, minWidth: 320 })

            } else {

                $page.css({ paddingBottom: '' });
                $mainBox.css({ minHeight: '' });
                $footer.css({ position: '', zIndex: '', left: '', bottom: '', right: '', minWidth: '' });

            }

        }

    }

    function benefits($namespace, $container) {

        $namespace = !!$namespace && !!$namespace.length ? $namespace : $('.b-page');

        var $grids = $container || $('.js-grid', $namespace);

        $grids.each(function() {

            var $grid = $(this),
                $tile = $('.js-tile', $grid),

                isCarousel = $grid.hasClass('b-carousel');

            $tile.each(function() {

                var $tile = $(this),
                    $parent = isCarousel ? $grid : $tile.parent();

                $tile.data('height', $tile.outerHeight(true));
                $parent.css({ height: ['xs', 'sm', 'md', 'lg'].indexOf(helpers.screen()) >= 0 ? $tile.outerHeight(true) : '' });

                $(window).bind('resize.categoryHeight', function() {

                    $tile.data('height', $tile.outerHeight(true));
                    $parent.css({ height: ['xs', 'sm', 'md', 'lg'].indexOf(helpers.screen()) >= 0 ? $tile.outerHeight(true) : '' });

                });

            });

            /*$tile
                .on('mouseenter', function() {

                    $(this).find('.js-tile-pocket').css({ maxHeight: $(this).find('.js-tile-pocket-inner').outerHeight() });

                })
                .on('mouseleave', function() {

                    $(this).find('.js-tile-pocket').css({ maxHeight: '' });

                });*/

        });

        siteModules.dropDown({
            selector: 'js-benefit',
            onToggle: function(state) {

                if (state) {

                    this.find('.js-tile-pocket').css({ maxHeight: this.find('.js-tile-pocket-inner').outerHeight() });

                } else {

                    this.find('.js-tile-pocket').css({ maxHeight: '' });

                }

            }
        });

    }

    function floatedAnchors() {

        var $page = $('.b-page'),

            $panelWrap = $('.b-main_anchors_wrap'),
            $panel = $('.b-main_anchors_wrap_panel', $panelWrap);

        _tracker();
        $(window).bind('load.anchors resize.anchors scroll.anchors', _tracker);

        function _tracker() {

            if (['lg'].indexOf(helpers.screen()) >= 0 && !!$panelWrap.length) {

                var scrollState = !$('.b-leaflet').length ? document.documentElement.scrollTop || document.body.scrollTop : -parseInt($page.css('margin-top'), 10),
                    threshold = $panelWrap.offset().top;

                $panel
                    .toggleClass('b-main_anchors_wrap_panel__floated', scrollState >= threshold)
                    .css({ width: scrollState >= threshold ? $panelWrap.width() : '' });

                $panelWrap
                    .css({ height: scrollState >= threshold ? $panel.outerHeight() : '' })

            } else {

                $panel
                    .toggleClass('b-main_anchors_wrap_panel__floated', false)
                    .css({ width: '' });

                $panelWrap
                    .css({ height: '' });

            }

        }

    }

    return {
        init: function() {

            // Header positioning
            header();

            // Header notice strip
            headerStrip();

            // Main menu
            mainMenu();

            // Service lists toggling
            servicesMenu();

            // Footer positioning
            footerBottom();

            // On load page
            onLoadPage();

            // Valid target attribute
            targetBlank();

            // Set start num for ol
            orderedLists();

            // Benefits grid
            benefits();

            // Floated anchors
            floatedAnchors();

        }
    };

})(window);