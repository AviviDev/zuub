var forms = (function(window, undefined) {

    'use strict';

    function init(namespace) {

        forms.styleControls(namespace + ' input[type="checkbox"], ' + namespace + ' input[type="radio"]', namespace + ' select:not([multiple]):not(.js-selectric)', namespace + ' input[type="file"]:not(.js-dropZone, .js-uploader, .js-photo-uploader-input)');
        forms.styleSelects(namespace + ' select.js-selectric');

        forms.maskedInput(namespace);
        forms.makePlaceholders(namespace + ' [placeholder]');

        forms.validate(namespace);
        forms.resetForm(namespace);
        forms.refreshCaptcha(namespace);
        forms.textArea(namespace + ' .js-textField');

        forms.spinner(namespace + ' .js-spinner');
        forms.rating('.js-rating', namespace);

        // Init drop zones
        dropZone.init({ dzParent: '.b-page' });

    }

    function validate(namespace) {

        $(namespace + ' form').each(function() {

            var $form = $(this);

            if ($.isFunction($.fn.validate) && $form.data('checkup')) {

                $form
                    .validate({
                        onChange: !!$form.data('checkupOnChange') ? $form.data('checkupOnChange') : false,
                        onKeyup: !!$form.data('checkupOnKeyup') ? $form.data('checkupOnKeyup') : false,
                        onBlur: !!$form.data('checkupOnBlur') ? $form.data('checkupOnBlur') : false,
                        conditional: {

                            passwords: function() {

                                return $(this).val() == $('[data-conditional-check="passwords"]').val();

                            },

                            credit: function() {

                                var checkUp = dataValidation.creditCard($(this).val()),
                                    type = (checkUp.luhn) ? checkUp.type : '';

                                return checkUp.length && checkUp.luhn;

                            },

                            checkboxes: function() {

                                var flag = true;

                                $(this).closest('.b-form_box_field').find('input[type="checkbox"]').each(function() {

                                    flag = $(this).is(':checked');

                                    return !flag;

                                });

                                return flag;

                            },

                            dropZone: function() {

                                return !!$(this).closest('.dropzone').find('.dz-preview.dz-complete.dz-success').length;

                            }

                        },
                        eachValidField: function() {

                            formNotifications.hideErrorLabel.call($(this));

                        },
                        eachInvalidField: function(status, options) {

                            var conditional = !!$(this).data('conditionalType') ? formNotifications.labels.conditional[$(this).data('conditionalType')] : formNotifications.labels.conditional.def,
                                pattern = !!$(this).data('patternType') ? formNotifications.labels.pattern[$(this).data('patternType')] : formNotifications.labels.pattern.def,

                                notification = (options.required) ? ((!options.conditional) ? conditional : (!options.pattern) ? pattern : '') : formNotifications.labels.required;

                            formNotifications.showErrorLabel.call($(this), notification, 0);

                        },
                        valid: function(e) {

                            var $form = $(this),
                                $btn = $(this).find('button[type="submit"].e-btn'),

                                xhrSubmit = !!$(this).data('xhr'),
                                callTouchSubmit = !!$(this).data('ct'),

                                validHandler = $(this).data('handler'),
                                validHandlerMethod = $(this).data('handlerProperty');

                            if (typeof window[validHandler] === 'function') {

                                window[validHandler].call($form, e);

                            }
                            else if (typeof window[validHandler] === 'object') {

                                if (!!window[validHandler][validHandlerMethod]) {

                                    window[validHandler][validHandlerMethod].call($form, e);

                                }

                            }

                            if (xhrSubmit) {

                                e.preventDefault();

                                if ($.isFunction($.fn.ajaxSubmit)) {

                                    $form.ajaxSubmit({
                                        url: $form.attr('action'),
                                        method: $form.attr('method'),
                                        dataType: 'json',
                                        beforeSubmit: function() {

                                            $btn.toggleClass('request');
                                            //xhrFormHandler.eventLog.call($form);

                                        },
                                        success: function(response) {

                                            $btn.toggleClass('request');
                                            xhrFormHandler.response.call($form, response);

                                        }
                                    });

                                } else {

                                    $.ajax({
                                        url: $form.attr('action'),
                                        method: $form.attr('method'),
                                        data: $form.serialize(),
                                        dataType: 'json',
                                        before: function() {

                                            $btn.toggleClass('request');
                                            //xhrFormHandler.eventLog.call($form);

                                        },
                                        success: function(response) {

                                            $btn.toggleClass('request');
                                            xhrFormHandler.response.call($form, response);

                                        }
                                    });

                                }

                            }

                            if (callTouchSubmit) {

                                e.preventDefault();
                                xhrFormHandler.callTouch.call($form);

                            }

                        },

                        prepare: {
                            tinyMCE: function() {

                                if (!!tinyMCE) tinyMCE.triggerSave();
                                return this.val();

                            }
                        }

                    })
                    .find('input, textarea, select').bind('focus rating', function() {

                        $(this).closest('.m-valid').removeClass('m-valid');
                        $(this).closest('.m-error').removeClass('m-error');

                    });

            }

            // Check to toggle a button
            if ($form.data('checkupBtn')) {

                $form.validate({
                    nameSpace : 'buttonSwitching',
                    buttonSwitching: true,
                    switching: function(event, options, btnState) {

                        var xhrValidateState = typeof $form.data('xhrValidateState') !== 'undefined' ? $form.data('xhrValidateState') : true,
                            prop = btnState && xhrValidateState;

                        $(this).find('button[type="submit"]').prop('disabled', !prop);

                    }
                });

            }

        }); // End loop

    }

    function styleControls(input, select, file) {

        if ($.isFunction($.fn.uniform)) {

            // Inputs
            $(input)
                .not('.js-switcher')
                .uniform();

            // Inputs with decoration
            $('.js-switcher').each(function() {

                var css = { backgroundImage: $(this).data('backgroundImage') || 'none', backgroundColor: $(this).data('backgroundColor') || 'transparent' };

                $(this)
                    .uniform({
                        radioClass: 'e-switcher'
                    })
                    .closest('.e-switcher')
                    .css(css);

            });

            // Select
            if(!!select) {

                $(select).uniform({
                    selectAutoWidth: false,
                    selectClass: 'e-select'
                });

            }

            // File
            if(!!file) {

                $(file).uniform({
                    fileButtonHtml: '',
                    fileClass: 'e-uploader',
                    filenameClass: 'e-uploader_file',
                    fileButtonClass: 'e-uploader_btn i-icon i-files',
                    fileDefaultHtml: 'Загрузить файлы'
                });

            }

        }

    }

    function styleSelects(selector) {

        if ($.isFunction($.fn.selectric)) {

            $(selector).selectric({
                maxHeight: 184,
                arrowButtonMarkup: '',
                disableOnMobile: false,
                optionsItemBuilder: function(data, el) {

                    var storage = $(el).data();

                    return data.text + ((!!storage.price && !!storage.currency) ? '&nbsp;&ndash; <strong>' + storage.price + '&nbsp;<i class="' + storage.currency + '"></i></strong>' : '');

                },
                labelBuilder: function(data) {

                    return data.value != '' ? data.text : '<span class="placeholder">' + data.text + '</span>';

                }
            });

            strongWidth();
            $(window).bind('resize.selectricWidth', strongWidth);

        }

        function strongWidth() {

            $(selector).each(function() {

                var $wg = $(this).closest('.selectric-wrapper').css({ width: '' });

                if (!!$(this).data('width')) {

                    switch ($(this).data('width')) {

                        default:

                            $wg.css({ width: $(this).data('width') });
                            break;

                        case 'strong':

                            $wg.css({ width: $wg.outerWidth() });
                            break;

                    }

                }

            });

        }

    }

    function makePlaceholders(selector) {

        /*if ($.isFunction($.fn.placeholder)) {

            $(selector).each(function() {

                $(this).placeholder();

            });

        }*/

        $(selector).each(function() {

            makeLabelsPlaceholders.call($(this));

        });

    }

    function makeLabelsPlaceholders() {

        var placeholdersIsSupports = 'placeholder' in document.createElement('input');

        if (!placeholdersIsSupports && this.is('input:not([type="radio"]):not([type="checkbox"]), textarea')) {

            var $field = this.attr('id', !!this.attr('id') ? this.attr('id') : helpers.randomString(6)),
                $placeholder = $('<label class="b-form_box_field_placeholder" for="' + this.attr('id') + '">' + this.attr('placeholder') + '</label>');

            $field
                .after($placeholder)
                .on('focus blur change', function() {

                    $placeholder.toggleClass('complete', !!$(this).val().length);

                });

        }

    }

    function maskedInput(namespace) {

        if ($.isFunction($.fn.inputmask)) {

            $(namespace + ' [data-masking]').each(function() {

                $(this).inputmask({
                    mask: $(this).data('masking') || '+7 (999) 999-99-99',
                    showMaskOnHover: false
                });

                makeLabelsPlaceholders.call($(this));

            });

        }

    }

    function resetForm() {

        $('body').on('click', '.js-reset-form', function() {

            var $form = $(this).closest('form');

            $form[0].reset();

            $form.find('input[type="text"], select').val('');
            $form.find('input[type="radio"], input[type="checkbox"]').prop('checked', false);

            $.uniform.update();

        });

    }

    function refreshCaptcha(namespace) {

        $(namespace).on('click', '.js-captcha-refresh', function(e) {

            e.preventDefault();

            var url = $(this).attr('href') + '/?t=' + (new Date()).getTime();
            $(this).closest('.b-form_captcha').find('.b-form_captcha_img').attr('src', url);

        });

    }

    function textArea(selector) {

        $(selector).each(function() {

            var $textArea = $(this),
                $textAreaClone = $('<div id="' + $textArea.attr('name') + '" class="js-textField-clone" />').css({
                    position: 'absolute',
                    left: -10000,
                    top: -10000,
                    opacity: 0,
                    visibility: 'hidden',
                    width: $textArea.outerWidth(),
                    paddingLeft: $textArea.css('padding-left'),
                    paddingTop: $textArea.css('padding-top'),
                    paddingRight: $textArea.css('padding-right'),
                    paddingBottom: $textArea.css('padding-bottom'),
                    fontWeight: $textArea.css('font-weight'),
                    fontStyle: $textArea.css('font-style'),
                    fontSize: $textArea.css('font-size'),
                    fontFamily: $textArea.css('font-family'),
                    lineHeight: $textArea.css('line-height')
                });

            $textArea
                .css(helpers.pfx + 'transition', 'height 250ms')
                .css('transition', 'height 250ms');

            $textArea
                .on('keydown keyup change cut paste', function() {

                    var textAreaString = $textArea.val().replace(/&/g,'&amp;').replace(/ {2}/g, '&nbsp;').replace(/<|>/g, '&gt;').replace(/\n/g, '<br />');
                    var resultHeight = $textAreaClone.html($textArea.val().length ? textAreaString + '<br />&nbsp;' : '&nbsp;').outerHeight(); //<br />&nbsp;

                    $textArea.css({
                        height: resultHeight,
                        overflow: 'hidden'
                        //overflow: resultHeight <= parseInt($textArea.css('max-height'), 10) ? 'hidden' : 'scroll'
                    });

                    setTimeout(function() {

                        if (typeof YandexMaps !== 'undefined') {

                            YandexMaps.fitAllMaps();

                        }

                    }, 250);

                })
                .after($textAreaClone);

            $(window).bind('resize', function() {

                $textAreaClone.css({ width: $textArea.outerWidth() });

            });

        });

    }

    function spinner(selector, options) {

        var $spinner = $(selector);

        options = !!options ? options : {};

        options.checkMaxHandler = !!options.checkMaxHandler ? options.checkMaxHandler : false;
        options.checkMinHandler = !!options.checkMinHandler ? options.checkMinHandler : false;

        $spinner
            .each(function() {

                var $field = $(this),
                    value = $(this).val() || 1;

                value = checkMax.call($field, value);

                $field.val(value).wrap('<div class="b-spinner"></div>');
                $field.closest('.b-spinner').append('<div class="b-spinner_buttons"><button class="e-btn e-btn_3d e-btn_green" data-spin="up">+</button><button class="e-btn e-btn_3d e-btn_blue" data-spin="down">-</button></div>');

                $field.closest('.b-spinner').find('button').click(function(e) {

                    e.preventDefault();

                    var quantity = parseInt($(this).closest('.b-spinner').find('input').val());
                    var target = 0;

                    switch($(this).data('spin')) {

                        default:
                        case 'up':
                            target = quantity + 1;
                            break;

                        case 'down':
                            target = (quantity < 2) ? 1 : quantity - 1;
                            break;

                    }

                    $(this).closest('.b-spinner').find('input').val(target).trigger('blur').trigger('change');

                });

            })
            .keyup(function() {

                if(!$(this).val().match(/^\d+$/)) {

                    $(this).val(1);

                }

            })
            .blur(function(){

                if($(this).val() == '' || $(this).val() == 0) {

                    $(this).val(1);

                }

                // Check maximum
                $(this).val(checkMax.call($(this), $(this).val(), options.checkMaxHandler));

                if (!!options.onBlur) {

                    options.onBlur(parseInt($(this).val()));

                }

            });

        function checkMax(amount, checkMaxHandler) {

            var checkMax = typeof this.data('max') !== 'undefined' && Number(amount) > this.data('max');

            if (!!checkMaxHandler) {

                checkMaxHandler.call(this, checkMax);

            }

            return checkMax ? this.data('max') : Number(amount);

        }

        function checkMin(amount, checkMinHandler) {

            var checkMin = typeof this.data('min') !== 'undefined' && Number(amount) < this.data('min');

            if (!!checkMinHandler) {

                checkMinHandler.call(this, checkMin);

            }

            return checkMin ? this.data('min') : Number(amount);

        }

    }

    function rating(selector, namespace) {

        selector = !!selector ? selector : '.js-rating';
        namespace = !!namespace ? namespace + ' ' : '.b-page ';

        $(namespace + selector).each(function() {

            var $wg = $(this),
                $input = $(this).find(selector + '-field'),

                current = parseInt($input.val(), 10) || 0,
                target = 0,

                step = $wg.width() / 5,
                position = $wg.offset().left,

                $wgRail = $('<div class="b-rating_rail e-rating"></span>'),
                $wgSelect = $('<div class="b-rating_select e-rating"></span>');

            $wg.prepend($wgRail);
            $wg.append($wgSelect.width(current * step));

            $wg
                .bind((navigator.userAgent.match(/iPhone|iPad|iPod/i)) ? 'touchstart.starRating' : 'mousemove.starRating', function(e) {

                    position = $wg.offset().left;

                    if (!$wg.hasClass('lock')) {

                        step = $wg.width() / 5;
                        position = $wg.offset().left;

                        target = parseInt(((e.pageX || e.originalEvent.touches[0].pageX) - position) / step, 10) + 1;

                        $wgSelect.css({ width: target * step });

                    }

                })
                .bind('mouseout', function(e) {

                    $wgSelect.css({ width: current * step });

                });

            if (!$wg.hasClass('lock')) {

                $wg.bind((navigator.userAgent.match(/iPhone|iPad|iPod/i)) ? 'touchend.starRating' : 'click.starRating', setRating);

            }

            function setRating() {

                current = target;
                $input.val(target).trigger('rating');

                var data = $wg.data() || {};

                if (!!$wg.data('xhr') && $wg.data('url')) {

                    $.each($wg.find('input').serializeArray(), function() {

                        data[this.name] = this.value;

                    });

                    $.ajax({
                        url: data.url,
                        method: data.method || 'post',
                        success: function(response) {

                            $input.trigger('change');

                        }
                    });

                } else {

                    $input.trigger('change');

                }

                if (!!$wg.data('lockOnChange')) {

                    $wg
                        .toggleClass('lock', true)
                        .unbind('click.starRating');

                }

            }

        });

    }

    function reCaptcha(selector) {

        var $wg = $(selector);

        $wg.each(function() {

            var id = helpers.randomString(10);

            $(this).append($('<div class="g-recaptcha" id="' + id + '"></div>'));
            grecaptcha.render(id, { sitekey: $(this).data('siteKey') || helpers.reCaptchaKey || '' });

        });

    }

    return {
        init: init,
        makePlaceholders: makePlaceholders,
        maskedInput: maskedInput,
        rating: rating,
        resetForm: resetForm,
        refreshCaptcha: refreshCaptcha,
        reCaptcha: reCaptcha,
        styleControls: styleControls,
        styleSelects: styleSelects,
        spinner: spinner,
        textArea: textArea,
        validate: validate
    };

})(window);

var formNotifications = (function(window, undefined) {

    var settings = {
        errorClass: 'm-error',
        errorSuffix: '_error',
        validClass: 'm-valid'
    };

    var labels = {
        required: 'Это поле необходимо заполнить',
        conditional: {
            def: 'Введенные данные не совпадают',
            credit: 'Некорректный номер банковской карты',
            passwords: 'Введенные пароли не совпадают',
            checkboxes: 'Необходимо выбрать один из параметров',
            dropZone: 'Вы должны загрузить хотя бы один файл'
        },
        pattern: {
            def: 'Некорректный формат данных',
            email: 'Некорректный адрес электронной почты',
            phone: 'Некорректный номер телефона'
        },
        uploader: {
            count: 'Вы пытаетесь загрузить больше изображений, чем это допустимо',
            uploading: 'Во время загрузки изображений возникла ошибка'
        },
        submit: {
            success: 'Спасибо. Мы свяжемся с вами в ближайшее время.',
            error: 'Ошибка.'
        }
    };

    // Notification alerts
    function showMessage(msg, status, hideForm, callback) {

        var $notice = this.find('.b-form_message').length ? this.find('.b-form_message') : $('<div class="b-form_message"></div>').prependTo(this),
            suffix = status ? 'success' : 'error';

        // Set height
        $notice
            .height($notice.height())
            .html('<div class="b-form_message_inner"><div class="b-form_message_balloon b-form_message_balloon__' + suffix + '">' + msg + '</div></div>');

        $notice
            .toggleClass('b-form_message__show', true)
            .animate({ height: $notice.find('.b-form_message_inner').height(), paddingBottom: hideForm ? 0 : $notice.css('padding-bottom') }, 200, 'easeOutQuart', function() {

                $(this).css({ height: '' });

                if (typeof YandexMaps !== 'undefined') {

                    YandexMaps.fitAllMaps();

                }

            });

        if (hideForm) {

            this
                .find('form')
                .toggleClass('b-form__hide', true)
                .slideUp({ duration: 300, easing: 'easeOutQuart' });

        }

        $('html, body').animate({ scrollTop: ($notice.offset().top || this.offset().top) - 30 }, 1000, 'easeInOutExpo');

        // Callback
        if(!!callback) {

            callback.call(this);

        }

    }

    function showMessageInPopUp(msg, status, hideForm, callback) {

        var $form = this.find('form'),
            $msg =
                $('<div class="b-form_message b-form_message__show" style="padding: 0;">' +
                    '<div class="b-form_message_inner">' +
                        '<div class="b-form_message_balloon b-form_message_balloon__' + (status ? 'success' : 'error') + '">' + msg + '</div>' +
                    '</div>' +
                '</div>');

        this.leafLetPopUp('show', {
            animationStyleOfBox: 'scale',
            animationStyleOfChange: 'slide',
            boxWidth: 740,
            boxHorizontalGutters: 50,
            boxVerticalGutters: 50,
            closeBtnLocation: 'box',
            closeBtnClass: 'i-icon i-close',
            content: $msg,
            overlayOpacity: 0.5,
            scrollLocker: $('.b-page'),
            beforeLoad: function() {

                var $this = this;

                $this.find('.b-leaflet_box').addClass('b-form_message_balloon_leaflet');

            },
            afterLoad: function() {

                if (hideForm) {

                    $form
                        .toggleClass('disabled', true)
                        .find('input, select, textarea, button')
                        .prop('disabled', true);

                    $.uniform.update($form.find('input, select, textarea'));

                }

                // Callback
                if(!!callback) {

                    callback.call(this);

                }

            }
        });

    }

    function hideMessage() {

        var $notice = this.find('.b-form_message').length ? this.find('.b-form_message') : $('<div class="b-form_message"></div>').prependTo(this);

        $notice
            .slideUp({duration: 300, easing: 'easeOutQuart' });

    }

    // Notification labels
    function showErrorLabel(text, status) {

        var $field = this.closest('.b-form_box');

        $field
            .find('.b-form_box' + settings.errorSuffix).remove();

        $field
            .find('.b-form_box_field')
            .append('<div class="b-form_box' + settings.errorSuffix + '">' + text + '</div>');

        setTimeout(function() {

            $field
                .removeClass(settings.validClass)
                .addClass(settings.errorClass);

        }, 100);

    }

    function hideErrorLabel() {

        var $field = this.closest('.b-form_box');

        $field.removeClass(settings.errorClass);
        $field.find('.b-form_box' + settings.errorSuffix).remove();

        if ($field.find('[data-required]').length) {

            setTimeout(function() {

                $field.addClass(settings.validClass);

            }, 100);

        }

    }

    return {
        labels: labels,
        showErrorLabel: showErrorLabel,
        showMessage: showMessage,
        showMessageInPopUp: showMessageInPopUp,
        hideErrorLabel: hideErrorLabel,
        hideMessage: hideMessage
    };

})(window);

var xhrFormHandler = (function(window, undefined) {

    function response(response) {

        var $form = this,
            message = '';

        // start check
        if (typeof response.fields === 'boolean' && response.fields) {

            if (response.captcha || typeof response.captcha == 'undefined') {

                // Success action
                message = response.msg || formNotifications.labels.submit.success;

                if (!!$form.data('inPopUp')) {

                    formNotifications.showMessageInPopUp.call(this.closest('.b-form'), message, true, response.hideForm);

                } else {

                    formNotifications.showMessage.call(this.closest('.b-form'), message, true, response.hideForm);

                }

            } else {

                // Captcha error action
                message = response.msg || formNotifications.labels.submit.error;
                captchaHandler.call(this, response);

            }

            // Redirect user
            if (!!response.redirect) {

                redirect(response.redirect);

            }

        } else if (typeof response.fields === 'object') {

            // Get error message string
            var messageStr = ' Некорректно заполнены поля: ';

            $.each(response.fields, function(key, value) {

                var fieldName = $form.find('[name="' + key + '"]').attr('placeholder') || $form.find('[name="' + key + '"]').closest('.b-form_box').find('.b-form_box_title').text().replace(' *', '');

                messageStr += '&laquo;' + fieldName + '&raquo;, ';

            });

            message = response.msg || formNotifications.labels.submit.error + messageStr.substring(0, messageStr.length - 2) + '.';

            // Init handlers
            captchaHandler.call(this, response);

            if (!!$form.data('inPopUp')) {

                formNotifications.showMessageInPopUp.call(this.closest('.b-form'), message, false, false, function(form) {

                    highlightFields($form, response.fields);

                });

            } else {

                formNotifications.showMessage.call(this.closest('.b-form'), message, false, false, function(form) {

                    highlightFields($form, response.fields);

                });
            }


        } else {

            if ('console' in window) {
                console.log('Неверный формат ответа обработчика формы');
                console.log(response);
            }

        }

    }

    function highlightFields(form, array) {

        $.each(array, function(key, value) {

            formNotifications.showErrorLabel.call(form.find('[name="' + key + '"]'), value, 0);

        });

    }

    function captchaHandler(response) {

        this.find('[name*="captcha"]').val('');
        this.find('img').attr('src', response.captchaImg);

        if (!response.captcha && typeof response.captcha !== 'undefined') {

            formNotifications.showErrorLabel.call(this, forms.msg.captcha, 0);

        }

    }

    function redirect(settings) {

        var seconds = settings.timer / 1000,
            $counter = $(settings.counter).text(seconds),

            withCounting = $counter.length && !!settings.timer,
            timeout = setTimeout(withCounting ? countdown : redirect, withCounting ? 1000 : !!settings.timer ? settings.timer : 100);

        function countdown() {

            $counter[0].innerHTML--;

            if ($counter[0].innerHTML == 0) {

                redirect();
                clearTimeout(timeout);

            } else {

                setTimeout(countdown, 1000);

            }

        }

        function redirect() {

            window.location = settings.url;

        }

    }

    function eventLog() {

        var gaCat = this.data('gaCategory') || this.find('[name="ga_category"]').val() || false,
            gaAction = this.data('gaAction') || this.find('[name="ga_action"]').val() || false;

        if(!!this.data('analyticsSend')) {

            gaCat = this.data('analyticsSend').split('_')[0];
            gaAction = this.data('analyticsSend').split('_')[1];

        }

        //analytics.reachGoal(gaCat, gaAction);

    }

    function callTouch() {

        var $form = this,

            dialingNumber = $form.find('.js-callTouchPhone').val(),
            dialingIsAllowed = true,

            request,
            response = {
                fields: true,
                captcha: true,
                hideForm: true,
                msg: ''
            };

        dialingNumber = dialingNumber && dialingNumber.length ? dialingNumber.replace(/[\(\)\s\-]/g, '') : '';

        if (moment !== 'undefined') {

            var timeFrom = moment($form.data('ctStart'), 'HH:mm Z'),
                timeTo = moment($form.data('ctEnd'), 'HH:mm Z'),

                now = moment();

            dialingIsAllowed = timeFrom.isValid() && timeTo.isValid() && now.isBetween(timeFrom, timeTo);

        }

        dialingIsAllowed = dialingIsAllowed && (typeof ctCheckCallbackShouldBeProcessed !== 'undefined' ? ctCheckCallbackShouldBeProcessed() : false);

        if (dialingIsAllowed) {

            request = ctSendCallbackRequest(dialingNumber);

            // Обработка ответа от Call Touch и вывод сообщения
            if (!!$form.data('ctResponseHandler')) {

                setTimeout(function() {

                    var status = ctGetCallbackRequestStatus();

                    if (status != 'Заявка на обратный звонок успешно отправлена.') {

                        response.fields = {};
                        response.fields[$form.find('.js-callTouchPhone').attr('name')] = request || formNotifications.labels.pattern.phone;

                    }

                    response.msg = '<p>' + status + '</p>';

                    xhrFormHandler.response.call($form, response);

                }, 1500);

            }

            // Debug
            if ('console' in window && !!$form.data('ctDebug')) {

                setTimeout(function() {

                    console.log('Ответ сервиса Call Touch:');
                    console.log(ctGetCallbackRequestStatus());

                }, 2000);

            }

        }

    }

    return {
        callTouch: callTouch,
        eventLog: eventLog,
        response: response
    };

})(window);

var dataValidation = (function(window, undefined) {

    function creditCard(number) {

        var card_number = number.replace(/[^0-9]/g, '');

        var cards_types = {
            visa        : /^4\d{3}\d{4}\d{4}\d{4}$/,
            mastercard  : /^5[1-5]\d{2}\d{4}\d{4}\d{4}$/,
            discover    : /^6011\d{4}\d{4}\d{4}$/,
            amex        : /^3[4,7]\d{13}$/,
            diners      : /^3[0,6,8]\d{12}$/,
            bankcard    : /^5610\d{4}\d{4}\d{4}$/,
            jcb         : /^[3088|3096|3112|3158|3337|3528]\d{12}$/,
            enroute     : /^[2014|2149]\d{11}$/
        };

        return {
            number: card_number,
            length: card_number.length > 15,
            type: (typeTest(cards_types, card_number)) ? typeTest(cards_types, card_number) : '',
            luhn: luhnTest(card_number)
        };

    }

    function luhnTest(number) {

        var checksum = 0;

        for (var i = 0; i < number.length; i++) {

            var val = 0;

            if ((i % 2) != 0) {

                val = parseInt(number[i]);

            } else {

                var digit = parseInt(number[i]);
                val = (digit * 2 > 9)  ? digit * 2 - 9: digit * 2;

            }

            checksum += val;
        }

        return (checksum % 10 == 0);

    }

    function typeTest(thesaurus, number) {

        var match = false;

        for (prop in thesaurus) {

            if (thesaurus[prop].test(number)) {
                match = prop;
            }

        }

        return match;

    }

    return {
        creditCard: creditCard
    };

})(window);

var dropZone = (function(window, undefined) {

    function dropZone(options) {

        options = $.extend(options, {
            dzParent: '.b-form',
            dzSelector: '[data-dropzone]',
            dzClassPrefix: 'dz-'
        });

        Dropzone.autoDiscover = false;

        var previewTpl =
            '<div class="' + options.dzClassPrefix + 'preview ' + options.dzClassPrefix + 'file-preview">' +

            '<div class="' + options.dzClassPrefix + 'details relative">' +
            '<div class="' + options.dzClassPrefix + 'details-img"><div class="' + options.dzClassPrefix + 'details-img-inner"><img data-dz-thumbnail /></div></div>' +
            '<div class="' + options.dzClassPrefix + 'filename"><span data-dz-name></span></div>' +
            '<div class="' + options.dzClassPrefix + 'size" data-dz-size></div></div>' +

            '<div class="' + options.dzClassPrefix + 'progress"><span class="' + options.dzClassPrefix + 'upload" data-dz-uploadprogress></span></div>' +

            '<div class="' + options.dzClassPrefix + 'success-mark i-icon i-icon-after i-dz-check" data-dz-remove></div>' +
            '<div class="' + options.dzClassPrefix + 'error-mark i-icon i-dz-cross" data-dz-remove></div>' +

            '<div class="' + options.dzClassPrefix + 'error-message" data-dz-remove><span data-dz-errormessage></span></div>' +

            '</div>';

        $(options.dzParent + ' ' + options.dzSelector).each(function() {

            if ($(this).data('dropzone') && $.isFunction($.fn.dropzone)) {

                var $file = $(this).wrap('<div class="b-dropZone dropzone"></div>'),
                    $dropZone = $file.closest('.dropzone'),

                    url = $file.data('api') || $file.closest('form').attr('action'),
                    storedFiles = $file.data('storedFiles');

                $dropZone.dropzone({

                    url: url,
                    paramName: $(this).attr('name'),

                    dictDefaultMessage: 'Перетащите файлы для загрузки',
                    dictInvalidFileType: "Вы не можете загружать файлы этого типа.",
                    dictMaxFilesExceeded: "Вы не можете загрузить больше " + ($(this).data('maxCount') || 1) + " файлов",
                    dictFallbackMessage: 'Ваш браузер не поддерживает мультизагрузку файлов',
                    dictFallbackText: '',

                    previewTemplate: previewTpl,

                    uploadMultiple: $(this).attr('multiple') == 'multiple',
                    maxFiles: $(this).data('maxCount') || null,
                    maxFilesize: $(this).data('maxSize') || 5,

                    acceptedFiles: $(this).attr('accept') || null,

                    init: function() {

                        var dz = this;

                        if (!!storedFiles) {

                            for (var i = 0; i < storedFiles.length; i++) {

                                // Create the mock file:
                                var mockFile = storedFiles[i];

                                dz.options.addedfile.call(dz, mockFile);
                                dz.options.thumbnail.call(dz, mockFile, mockFile.url);

                                dz.options.complete.call(dz, mockFile);
                                dz.options.processing.call(dz, mockFile);
                                dz.options.success.call(dz, mockFile);

                                mockFile.accepted = true;

                                dz.files.push(mockFile);

                            }

                        }

                        // Append additional paramteres
                        dz.on('sending', function(file, xhr, data) {

                            var params = $file.data('params');

                            for (var prop in params) {

                                if (params.hasOwnProperty(prop)) {
                                    data.append(prop, params[prop]);
                                }

                            }

                        });

                    },

                    removedfile: function(file) {

                        removePhoto.call($(file.previewElement), url, file.name, $file.data('params'));

                    },

                    fallback: function() {

                        var $this = this,

                            $fallBackPreviewsContainer = $('<div class="dz-fallback-previews"></div>').prependTo($dropZone.addClass('dz-browser-not-supported')),
                            $fallbackTrigger = $file.wrap('<label class="dz-fallback-btn" for="' + $file.attr('id') + '">Выберите файл для загрузки</label>').appendTo($dropZone);

                        // Event listeners
                        $fallBackPreviewsContainer.on('click', '[data-dz-remove]', function() {

                            var $removingItem = $(this).closest('.dz-preview'),
                                filename = $removingItem.find('[data-dz-name]').text();

                            removePhoto.call($removingItem, url, filename);

                        });

                        // Show stored files
                        if (!!storedFiles) {

                            for (var i = 0; i < storedFiles.length; i++) {

                                storedFiles[i].size = $this.filesize(storedFiles[i].size);
                                $fallBackPreviewsContainer.append(_getFallbackPreview(storedFiles[i]));

                            }

                        }

                        /*
                         $fallbackTrigger.on('click', function() {
                         $file.trigger('focus');
                         });
                         */

                        // File transport
                        $file.on('change', function() {

                            $.ajax({
                                url: url,
                                files: $file,
                                iframe: true,
                                dataType: 'json',
                                success: function(response) {

                                    var mockFile = {
                                        name: response.uploadedFilesNames[0],
                                        size: $this.filesize(response.uploadedFilesSizes[0]),
                                        url: response.uploadedFiles[0]
                                    };

                                    if (response.uploaded) {

                                        $fallBackPreviewsContainer.append(_getFallbackPreview(mockFile));

                                    } else {

                                        $fallBackPreviewsContainer.append(_getFallbackPreview(mockFile), response.error);

                                    }

                                }
                            });

                        });

                    }
                });

            }

        });

        function removePhoto(url, name, data) {

            data = !!data ? data : {};

            var $preview = this,
                $file = this.closest('.dropzone').find('input[type="file"][data-dropzone]');

            $.ajax({
                type: $file.data('method') || 'post',
                url: url,
                data: $.extend({}, { remove: true, fileToRemove: name }, data),
                success: function() {

                    $preview.css({ opacity: 0 });

                    $preview.height($preview.height());
                    $preview.width($preview.width() - 1);

                    setTimeout(function() {

                        $preview.css({ height: 0, width: 0, margin: 0 });

                        setTimeout(function() {
                            $preview.remove();
                        }, 400);

                    }, 400);

                }
            });

        }

        function _getFallbackPreview(data, errorText) {

            var $preview = $(previewTpl).addClass('dz-complete dz-processing dz-image-preview dz-success');

            $preview.find('[data-dz-thumbnail]').attr('src', data.url);

            $preview.find('[data-dz-name]').html(data.name);
            $preview.find('[data-dz-size]').html(data.size);

            if (!!errorText) {

                $preview
                    .addClass('dz-error')
                    .removeClass('dz-success')
                    .find('.dz-error-message span').html(errorText);

            }

            return $preview;

        }

    }

    return {
        init: dropZone
    };

})(window);