<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<article class="b-announce b-announce__short clear">
    <?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])):?>
        <a class="b-announce_pic" href="<?=$arItem["~DETAIL_PAGE_URL"]?>">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"  title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>" />
        </a>
    <?endif;?>
    <div class="b-announce_text">

        <h3 class="b-announce_title"><a href="<?=$arItem["~DETAIL_PAGE_URL"]?>"><?=$arItem["~NAME"]?></a></h3>
       
    </div>

</article>
