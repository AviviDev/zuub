var siteResponsive = (function(window, undefined) {

    function init(namespace) {

        // For Retina displays
        siteModules.imagesOnRetinaDisplays();

        // Tables
        swipedTables({
            collection: '.b-table',
            namespace: namespace
        });

        // Set screen class
        var $body = $('body').addClass('m-' + helpers.screen()).addClass('m-' + (!!helpers.mobile() ? 'touch' : 'mouse'));

        $(window).bind('resize.sizeClass', function() {

            $body
                .removeClass('m-xs m-sm m-md m-lg m-xl')
                .addClass('m-' + helpers.screen());

        });

    }

    function swipedTables(options) {

        $(window).bind('load.responsive resize.responsive', function() {

            var $collection = $(options.collection);

            $collection.each(function() {

                if (!$(this).closest('.b-table_overflow').length) {

                    $(this).wrap('<div class="b-table_overflow"></div>');

                }

                var $container = $(this).closest('.b-table_overflow'),

                    tableWidth = $(this).width(),
                    containerWidth = $container.width();

                if (tableWidth > containerWidth) {

                    if (!!0/*$.isFunction($.fn.mCustomScrollbar)*/) {

                        $(this)
                            .closest('.b-table_overflow')
                            .addClass('swipe')
                            .mCustomScrollbar({
                                axis: 'x',
                                scrollInertia: 0,
                                scrollButtons:{
                                    enable: true
                                },
                                callbacks:{
                                    whileScrolling: swipeButtons,
                                    onScroll: swipeButtons,
                                    onInit: swipeButtons
                                }
                            });

                        if (!$(this).closest('.b-table_overflow').find('.b-table_overflow_shadow').length) {

                            $container.append('<div class="b-table_overflow_shadow b-table_overflow_shadow__left" /><div class="b-table_overflow_shadow b-table_overflow_shadow__right" />');

                        }

                    } else {

                        $(this)
                            .closest('.b-table_overflow')
                            .addClass('scrollable');

                    }

                } else {

                    if (!!0/*$.isFunction($.fn.mCustomScrollbar)*/) {

                        $(this)
                            .closest('.b-table_overflow')
                            .removeClass('swipe')
                            .mCustomScrollbar('destroy');

                        $container
                            .find('.b-table_overflow_shadow')
                            .remove();

                    } else {

                        $(this)
                            .closest('.b-table_overflow')
                            .removeClass('scrollable');

                    }

                }

                function swipeButtons() {

                    var $container = $(this).closest('.mCustomScrollbar');

                    $container.toggleClass('left', this.mcs.left < 0);
                    $container.toggleClass('right', $(this.mcs.content).outerWidth() != $container.width() - this.mcs.left);

                }

            });

        });

    }

    return {
        init: init
    };

})(window);