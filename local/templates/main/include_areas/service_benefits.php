<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule('iblock');
	$result = array();
	$elementsObj = CIBlockElement::GetList(
		array("SORT" => "ASC"),
		array("IBLOCK_ID" => 10, "ACTIVE" => "Y"),
		false,
		false,
		array("NAME", "PREVIEW_TEXT", "PROPERTY_ICON")
	);
	$result = array();
	while($element = $elementsObj->GetNext())
	{
		$result[] = $element;
	}
	//$result = array_chunk($result, ceil(count($result)/4));
if(count($result) > 0):
?>
<section class="b-benefits b-section__closed" id="service-benefits">

  <header class="b-heading">
	  <h2 class="b-heading_subtitle">Преимущества</h2>
  </header>

  <div class="b-benefits_list flex js-grid">
	<?foreach($result as $benefits):?>
	  <div class="col-xs-24 col-md-6">

		  <div class="b-benefits_item hasChild js-tile js-benefit">

			  <div class="b-benefits_item_short js-benefit-toggle">
				  <i class="b-benefits_item_short_icon i-icon <?=$benefits["PROPERTY_ICON_VALUE"]?>"></i>
				  <p><?=$benefits["NAME"]?></p>
			  </div>

			  <div class="b-benefits_item_details js-tile-pocket js-benefit-box">
				  <div class="js-tile-pocket-inner">
					  <p><?=$benefits["PREVIEW_TEXT"]?></p>
					  <p><a href="/forms/?form_id=1&pushivent=zapis_na_priem" class="e-btn e-btn_green e-btn_md e-btn_block js-popup">Записаться на прием</a></p>
				  </div>
			  </div>

		  </div>

	  </div>
	  <?endforeach;?>
  </div>
</section>
<?endif;?>