<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<article class="b-announce b-announce__short clear">

  <a class="b-announce_pic i-icon i-play js-videoBox" href="<?=$arItem["~PREVIEW_TEXT"]?>" title="<?=$arItem["~NAME"]?>" data-group="video">
	  <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["~NAME"]?>">
  </a>

  <div class="b-announce_text">

	  <h3 class="b-announce_title"><a class="js-videoBox" href="<?=$arItem["~PREVIEW_TEXT"]?>"><?=$arItem["~NAME"]?></a></h3>

  </div>

</article>
