<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<article class="b-dentist-short">

	  <div class="b-dentist-short_pic">
		  <a class="b-dentist-short_pic_capsule" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
			  <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["~NAME"]?>" />
		  </a>
	  </div>

	  <h3 class="b-dentist-short_header"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["~NAME"]?></a></h3>
	<?if(!empty($arItem["HLB_SPETSIALIZATSIYA_VRACHA"])):?>
        <p><?=$arItem["HLB_SPETSIALIZATSIYA_VRACHA"]?></p>
	<?endif;?>

  </article>