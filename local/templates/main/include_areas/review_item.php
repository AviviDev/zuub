<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<article class="b-comment">

    <div class="b-comment_header">
        <span class="b-comment_header_author"><?=$arItem["~NAME"]?></span>
        <?if(!empty($arItem["ACTIVE_FROM"])):?>
            <time class="b-recommendation_header_date"><?=$arItem["ACTIVE_FROM"]["RUS"]?></time>
        <?endif;?>
    </div>

    <p><?=$arItem["~PREVIEW_TEXT"]?></p>
    <?if(!empty($arItem["DETAIL_TEXT"])):?>
    <div class="b-comment_answer clear">

        <div class="b-comment_answer_header">

            <?if(!empty($arItem["ANSWER_DOCTOR"])):?>
                <div class="b-comment_answer_header_title hidden-md hidden-lg">Комментарий врача</div>
                <div class="b-comment_answer_header_pic">
                    <a class="b-comment_answer_header_pic_inner hidden-xs hidden-sm" href="#">
                        <img src="<?=$arItem["ANSWER_DOCTOR"]["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["ANSWER_DOCTOR"]["NAME"]?>" />
                    </a>
                </div>
                <div class="b-comment_answer_header_name"><?=$arItem["ANSWER_DOCTOR"]["PROPERTIES"]["LAST_NAME"]["VALUE"]?> <?=mb_substr($arItem["ANSWER_DOCTOR"]["PROPERTIES"]["FIRST_NAME"]["VALUE"],0,1,'UTF-8');?>.<?=mb_substr($arItem["ANSWER_DOCTOR"]["PROPERTIES"]["SECOND_NAME"]["VALUE"],0,1,'UTF-8');?>.</div>
            <?else:?>
                <div class="b-comment_answer_header_title hidden-md hidden-lg">Комментарий</div>
                <div class="b-comment_answer_header_pic">
                    <a class="b-comment_answer_header_pic_inner hidden-xs hidden-sm" href="#">
                        <img src="/upload/admins.jpg" alt="Администрация zuub.ru" />
                    </a>
                </div>
                <div class="b-comment_answer_header_name">Администрация</div>
            <?endif;?>

        </div>

        <div class="b-comment_answer_balloon">

            <p><?=$arItem["~DETAIL_TEXT"]?></p>

        </div>

    </div>
    <?endif;?>
    <!--div class="b-comment_footer">
        <p>Тема: Лечение кариеса, Удаление зубов</p>
    </div-->

</article>
