          <!-- Header -->
          <header class="b-header b-header__aside js-mainNav" data-lock="false" data-resolutions="xs,sm">

              <div class="b-notice js-headerStrip hidden-lg">
                  <div class="container">

                      <p>C 11 по 35 июня клиника Зууб.рф будет работать с 9 до 21 без выходных</p>

                  </div>
              </div>

              <div class="b-header_container container">

                  <div class="row">
                      <div class="col-xs-16 col-lg-24 col-right">
                          <div class="row">
                              <div class="col-xs-12 col-lg-24">

                                  <a class="b-header_logo" href="/" title="Зууб.рф: ваша здоровая улыбка &mdash; наша главная работа">
                                      <img src="<?=SITE_TEMPLATE_PATH?>/include_areas/img/zuub.svg" alt="Зууб.рф: ваша здоровая улыбка &mdash; наша главная работа" />
                                  </a>

                              </div>
                              <div class="col-xs-12 col-lg-24 hidden-lg g-right">

                                  <div class="b-header_phone hidden-xs hidden-sm">
                                      <a href="tel:+74952415641">+7 (495) 241-56-41</a>
                                  </div>

                                  <a class="b-header_call e-btn e-btn_green i-icon i-phone hidden-md hidden-lg" href="tel:+74952415641"></a>

                              </div>
                          </div>
                      </div>
                      <div class="col-xs-8 col-lg-24">

                          <button class="b-header_nav_toggle e-btn hidden-lg js-mainNav-toggle" type="button">
                              <i class="b-header_nav_toggle_icon"></i>
                              <span class="hidden-xs hidden-sm">Меню</span>
                          </button>

                          <div class="b-header_nav js-mainNav-box">

                              <div class="b-header_nav_contacts">

                                  <p>c 9 до 22 и без выходных</p>
                                  <p class="b-header_phone hidden-xs hidden-sm hidden-md"><a href="tel:+74952415641">+7 (495) 241-56-41</a></p>

                                  <p><a href="/html/forms/appointment.html" class="e-btn e-btn_green e-btn_md e-btn_block js-popup" data-box-width="350">Записаться на прием</a></p>

                              </div>

                              <nav class="b-header_nav_menu">

                                  <!--
                                  Атрибут data-target="" содержит селектор (в данном случае id) вызываемого блока
                                  Атрибут data-target-position="true" указывает на то, что меню нужно позиционировать относительно 1ого уровня
                                  -->
                                  <ul class="b-header_nav_menu_list js-nav">

                                      <li class="marker triangle">
                                          <a href="#" class="js-menu-trigger" data-target="#<?=ServiceMenu::GetTarget(CUR_ELEMENT_ID)?>" data-target-position="true">Услуги</a>
                                      </li>

                                      <li class="marker triangle">
                                          <a href="#" class="js-menu-trigger" data-target="#header-menu-03" data-target-position="true">О клинике</a>
                                      </li>

                                      <li><a href="#">Цены</a></li>
                                      <li><a href="#">Врачи</a></li>

                                      <li class="marker triangle">
                                          <a href="#" class="js-menu-trigger" data-target="#header-menu-04" data-target-position="true">Пациентам</a>
                                      </li>

                                      <li><a href="#">Контакты</a></li>

                                  </ul>

                              </nav>

                              <div class="b-header_search">
                                  <form action="/" method="get">

                                      <div class="b-header_search_field">

                                          <input type="text" name="query" placeholder="Поиск&hellip;">
                                          <button type="submit" class="b-header_search_btn e-btn i-icon i-magnifier"></button>

                                      </div>

                                  </form>
                              </div>

                              <ul class="b-header_social">
                                  <li><a href="#" class="i-icon i-soc-vk"></a></li>
                                  <li><a href="#" class="i-icon i-soc-facebook"></a></li>
                                  <li><a href="#" class="i-icon i-soc-instagram"></a></li>
                                  <li><a href="#" class="i-icon i-soc-youtube"></a></li>
                                  <li><a href="#" class="i-icon i-soc-google"></a></li>
                              </ul>

                          </div>

                      </div>
                  </div>

                  <!--#include virtual="_header_services.html"-->

                  <div class="b-header_nav_menu_box js-menu" id="header-menu-01">

                      <ul class="b-header_nav_menu_box_list b-header_nav_menu_box_list__left">
                          <li>

                              <!--
                              Атрибут
                              data-target-position="true"
                              указывает на то, что при возврате назад на трейтий уровень с четвертого,
                              его нужно спозиционировать относительно родительского элемента 1 уровня
                              -->
                              <button class="b-header_nav_menu_box_list_back e-btn i-icon i-chevron-circled-left js-menu-trigger js-menu-change" data-target="#header-menu-02" data-target-position="true" type="button"></button>

                              <a href="#">Раздел 2-го уровня</a>
                              <ul>
                                  <li><a href="#">Раздел 3-его уровня</a></li>
                                  <li><a href="#">Раздел 3-его уровня</a></li>
                                  <li><a href="#">Раздел 3-его уровня</a></li>
                              </ul>

                          </li>
                          <li class="b-header_nav_menu_box_list_footer">
                              <a class="js-menu-trigger js-menu-change" data-target="#header-services-full" href="#"><span>Все услуги</span></a>
                          </li>
                      </ul>

                      <button class="b-header_nav_menu_box_close e-btn i-icon i-close js-menu-close" type="button"></button>

                  </div>

                  <div class="b-header_nav_menu_box js-menu" id="header-menu-02">

                      <ul class="b-header_nav_menu_box_list b-header_nav_menu_box_list__left">
                          <li>

                              <!--
                              Здесь позиционирование не требуется,
                              так как возврат идет на большое меню услуг
                              -->
                              <button class="b-header_nav_menu_box_list_back e-btn i-icon i-chevron-circled-left js-menu-trigger js-menu-change" data-target="#header-services-full" data-target-position="false" type="button"></button>

                              <a href="#">Раздел 1-го уровня</a>
                              <ul>
                                  <li><a href="#">Раздел 2-го уровня</a></li>
                                  <li><a href="#">Раздел 2-го уровня</a></li>
                                  <li><a href="#">Раздел 2-го уровня</a></li>
                              </ul>

                          </li>
                          <li class="b-header_nav_menu_box_list_footer">
                              <a class="js-menu-trigger js-menu-change" data-target="#header-services-full" href="#"><span>Все услуги</span></a>
                          </li>
                      </ul>

                      <button class="b-header_nav_menu_box_close e-btn i-icon i-close js-menu-close" type="button"></button>

                  </div>

                  <div class="b-header_nav_menu_box js-menu" id="header-menu-03">

                      <ul class="b-header_nav_menu_box_list">
                          <li><a href="#">Полезная информация</a></li>
                          <li><a href="#">Расписание приемов</a></li>
                          <li><a href="#">Гарантия качества</a></li>
                          <li><a href="#">Прочее</a></li>
                      </ul>

                      <button class="b-header_nav_menu_box_close e-btn i-icon i-close js-menu-close" type="button"></button>

                  </div>

                  <div class="b-header_nav_menu_box js-menu" id="header-menu-04">

                      <ul class="b-header_nav_menu_box_list">
                          <li><a href="#">Полезная информация</a></li>
                          <li><a href="#">Расписание приемов</a></li>
                          <li><a href="#">Гарантия качества</a></li>
                          <li><a href="#">Прочее</a></li>
                      </ul>

                      <button class="b-header_nav_menu_box_close e-btn i-icon i-close js-menu-close" type="button"></button>

                  </div>

              </div>

          </header>
          <!-- Header :: End -->