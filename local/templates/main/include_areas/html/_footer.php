          <!-- Footer -->
          <footer class="b-footer">
              <div class="b-footer_container container">

                  <div class="b-footer_links">

                      <p><a href="/contacts.html" class="g-nowrap">Контакты</a></p>
                      <p><a href="/licenzii.html" class="g-nowrap">Лицензии и сертификаты</a></p>

                  </div>

                  <div class="b-footer_info">
                      <div class="row">
                          <div class="row-table">

                              <div class="col-xs-24 col-md-24 col-lg-24 col-vertical-bottom">

                                  <p>&copy; 1998&ndash;<?=date('Y')?> Стоматологический центр 3ууб: Стоматология Царицыно, Бирюлево, Бауманская, Басманный, Белорусская, ЦАО, ЮАО</p>

                              </div>
                              <?/*
                              <div class="col-xs-5 col-md-6 col-lg-2 col-vertical-bottom">

                                  <a class="b-footer_developer" href="http://prexpert.com" data-target="_blank">
                                      <svg width="30px" height="30px" viewBox="0 0 30 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                              <path id="logo-frame" d="M26.5384615,30 L3.46153846,30 C1.54978356,30 0,28.4502164 0,26.5384615 L0,3.46153846 C0,1.54978356 1.54978356,0 3.46153846,0 L19.6153846,0 L18.4615385,2.30769231 L3.46153846,2.30769231 C2.82428683,2.30769231 2.30769231,2.82428683 2.30769231,3.46153846 L2.30769231,26.5384615 C2.30769231,27.1757131 2.82428683,27.6923077 3.46153846,27.6923077 L26.5384615,27.6923077 C27.1757131,27.6923077 27.6923077,27.1757131 27.6923077,26.5384615 L27.6923077,3.46153846 C27.6923077,2.82428683 27.1757131,2.30769231 26.5384615,2.30769231 L25.3846154,2.30769231 L26.5384615,0 C28.4502164,-5.24708478e-13 30,1.54978356 30,3.46153846 L30,26.5384615 C30,27.4565187 29.6353033,28.3369745 28.9861388,28.9861388 C28.3369745,29.6353033 27.4565187,30 26.5384615,30 L26.5384615,30 L26.5384615,30 L26.5384615,30 Z" fill="#060606"></path>
                                              <path id="logo-coma" d="M6,21.8461538L14.0769231 8 25.6153846 8 12.9230769 21.8461538 6 21.8461538 6 21.8461538z" fill="#ff0000"></path>
                                          </g>
                                      </svg>
                                  </a>


                              </div>
*/?>
                          </div>
                      </div>
                  </div>

              </div>
          </footer>
          <!-- Footer :: end -->