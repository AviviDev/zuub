          <!-- Header -->
          <header class="b-header b-header__aside js-mainNav" data-lock="false" data-resolutions="xs,sm">

              <?$APPLICATION->IncludeComponent(
                  "prexpert:active_elements",
                  "mobile_notice",
                  array(
                      "IBLOCK_ID" => 9,
                  ),
                  false
              );
              ?>

              <div class="b-header_container container">

                  <div class="row">
                      <div class="col-xs-16 col-lg-24 col-right">
                          <div class="row">
                              <div class="col-xs-12 col-lg-24">

                                  <a class="b-header_logo" href="/" title="Зууб.рф: ваша здоровая улыбка &mdash; наша главная работа">
                                      <img src="<?=SITE_TEMPLATE_PATH?>/include_areas/img/zuub.svg" alt="Зууб.рф: ваша здоровая улыбка &mdash; наша главная работа" />
                                  </a>

                              </div>
                              <div class="col-xs-12 col-lg-24 hidden-lg g-right">

                                  <div class="b-header_phone hidden-xs hidden-sm">
                                      <a href="tel:+74951351064">+7 (495) 135-10-64</a>
                                  </div>

                                  <a class="b-header_call e-btn e-btn_green i-icon i-phone hidden-md hidden-lg" href="tel:+74951351064"></a>

                              </div>
                          </div>
                      </div>
                      <div class="col-xs-8 col-lg-24">

                          <button class="b-header_nav_toggle e-btn hidden-lg js-mainNav-toggle" type="button">
                              <i class="b-header_nav_toggle_icon"></i>
                              <span class="hidden-xs hidden-sm">Меню</span>
                          </button>

                          <div class="b-header_nav js-mainNav-box">

                              <div class="b-header_nav_contacts">

                                  <p>c 9 до 22 и без выходных</p>
                                  <p class="b-header_phone hidden-xs hidden-sm hidden-md"><a href="tel:+74951351064">+7 (495) 135-10-64</a></p>

                                  <p><a href="/forms/?form_id=1&pushivent=zakaz_zvonka_shapka" class="e-btn e-btn_green e-btn_md e-btn_block js-popup" data-box-width="350">Записаться на прием</a></p>

                              </div>

                              <nav class="b-header_nav_menu">

                                  <ul class="b-header_nav_menu_list js-nav">

                                      <li class="marker triangle">
											<?if(CUR_IBLOCK_ID == 1):
											$arrayMenuService = getMenuService("service_menu_", CUR_IBLOCK_ID, CUR_ELEMENT_ID);
											?>
											<a href="#" class="js-menu-trigger" data-target="#<?=$arrayMenuService["ID"]?>" data-target-position="true">Услуги</a>
											<?else:?>
											<a href="#" class="js-menu-trigger" data-target="#service_menu_main" data-target-position="false">Услуги</a>
											<?endif;?>
                                      </li>

                                      <li><a href="/ceny.html">Цены</a></li>
                                      <li><a href="/vrachi.html">Врачи</a></li>

                                      <li class="marker triangle">
                                          <a href="#" class="js-menu-trigger" data-target="#header-menu-pacientam" data-target-position="true">Пациентам</a>
                                      </li>

                                      <li class="marker triangle">
                                          <a href="#" class="js-menu-trigger" data-target="#header-menu-about-us" data-target-position="true">О клинике</a>
                                      </li>

                                      <li><a href="/contacts.html">Контакты</a></li>

                                  </ul>

                              </nav>

                              <div class="b-header_search">
                                  <form action="/search/" method="get">

                                      <div class="b-header_search_field">

                                          <input type="text" name="q" placeholder="Поиск&hellip;">
                                          <button type="submit" class="b-header_search_btn e-btn i-icon i-magnifier"></button>

                                      </div>

                                  </form>
                              </div>

                              <ul class="b-header_social">
                                  <li><a target="_blank" href="https://vk.com/zuub_ru" target="_blank" class="i-icon i-soc-vk"></a></li>
                                  <li><a href="https://www.instagram.com/zuub.ru/" target="_blank" class="i-icon i-soc-instagram"></a></li>
                                  <?/*<li><a href="#" class="i-icon i-soc-facebook"></a></li>

                                  <li><a href="#" class="i-icon i-soc-youtube"></a></li>
                                  <li><a href="#" class="i-icon i-soc-google"></a></li>*/?>
                              </ul>

                          </div>

                      </div>
                  </div>

                  <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/html/_header_services.php"), Array(), Array("MODE"=>"php") );?>

                  <?foreach($arrayMenuService["SUB_MENUS"] as $key=>$menu):
                      if(empty($menu)) continue;
                      ?>
                      <div class="b-header_nav_menu_box js-menu" id="<?=$menu["ID"]?>">

                          <ul class="b-header_nav_menu_box_list b-header_nav_menu_box_list__left">
                              <li>
								<?if(!empty($menu["PARENT_TARGET"])):?>
                                  <button class="b-header_nav_menu_box_list_back e-btn i-icon i-chevron-circled-left js-menu-trigger js-menu-change" data-target="#<?=$menu["PARENT_TARGET"]?>" data-target-position="<?=$menu["TARGET_POSITION"]?>" type="button"></button>
								<?endif;?>
                                  <a href="<?=$menu["LINK"]?>"><?=$menu["NAME"]?></a>
                                  <ul>
                                      <?foreach($menu["ITEMS"] as $arItem):?>
                                      <li><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></li>
                                      <?endforeach;?>
                                  </ul>

                              </li>
                              <li class="b-header_nav_menu_box_list_footer">
                                  <a class="js-menu-trigger js-menu-change" data-target="#service_menu_main" href="#"><span>Все услуги</span></a>
                              </li>
                          </ul>

                          <button class="b-header_nav_menu_box_close e-btn i-icon i-close js-menu-close" type="button"></button>

                      </div>
                  <?endforeach;?>
                  <div class="b-header_nav_menu_box js-menu" id="header-menu-pacientam">
                      <?$APPLICATION->IncludeComponent(
                          "bitrix:menu",
                          "",
                          array(
                              "ROOT_MENU_TYPE" => "pacientam",
                              "MENU_CACHE_TYPE" => "N",
                              "MENU_CACHE_TIME" => "3600",
                              "MENU_CACHE_USE_GROUPS" => "Y",
                              "MENU_CACHE_GET_VARS" => array(
                              ),
                              "MAX_LEVEL" => "2",
                              "CHILD_MENU_TYPE" => "",
                              "USE_EXT" => "N",
                              "DELAY" => "N",
                              "ALLOW_MULTI_SELECT" => "N"
                          ),
                          false
                      );?>

                      <button class="b-header_nav_menu_box_close e-btn i-icon i-close js-menu-close" type="button"></button>

                  </div>
                    <div class="b-header_nav_menu_box js-menu" id="header-menu-about-us">
                        <?$APPLICATION->IncludeComponent(
                          "bitrix:menu",
                          "",
                          array(
                              "ROOT_MENU_TYPE" => "about_us",
                              "MENU_CACHE_TYPE" => "N",
                              "MENU_CACHE_TIME" => "3600",
                              "MENU_CACHE_USE_GROUPS" => "Y",
                              "MENU_CACHE_GET_VARS" => array(
                              ),
                              "MAX_LEVEL" => "2",
                              "CHILD_MENU_TYPE" => "",
                              "USE_EXT" => "N",
                              "DELAY" => "N",
                              "ALLOW_MULTI_SELECT" => "N"
                          ),
                          false
                        );?>

                        <button class="b-header_nav_menu_box_close e-btn i-icon i-close js-menu-close" type="button"></button>

                    </div>
                  <div class="b-header_nav_menu_overlay"></div>

              </div>

          </header>
          <!-- Header :: End -->