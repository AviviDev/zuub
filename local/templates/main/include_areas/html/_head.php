<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
        <title><?$APPLICATION->ShowTitle()?></title>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, maximum-scale=1" />
        <meta name="robots" content="noyaca">
    <?$APPLICATION->ShowHead();?>

        <!-- Icons ans social images -->
        <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/favicons/favicon.ico" />

        <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/favicons/apple-touch-icon.png" />
        <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/favicons/apple-touch-icon-76x76.png" sizes="76x76" />
        <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/favicons/apple-touch-icon-120x120.png" sizes="120x120" />
        <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/include_areas/img/icons/favicons/apple-touch-icon-152x152.png" sizes="152x152" />

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css' />

        <!-- Styles -->
        <link href="<?=SITE_TEMPLATE_PATH?>/include_areas/plain/build/site.min.css" type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/include_areas/plain/build/print.min.css" type="text/css" rel="stylesheet" media="print" />

        <!--[if lt IE 10]>

            <link href="<?=SITE_TEMPLATE_PATH?>/include_areas/plain/build/ie9.min.css" type="text/css" rel="stylesheet" />

        <![endif]-->
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/include_areas/plain/js/jquery.1.11.1.min.js"></script>