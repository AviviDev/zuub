<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<article class="b-dentist<?if($isGray):?> b-dentist__gray<?endif;?>">
    <div class="row">
        <div class="col-xs-24 col-md-6">

            <div class="b-dentist_pic">
                <a class="b-dentist_pic_capsule" href="<?=$arItem["~DETAIL_PAGE_URL"]?>">

                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"  title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"/>

                </a>
            </div>

        </div>
        <div class="col-xs-24 col-md-12">

            <header class="b-dentist_header">
                <h2 class="b-dentist_header_name"><a href="<?=$arItem["~DETAIL_PAGE_URL"]?>"><?=$arItem["DISPLAY_PROPERTIES"]["LAST_NAME"]["DISPLAY_VALUE"]?> <?=$arItem["DISPLAY_PROPERTIES"]["FIRST_NAME"]["DISPLAY_VALUE"]?> <br class="hidden-xs hidden-sm" /><?=$arItem["DISPLAY_PROPERTIES"]["SECOND_NAME"]["DISPLAY_VALUE"]?></a></h2>
            </header>

            <ul class="b-dentist_info">
                <?if(!empty($arItem["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"])):?>
                <li>Специализация:&nbsp;<span><?=$arItem["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"]?></span></li>
                <?endif;?>
                <?if(!empty($arItem["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"])):?>
                <li>Опыт:&nbsp;<span><?=$arItem["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]?> <?=declensionByNumber(intVal($arItem["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))?></span></li>
                <?endif;?>
            </ul>

            <p class="b-dentist_info_reviews hidden-md hidden-lg"><a href="<?=$arItem["DISPLAY_PROPERTIES"]["OTZYVI"]["LINK"]?>">Отзывов:</a>&nbsp;<?=$arItem["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"]?></p>
            <?foreach($arItem["DISPLAY_PROPERTIES"]["SHORT_NAME"]["DISPLAY_VALUE"] as $klinika) :?>
            <p class="b-dentist_clinic i-icon i-location"><?=$klinika?></p>
            <?endforeach;?>
            <p class="b-dentist_clinic_more"><a href="<?=$arItem["~DETAIL_PAGE_URL"]?>">Подробнее</a></p>

        </div>

        <div class="col-xs-24 col-md-6 g-center-xs g-center-sm g-right-md g-right-lg">

            <a href="/forms/?form_id=1&pushivent=zapis_na_priem_vrachi" class="b-dentist_appointment e-btn e-btn_md e-btn_green e-btn_block_md e-btn_block_lg js-popup" data-box-width="350">Записаться на прием</a>
            <?if($arItem["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"] > 0):?>
                <div class="b-dentist_reviews hidden-xs hidden-sm">
                    <a class="b-dentist_reviews_capsule" href="<?=$arItem["~DETAIL_PAGE_URL"]?>#doctors-reviews">
                        <i class="i-icon i-reviews"></i>
                        <span><?=$arItem["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"]?> <?=declensionByNumber($arItem["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"], $arValue=array("отзыв","отзыва","отзывов"))?></span>
                    </a>
                </div>
            <?endif?>
        </div>
    </div>
</article>
