<?$is_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("form");
$formId = strip_tags($_REQUEST["form_id"]);
$rsForm = CForm::GetByID($formId);
if($arForm = $rsForm->Fetch())
{
  $formName = $arForm["NAME"];
}
$arValues = array();
$result = Array(
    'fields' => true,
    'hideForm' => true,
    'msg' => '
        <h6>Спасибо за обращение,</h6>
        <p>администратор позвонит Вам в ближайшее время!</p>
    '
);
$arMapCoMagic = array(
    1 => "name",
    2 => "phone",
    3 => "name",
    4 => "phone",
    6 => "message",
    8 => "name",
    9 => "phone",
    10 => "message"
);
$arFieldsMap = array(
    1 => "fio",
    2 => "phoneNumber",
    3 => "fio",
    4 => "phoneNumber",
    6 => "orderComment",
    8 => "fio",
    9 => "phoneNumber",
    10 => "orderComment"
);

$arCoMagic = array();

$arCallTouchParams = array(
    "clientApiId"   => '"943007355ct296201ab81d47557d2dee2e091996d66"',
    "subject"       => '"'.$formName.'"',
    "sessionId"     => 'window.call_value_1'
);

$pushivent = strip_tags($_REQUEST["pushivent"]);
if(!empty($pushivent))
{
    $result['msg'] .= '
    <script>dataLayer.push({"event": "'.$pushivent.'"})</script>
    ';
}
$rsQuestions = CFormField::GetList(
    $formId,
    "N",
    $by="s_sort",
    $order="asc",
    array("ACTIVE" => "Y")
);
$arEventFields = array("TITLE" => $formName, "MSG" => "", "SERVICE_MSG" => 'Страница: '.urldecode(strip_tags($_REQUEST["referrer"])));
$errorQuestions = array();
//print_r($_REQUEST);
while ($arQuestion = $rsQuestions->Fetch())
{
    switch ($arQuestion['FIELD_TYPE']) {
        case 'text':
            $fieldName = "form_text_";
            $questionParam = "ID";
            if(empty($_REQUEST[$fieldName.$arQuestion['ID']]))
            {
                $fieldName = "form_textarea_";
            }
            if(empty($_REQUEST[$fieldName.$arQuestion['ID']]))
            {
                $fieldName = "form_dropdown_";
                $questionParam = "SID";
            }

            if(!empty($_REQUEST[$fieldName.$arQuestion['ID']]))
            {
                if(!empty($arFieldsMap[$arQuestion['ID']]))
                {
                    $arCallTouchParams[$arFieldsMap[$arQuestion['ID']]] = '"'.strip_tags($_REQUEST[$fieldName.$arQuestion['ID']]).'"';
                    $arCoMagic[$arMapCoMagic[$arQuestion['ID']]] = strip_tags($_REQUEST[$fieldName.$arQuestion['ID']]);
                }

                $arEventFields["MSG"] .= $arQuestion['TITLE'].": ".strip_tags($_REQUEST[$fieldName.$arQuestion['ID']])."\n";
                $arValues[$fieldName.$arQuestion[$questionParam]] = strip_tags($_REQUEST[$fieldName.$arQuestion['ID']]);
            }
            elseif($arQuestion['REQUIRED'] == "Y")
            {
                $result['hideForm'] = false;
                if(!is_array($result['fields']))
                    $result['fields'] = array();
                $result['fields'][$fieldName.$arQuestion['ID']] = 'Необходимо заполнить';
                $errorQuestions[] = $arQuestion['TITLE'];
            }
            break;
    }
    if(!empty($_FILES["form_file_".$arQuestion['ID']]))
    {
        $fid = CFile::SaveFile(
            array_merge($_FILES["form_file_".$arQuestion['ID']], Array("del" => "Y", "MODULE_ID" => "forms")),
            "forms");
        ;

        $file = CFile::GetFileArray($fid);
        $arEventFields["MSG"] .= "Файл: http://zuub.ru".$file["SRC"]."\n";
        if(!CFile::IsImage($file["FILE_NAME"]))
            $arValues["form_file_".$arQuestion['ID']] = CFile::MakeFileArray($fid);
        else
            $arValues["form_image_".$arQuestion['ID']] = CFile::MakeFileArray($fid);
    }
}
if($_REQUEST["sessid"] !== bitrix_sessid())
{
    $errorQuestions[] = "Сессия пользователя";
    $result['fields']["sessid"] = 'Необходимо заполнить';
}

//if($result['fields'] !== true)
if( $result['fields'] !== true or ((int)$_REQUEST["form_id"] == 1 and preg_match("/^(\+7)\s\(\d{3}\)\s\d{3}-\d{2}-\d{2}$/i", $_REQUEST["form_text_2"]) != 1) or  ((int)$_REQUEST["form_id"] == 2 and preg_match("/[0-9a-z_.-]+@[0-9a-z_]+\.[a-z]{2,5}/i", $_REQUEST["form_text_4"]) != 1) )
{
    $result['msg'] = '
       <h6>Ошибка!</h6>
       <p>Некорректно заполнены поля &laquo;'. implode("&raquo; , &laquo;",$errorQuestions).'&raquo;</p>
    ';
}
else
{
    if ($resultId = CFormResult::Add($formId, $arValues,"N"))
    {
        $event = new CEvent();
        $event->SendImmediate("NEW_MESSAGE_ON_SITE","s1",$arEventFields);
        $result['msg'] .= '
            '.getCallTouchCode($arCallTouchParams).'
            '.getCoMagicCode($arCoMagic).'
        ';
    }
    else
    {
        global $strError;
        $result['fields'] = false;
        $result['hideForm'] = false;
        $result['msg'] = '
           <h6>Ошибка!</h6>
           <p>'.$strError.'</p>
        ';
    }
}

if($is_ajax):
    echo json_encode($result);
    die;
else:
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Результат регистрации заявки");
    $rsForm = Cform::GetByID($formId);
    if($arForm = $rsForm->Fetch())
    {
        $APPLICATION->AddChainItem($arForm["NAME"], "/forms/?form_id=".$formId);
    }

    $APPLICATION->AddChainItem("Результат регистрации заявки", "");
    ?>
    <section class="b-page_content">
    <article class="container">
    <?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "START_FROM" => "0",
        "PATH" => "",
        "SITE_ID" => "s1"
    ),
    false
);?>
    <main class="b-main b-main__box g-expand-background g-expand-border">
        <article class="b-main_wrapper">
            <article class="b-wysiwyg">
            <?=$result['msg']?>
            </article>
        </article>
    </main>
    </article>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
endif;?>

