<?$is_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$arValues = array();
$result = Array(
    'fields' => true,
    'hideForm' => true,
    'msg' => '
        <!--<h6>Спасибо,</h6>
        <p>Ваша вопрос добавлен на сайт!</p>-->
        <p>Спасибо за Ваш вопрос!</p> 
        <p>В ближайшее время врач оставит комментарии к нему на сайте.</p>
    '
);
$pushivent = strip_tags($_REQUEST["pushivent"]);
if(!empty($pushivent))
{
    $result['msg'] .= '
    <script>dataLayer.push({"event": "'.$pushivent.'"})</script>
    ';
}
$name = strip_tags($_REQUEST["name"]);
$msg = strip_tags($_REQUEST["msg"]);
$errorFields = array();
$arValues["IBLOCK_ID"] = BX_INFOBLOCK_VOPROS_OTVET;
$arValues["ACTIVE"] = "N";
$arValues["DATE_ACTIVE_FROM"] = ConvertTimeStamp(time(), "FULL");
$arEventFields = array("TITLE" => "Вопрос с сайта", "MSG" => "", "SERVICE_MSG" => "Страница: ".urldecode(strip_tags($_REQUEST["referrer"])));
if(!empty($name))
{
    $arValues["NAME"] = $name;
    $arEventFields["MSG"] .= $arValues["NAME"]."\n";
}
else
{
    $result['hideForm'] = false;
    if(!is_array($result['fields']))
        $result['fields'] = array();
    $result['fields']['name'] = 'Необходимо заполнить';
    $errorFields[] = "Имя";
}
if(!empty($msg))
{
    $arValues["PREVIEW_TEXT"] = $msg;
    $arEventFields["MSG"] .= "Вопрос: \n".$msg."\n";
}
else
{
    $result['hideForm'] = false;
    if(!is_array($result['fields']))
        $result['fields'] = array();
    $result['fields']['msg'] = 'Необходимо заполнить';
    $errorFields[] = "Ваша рекомендация";
}

if($_REQUEST["sessid"] !== bitrix_sessid())
{
    $errorQuestions[] = "Сессия пользователя";
    $result['fields']["sessid"] = 'Необходимо заполнить';
}
if($result['fields'] !== true)
{
    $result['msg'] = '
       <h6>Ошибка!</h6>
       <p>Некорректно заполнены поля &laquo;'. implode("&raquo; , &laquo;",$errorFields).'&raquo;</p>
    ';
}
else
{
    CModule::IncludeModule("iblock");
    $el = new CIBlockElement;
    if ($resultId = $el->Add($arValues))
    {
        $event = new CEvent();
        $event->SendImmediate("NEW_MESSAGE_ON_SITE","s1",$arEventFields);
    }
    else
    {
        global $strError;
        $result['fields'] = false;
        $result['hideForm'] = false;
        $result['msg'] = '
           <h6>Ошибка!</h6>
           <p>'.$el->LAST_ERROR.'</p>
        ';
    }
}
if($is_ajax):
    echo json_encode($result);
    die;
else:
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Результат отправки рекомендации");

    ?>    <section class="b-page_content">
    <article class="container">
    <?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "START_FROM" => "0",
        "PATH" => "",
        "SITE_ID" => "s1"
    ),
    false
);?>
    <main class="b-main b-main__box g-expand-background g-expand-border">
        <article class="b-main_wrapper">
            <article class="b-wysiwyg">
            <?=$result['msg']?>
            </article>
        </article>
    </main>
    </article>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
endif;?>