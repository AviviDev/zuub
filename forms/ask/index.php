<?$is_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!$is_ajax):
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Задать вопрос");
    ?>
    <section class="b-page_content">
    <article class="container">
    <?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "START_FROM" => "0",
        "PATH" => "",
        "SITE_ID" => "s1"
    ),
    false
);?>
    <section class="b-section b-section__closed">
			<header class="b-heading">
				<h1 class="b-heading_title"><?=$formName?></h1>
			</header>
			<article class="b-wysiwyg">
			
			<div class="row">
				<div class="col-xs-24 col-md-18 col-lg-15">
    <?
endif;
?>
   

        <div class="b-form">
            <form action="/forms/ask/send/" method="post" id="resume_form" data-checkup="true" data-checkup-on-change="true" data-xhr="true">
                <?=bitrix_sessid_post()?>
                <input type="hidden" name="referrer" value="<?=htmlspecialchars(urlencode($_SERVER['HTTP_REFERER']))?>" />
                <input type="hidden" name="pushivent" value="<?=strip_tags($_GET["pushivent"])?>" />
                <div class="b-form_box">

                    <div class="b-form_box_field">
                        <input type="text" name="name" id="resume_form_name" placeholder="Как вас зовут?" data-required />
                    </div>

                </div>

                <div class="b-form_box">

                    <div class="b-form_box_field">
                        <textarea name="msg" id="resume_form_msg" placeholder="Ваш вопрос" data-required ></textarea>
                    </div>

                </div>

                <!--div class="b-form_box">

                    <div class="g-recaptcha" id="resume_form_captcha"></div>

                    <script type="text/javascript">

                        grecaptcha.render('resume_form_captcha', { sitekey: '6Le9SyQTAAAAALH2Kau3w2lQpbFO4XTB_xJIA0Pr' });

                    </script>

                </div-->

                <div class="b-form_bottom">

                    <button type="submit" class="e-btn e-btn_md e-btn_orange e-btn_block_xs e-btn_block_sm">Задать вопрос</button>

                </div>

            </form>
        </div>

<?
if(!$is_ajax):?>
    </div>
			</div>
            </article>
		</section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
endif;?>