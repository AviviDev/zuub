<?$is_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$idPopup = intVal($_GET["id"]);
if(!$is_ajax || $idPopup < 1 || !CModule::IncludeModule("iblock"))
    die;



$elObj = CIBlockElement::GetByID($idPopup);
if($el = $elObj->GetNext())
{
    print($el["~PREVIEW_TEXT"]);
}
?>

