<?$is_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("form");
$formTemplate = "";
$formName = "";
$formId = intVal(strip_tags($_GET["form_id"]));
if($formId < 1)
    $formId = 1;
$rsForm = Cform::GetByID($formId);
if($arForm = $rsForm->Fetch())
{
    $formTemplate = $arForm["FORM_TEMPLATE"];
    $formName = $arForm["NAME"];
}
else
{
    $rsForms = Cform::GetList($by="s_sort", $order="asc");
    if($arForm = $rsForm->Fetch())
    {
        $formTemplate = $arForm["FORM_TEMPLATE"];
        $formName = $arForm["NAME"];
    }
}



if(!$is_ajax):
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle($formName);
    $APPLICATION->AddChainItem($formName, "");
?>
<section class="b-page_content">
    <article class="container">
    <?$APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
            "START_FROM" => "0",
            "PATH" => "",
            "SITE_ID" => "s1"
        ),
        false
    );?>
    <main class="b-main b-main__box g-expand-background g-expand-border">
        <article class="b-main_wrapper">
            <article class="b-wysiwyg">
<?
endif;
echo $formTemplate;
?>

<?
if(!$is_ajax):?>
            </article>
        </article>
    </main>
    </article>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
endif;?>