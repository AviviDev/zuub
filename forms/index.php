<?$is_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("form");
$formTemplate = "";
$formName = "";
$formId = intVal(strip_tags($_GET["form_id"]));
if($formId < 1)
    $formId = 1;
$rsForm = Cform::GetByID($formId);
if($arForm = $rsForm->Fetch())
{
    $formTemplate = $arForm["FORM_TEMPLATE"];
    $formName = $arForm["NAME"];
}
else
{
    $rsForms = Cform::GetList($by="s_sort", $order="asc");
    if($arForm = $rsForm->Fetch())
    {
        $formTemplate = $arForm["FORM_TEMPLATE"];
        $formName = $arForm["NAME"];
    }
}



if(!$is_ajax):
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle($formName);
    $APPLICATION->AddChainItem($formName, "");
?>
		<section class="b-section b-section__closed">
			<header class="b-heading">
				<h1 class="b-heading_title"><?=$formName?></h1>
			</header>
			<article class="b-wysiwyg">
			
			<div class="row">
				<div class="col-xs-24 col-md-18 col-lg-15">
    <?
endif;
    ?>
                
                <div class="b-leaflet_box_wrapper">
                    <div class="b-form">
                        <form action="/forms/send/" method="post" id="appointment_form" data-checkup="true" data-checkup-on-change="true" data-xhr="true">
                            <?=bitrix_sessid_post()?>
                            <input type="hidden" name="form_id" value="<?=$formId?>" />
                            <input type="hidden" name="referrer" value="<?=htmlspecialchars(urlencode($_SERVER['HTTP_REFERER']))?>" />
                            <input type="hidden" name="pushivent" value="<?=strip_tags($_GET["pushivent"])?>" />
<?
echo $formTemplate;
?>
<div class="b-form_box">

                    <div class="js-reCaptcha" data-sitekey="6Lfp4iYTAAAAALL4XsWR5u_VzSPwK_7YD9d00Fb6"></div>

                </div>
                            <?/*if($is_ajax):?>
                            <div class="b-form_box">
                                <div class="g-recaptcha" id="appointment_form_captcha"></div>
                                <script type="text/javascript">
                                    grecaptcha.render('appointment_form_captcha', { sitekey: '6Lfp4iYTAAAAALL4XsWR5u_VzSPwK_7YD9d00Fb6' });
                                </script>
                            </div>
                            <?else:?>
                                <div class="b-form_box">
                                    <div class="g-recaptcha" id="quality_control_form_captcha" data-sitekey="6Lfp4iYTAAAAALL4XsWR5u_VzSPwK_7YD9d00Fb6"></div>
                                </div>
                            <?endif;*/?>
                            <div class="b-form_bottom"> <button type="submit" class="e-btn e-btn_md e-btn_orange e-btn_block_xs e-btn_block_sm" disabled><?=$arForm["BUTTON"]?></button> </div>
                        </form>
                    </div>
                </div>
<?
if(!$is_ajax):?>
				</div>
			</div>
            </article>
		</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
endif;
?>

