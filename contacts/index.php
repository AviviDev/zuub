<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
$APPLICATION->AddChainItem("Контакты", "/contacts.html");
?>
<div class="js-tabs">
	<header class="b-heading">
		<div class="row">
			<div class="row-table">
				<div class="col-xs-12 col-sm-10 col-md-16 col-vertical-middle">

					<h1 class="b-heading_title"><?$APPLICATION->ShowTitle(false)?></h1>

				</div>
				<div class="col-xs-12 col-sm-14 col-md-8 col-vertical-middle g-right">

					<div class="b-contacts_tabs js-tabs-toggle">
						<a href="#contacts-list" class="i-icon i-view-list current"><span class="hidden-xs">Списком</span></a>
						<a href="#contacts-map" class="i-icon i-view-map"><span class="hidden-xs">На карте</span></a>
					</div>

				</div>
			</div>
		</div>
	</header>
<?
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"contacts",
	array(
		"PUSHIVENT" => "zapis_na_priem_contacts_list",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "8",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"PAGE_ELEMENT_COUNT" => "10",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => array(
			0 => "HLB_METRO",
			1 => "ADDRESS",
			2 => "SHORT_NAME",
			3 => "YANDEX_ADDRESS",
			4 => "WORK_TIME"
		),
		"OFFERS_LIMIT" => "5",
		"BACKGROUND_IMAGE" => "-",
		"TEMPLATE_THEME" => "blue",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_URL" => "",
		"DETAIL_URL" => "/#ELEMENT_CODE#.html",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страницы",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"FILE_404" => ""
	),
	false
);?>
<section class="b-section b-section__closed b-section__gray b-section__last">

    <header class="b-heading">
        <h2 class="b-heading_subtitle">Обратная связь</h2>
    </header>

    <div class="b-form b-form__white">
        <form action="/forms/contacts/" method="post" enctype="multipart/form-data" id="feedback-form-gray" data-checkup="true" data-checkup-on-change="true" data-xhr="true" data-in-pop-up="true">
			<?=bitrix_sessid_post()?>
				<input type="hidden" name="form_id" value="2" />
			<input type="hidden" name="referrer" value="<?=htmlspecialchars(urlencode($APPLICATION->GetCurPageParam()))?>" />
				<input type="hidden" name="pushivent" value="obratnaya_svyaz" />
		  <div class="row">
			  <div class="col-xs-24 col-md-8">

				  <div class="b-form_box">

					  <div class="b-form_box_field">
						  <input type="text" name="form_text_3" placeholder="Имя" data-required />
					  </div>

				  </div>

				  <div class="b-form_box">

					  <div class="b-form_box_field">
						  <input type="text" name="form_text_4" placeholder="Электронная почта" data-required data-pattern="[0-9a-z_.-]+@[0-9a-z_]+\.[a-z]{2,5}" data-pattern-type="email" />
					  </div>

				  </div>

				  <div class="b-form_box">

					  <label class="b-form_box_field">
						  <select name="form_dropdown_5" class="js-selectric">
							  <option value="">Тема сообщения</option>
							  <option value="5">Лечение</option>
							  <option value="6">Консультация</option>
                              <option value="Сотрудничество">Сотрудничество</option>
                              <option value="Сообщение руководству">Сообщение руководству</option>
						  </select>
					  </label>

				  </div>

			  </div>
			  <div class="col-xs-24 col-md-16">

				  <div class="b-form_box">

					  <div class="b-form_box_field">
						  <textarea name="form_textarea_6" placeholder="Текст сообщения" class="b-form_box_field_3x"></textarea>
					  </div>

				  </div>

			  </div>
		  </div>

		  <div class="b-form_bottom b-form_bottom__single">
			  <div class="row">
				  <div class="col-xs-24 col-md-8">

					  <div class="b-form_box">

						  <div class="b-form_box_field">
							  <input type="file" name="form_file_7" />
						  </div>

						  <div class="b-form_box_notice b-form_box_notice__file">Размер файла не более 5 Мб</div>

					  </div>

				  </div>
				  <div class="col-xs-24 col-md-16">

					  <div class="row">
						  <div class="col-xs-24 col-md-14">

							  <div class="js-reCaptcha" data-sitekey="6Lfp4iYTAAAAALL4XsWR5u_VzSPwK_7YD9d00Fb6"></div>

						  </div>
						  <div class="col-xs-24 col-md-10 g-center-xs g-center-sm g-right-md g-right-lg">

							  <button type="submit" class="e-btn e-btn_green e-btn_md" disabled="">Отправить</button>

						  </div>
					  </div>

				  </div>
			  </div>
		  </div>

		</form>
    </div>

</section>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>