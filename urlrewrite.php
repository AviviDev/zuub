<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/contacts.html(.*)#",
		"RULE" => "/contacts/index.php",
	),
	array(
		"CONDITION" => "#^/articles.html(.*)#",
		"RULE" => "/articles/index.php",
	),
	array(
		"CONDITION" => "#^/bolezni.html(.*)#",
		"RULE" => "/bolezni/index.php",
	),
	array(
		"CONDITION" => "#^/akcii.html(.*)#",
		"RULE" => "/akcii/index.php",
	),
	array(
		"CONDITION" => "#^/novosti.html(.*)#",
		"RULE" => "/novosti/index.php",
	),
	array(
		"CONDITION" => "#^/vrachi.html(.*)#",
		"RULE" => "/vrachi/index.php",
	),
	array(
		"CONDITION" => "#^/price.html(.*)#",
		"RULE" => "/price/index.php",
	),
	array(
		"CONDITION" => "#^/video.html(.*)#",
		"RULE" => "/video/index.php",
	),
	array(
		"CONDITION" => "#^/otzyvy-klientov.html(.*)#",
		"RULE" => "/otzyvy-klientov/index.php",
	),
	array(
		"CONDITION" => "#^/voprosy-polzovatelej.html(.*)#",
		"RULE" => "/voprosy-polzovatelej/index.php",
	),
	array(
		"CONDITION" => "#^/ceny.html(.*)#",
		"RULE" => "/ceny/index.php",
	),
	array(
		"CONDITION" => "#^(.*)#",
		"RULE" => "/index.php",
	),
);

?>