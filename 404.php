<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
/*
$sapi_type = php_sapi_name();
if ($sapi_type=="cgi")
	header("Status: 404");
else
	header("HTTP/1.1 404 Not Found");
*/
//@define("ERROR_404","Y");
if (strpos(php_sapi_name(),'cgi') !== false)
    header('Status: 404 Not Found');
else
    header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 - HTTP not found");
$APPLICATION->SetPageProperty("404Class", "b-nf");;
?>


    <!-- Search section -->
    <section class="b-nf-search b-section b-section__gray">
        <div class="container">

            <header class="b-nf-search_header">
                <h1 class="b-nf-search_header_title">Вы попали <br class="visible-xs" />на несуществующую страницу</h1>
                <p>Чтобы найти интересующую информацию воспользуйтесь поиском</p>
            </header>

            <div class="b-nf-search_form">
                <form action="/search/" method="get">

                    <div class="row compact">
                        <div class="row-table">
                            <div class="col-xs-8 col-md-10 col-vertical-middle">

                                <div class="b-nf-search_form_field i-icon i-magnifier">
                                    <input type="text" name="q" placeholder="Поиск по сайту" />
                                </div>

                            </div>
                            <div class="col-xs-4 col-md-2 col-vertical-middle">

                                <button type="submit" class="b-nf-search_form_btn e-btn e-btn_orange e-btn_sm e-btn_block">Искать</button>

                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </section>
    <!-- Search section end -->

    <!-- Nav section -->
    <section class="b-nf-nav b-section">
        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-lg-10 col-lg-offset-2">

                    <header class="b-heading">
                        <h2 class="b-heading_subtitle">Или воспользуйтесь нашим меню</h2>
                    </header>

                </div>
            </div>

            <div class="flex">
                <div class="col-xs-12 col-md-4 col-lg-3 col-lg-offset-2">

                    <ul class="b-nf-nav_list">
                        <li>
                            <a href="#">Услуги</a>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "404",
                                array(
                                    "ROOT_MENU_TYPE" => "uslugi",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(
                                    ),
                                    "MAX_LEVEL" => "2",
                                    "CHILD_MENU_TYPE" => "",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N"
                                ),
                                false
                            );?>
                        </li>
                    </ul>

                </div>
                <div class="col-xs-12 col-md-4 col-lg-3">

                    <ul class="b-nf-nav_list">
                        <li>
                            <a href="#">Пациентам</a>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "404",
                                array(
                                    "ROOT_MENU_TYPE" => "left",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(
                                    ),
                                    "MAX_LEVEL" => "2",
                                    "CHILD_MENU_TYPE" => "",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N"
                                ),
                                false
                            );?>
                        </li>
                    </ul>

                </div>
                <div class="col-xs-12 col-md-4 col-lg-3">

                    <ul class="b-nf-nav_list">
                        <li>
                            <a href="/o_klinike.html">О компании</a>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "404",
                                array(
                                    "ROOT_MENU_TYPE" => "oklinike",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(
                                    ),
                                    "MAX_LEVEL" => "2",
                                    "CHILD_MENU_TYPE" => "",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N"
                                ),
                                false
                            );?>
                        </li>
                    </ul>

                </div>
            </div>

        </div>
    </section>
    <!-- Nav section end -->

    <!-- Feedback section -->
    <section class="b-nf-feedback b-section">
        <div class="container">

            <div class="b-form_window b-form">
                <form class="b-form_window_box" id="comments-form" action="/forms/?form_id=8" method="post" data-checkup="true" data-checkup-on-change="true" data-xhr="true" data-ct="true" data-ct-start="09:00" data-ct-end="21:00" data-ct-debug="true">

                    <div class="b-form_window_box_title">Напишите нам</div>

                    <div class="flex">
                        <div class="col-xs-12 col-md-6">

                            <div class="b-form_box">

                                <div class="b-form_box_field b-form_box_field__icon">
                                    <input type="text" name="form_text_20" id="comments-form-name" placeholder="Как вас зовут? *" data-required />
                                </div>

                            </div>

                        </div>
                        <div class="col-xs-12 col-md-6">

                            <div class="b-form_box">

                                <div class="b-form_box_field b-form_box_field__icon">
                                    <input type="text" name="form_text_21" id="comments-form-email" placeholder="Ваш e-mail *" data-required data-pattern="[0-9a-z_.-]+@[0-9a-z_]+\.[a-z]{2,5}" data-pattern-type="email" />
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="b-form_box">

                        <div class="b-form_box_field b-form_box_field__icon">
                            <textarea name="form_textarea_22" id="comments-form-msg" placeholder="Ваш комментарий *" data-required></textarea>
                        </div>

                    </div>

                    <div class="b-form_bottom">
                        <div class="row">
                            <div class="row-table-md row-table-lg">
                                <div class="col-xs-12 col-md-7 col-md-vertical-middle col-lg-vertical-middle">

                                    <div class="g-recaptcha" data-sitekey="6Le9SyQTAAAAALH2Kau3w2lQpbFO4XTB_xJIA0Pr"></div>

                                </div>
                                <div class="col-xs-12 col-md-5 g-right-md g-right-lg col-md-vertical-middle col-lg-vertical-middle">

                                    <button type="submit" class="e-btn e-btn_md e-btn_orange">Отправить</button>

                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </section>
    <!-- Feedback section end -->

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>