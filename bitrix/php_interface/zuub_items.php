<?
class ZuubItems
{
    static function GetDoctors($arrFilter=array(), $nav=array(), $display=array())
    {
        CModule::IncludeModule('iblock');
        $result = array();
        //�������� ��� ������ ������
        $result["LINK"] = "#";
        $iblockObj = CIBlock::GetByID(BX_INFOBLOCK_VRACHI);
        if($iblock = $iblockObj->GetNext())
            $result["LINK"] = $iblock["~LIST_PAGE_URL"];
        //�������� ������ ������
        $time = ConvertTimeStamp(time(),"FULL");
        $arFilter = array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => BX_INFOBLOCK_VRACHI,
            array(
                "LOGIC" => "OR",
                array(
                    "LOGIC" => "AND",
                    array("DATE_ACTIVE_FROM"=>false),
                    array("DATE_ACTIVE_TO"=>false)
                ),
                array(
                    "LOGIC" => "AND",
                    array("<=DATE_ACTIVE_FROM"=>$time),
                    array(">=DATE_ACTIVE_TO"=>$time)
                ),
                array(
                    "LOGIC" => "AND",
                    array("DATE_ACTIVE_FROM"=>false),
                    array(">=DATE_ACTIVE_TO"=>$time)
                ),
                array(
                    "LOGIC" => "AND",
                    array("<=DATE_ACTIVE_FROM"=>$time),
                    array("DATE_ACTIVE_TO"=>false)
                )
            )
        );
        $arFilter = array_merge($arFilter,$arrFilter);
        $elementsObj = CIBlockElement::GetList(
            array("SORT"=>"ASC"),
            $arFilter,
            false,
            $nav
        );
        while($elementObj = $elementsObj->GetNextElement())
        {
            $element = $elementObj->GetFields();

            if(empty($element["PREVIEW_PICTURE"]))
                $element["PREVIEW_PICTURE"] = $element["DETAIL_PICTURE"];
            if(!empty($element["PREVIEW_PICTURE"]))
            {
                $img = CFile::ResizeImageGet(
                    $element["PREVIEW_PICTURE"],
                    array("width"=>308, "height"=>436),
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    false
                );
                $element["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
            }
            $element["PROPERTIES"] = $elementObj->GetProperties();
            foreach($display as $value)
            {
                if(!array_key_exists($value, $element["PROPERTIES"]) || empty($element["PROPERTIES"][$value]["VALUE"])) continue;
                $element["DISPLAY_PROPERTIES"][$value] = CIBlockFormatProperties::GetDisplayValue($element, $element["PROPERTIES"][$value], "catalog_out");
            }

            //���������� ������ ������������� �����. ������
            //$hlb_spetsializatsiya_vracha = $elementObj->GetProperty("HLB_SPETSIALIZATSIYA_VRACHA");
            if(!empty($element["PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["VALUE"]))
                $hlb_spetsializatsiya_vracha = CIBlockFormatProperties::GetDisplayValue($element, $element["PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"], "catalog_out");
            $specializatsiya = "";
            if(!empty($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"]) &&
                !is_array($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"]))
                $hlb_spetsializatsiya_vracha["DISPLAY_VALUE"] = array($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"]);
            foreach($hlb_spetsializatsiya_vracha["DISPLAY_VALUE"] as $keyProf=>$profession)
            {
                if($keyProf > 0) $specializatsiya .= ", ";
                $specializatsiya .= strtolower($profession);
            }
            $element["DISPLAY_PROPERTIES"]["HLB_SPETSIALIZATSIYA_VRACHA"]["DISPLAY_VALUE"] = ucfirst_utf8(strtolower($specializatsiya));

            //���������� ���� ������. ������
            $year = 0;
            if(!empty($element["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))
                $year = (time() - MakeTimeStamp($element["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"]))/(365*86400);
            $element["PROPERTIES"]["WORK_EXPERIENCE"]["VALUE"] = intVal($year);
            //�������� ���������� �������
            if(!empty($element["PROPERTIES"]["HLB_VRACHI"]["VALUE"]))
            {
                $otzyviObj = CIBlockElement::GetList(
                    array("DATE_ACTIVE_FROM" => "DESC"),
                    array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_OTZYV, "PROPERTY_HLB_VRACHI" => $element["PROPERTIES"]["HLB_VRACHI"]["VALUE"])
                );
                $otzyviObj->NavStart(1);
                $element["DISPLAY_PROPERTIES"]["OTZYVI"]["COUNT"] = intVal($otzyviObj->SelectedRowsCount());
            }
            $result["DISPLAY_VALUE"][$element["ID"]] = $element;
            //���������� ������ ������������� �����. �����
            /*print_r("<pre>");
            print_r($result["DISPLAY_VALUE"]);
            print_r("</pre>");*/

        }
        return $result;
    }
}
?>