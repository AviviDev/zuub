<?
include_once("zuub_items.php");
function getCoMagicCode($arParams)
{
    $str = '<script type="text/javascript">';
    $str .= "$(document).ready(function() {
    Comagic.addOfflineRequest({";
    $second = false;
    foreach($arParams as $key=>$value)
    {
        if($second)
            $str .= ", ";
        else
            $second = true;
        $str .= $key.": '".$value."'";
    }
    $str .= "});
    });
    </script>
    ";
    return $str;
}
function getCallTouchCode($arParams, $arWorkTime=false)
{
    return ""; //отключаем вывод
    $send = true;
    if(is_array($arWorkTime) && $arWorkTime !== false)
    {
        if (function_exists('date_default_timezone_set'))
            date_default_timezone_set('Europe/Moscow');
        $startTime = mktime($arWorkTime["START"],0,0);
        $endTime = mktime($arWorkTime["END"],0,0);
        $curTime = time();
        if($startTime < $curTime && $curTime < $endTime)
            $send = false;
    }
    //if(!$send) return '';
    $str = '<script type="text/javascript">';
    $str .= "$(document).ready(function() {
    ";
    $str .= "postDataVar = {};";
    foreach($arParams as $key=>$value)
    {
        $str .= '
             postDataVar.'.$key.'='.$value.';';
    }
    $str .= '
            $.getJSON(
                "http://api.calltouch.ru/calls-service/RestAPI/requests/orders/register/",
                postDataVar
            );';
    $str .= "
    });
    </script>
    ";
    return $str;
}
function declensionByNumber($number, $suffix=array("год","года","лет"))
{
    $keys = array(2, 0, 1, 1, 1, 2);
    $mod = $number % 100;
    $suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
    return $suffix[$suffix_key];
/*
    $m = $n % 10; $j = $n % 100;
    if($m==0 || $m>=5 || ($j>=10 && $j<=20)) return $arValue[2];
    if($m>=2 && $m<=4) return  $arValue[1];
    return $arValue[0];*/
}
function getMenuService($target, $iblockId = false, $elementId = false)
{
	if($elementId == false) return array("ID" => $target);
	CModule::IncludeModule('iblock');
	$elementObj = CIBlockElement::GetList(
		array("SORT" => "ASC"),
		array("IBLOCK_ID" => $iblockId, "PROPERTY_PARENT" => $elementId, "ACTIVE" => "Y"),
		false,
		array("nTopCount" => 1),
		array("ID")
	);
	if($element = $elementObj->GetNext())
	{
		$path = GetElementsPathById($elementId, "PARENT");
	}
	else
	{
		$element = CIBlockElement::GetList(
			array("SORT" => "ASC"),
			array("IBLOCK_ID" => $iblockId, "ID" => $elementId, "ACTIVE" => "Y"),
			false,
			array("nTopCount" => 1),
			array("PROPERTY_PARENT")
		)->GetNext();
		$path = GetElementsPathById($element["PROPERTY_PARENT_VALUE"], "PARENT");
	}
	$arrayMenu = array();
	foreach($path as $key=>$element)
	{
		$arMenu = array(
			"ID" => $target . $element["ID"],
			"NAME" => $element["NAME"],
            "LINK" => $element["DETAIL_PAGE_URL"],
			"ITEMS" => getCurLevel($path["IBLOCK_ID"], ($element["ID"])?$element["ID"]:false)
		);
        if(intVal($element["PROPERTIES"]["PARENT"]["VALUE"]) > 0)
        {
            $arMenu["PARENT_TARGET"] = $target . (string)intVal($element["PROPERTIES"]["PARENT"]["VALUE"]);
            $arMenu["TARGET_POSITION"] = true;
        }
        else//переадресуем на общее меню
        {
            $arMenu["PARENT_TARGET"] = "service_menu_main";
            $arMenu["TARGET_POSITION"] = false;
        }

		$arrayMenu[] = $arMenu;
	}
    /*
	$arMenu = array(
		"ID" => $target . "0",
		"NAME" => "Услуги",
		"ITEMS" => getCurLevel($iblockId, false)
	);
	$arrayMenu[] = $arMenu;
	$id = 0;
    */
    $arMenu = array();
	$arrayMenu[] = $arMenu;
	$id = "service_menu_main";

	if(count($path) > 0)
		$id = $target . $path[count($path)-1]["ID"];



	return array(
		"ID" => $id,
		"SUB_MENUS" => $arrayMenu
	);


}


class ServiceMenu
{
    const TEMPLATE = "service_menu_";
    const MAIN_URL_ID = "service_menu_main";
    static function GetTarget($id=false)
    {
        if($id !== false)
            return ServiceMenu::TEMPLATE.$id;
        return ServiceMenu::MAIN_URL_ID;
    }
}
function getCurLevel($iblockId, $id = false, $propertyParent = "PARENT")
{
	CModule::IncludeModule('iblock');

	$elementsObj = CIBlockElement::GetList(
		array("SORT" => "ASC"),
		array("IBLOCK_ID" => $iblockId, "PROPERTY_".$propertyParent => $id, "ACTIVE" => "Y")
	);
	$result = array();
	while($elementObj = $elementsObj->GetNextElement())
	{
		$element = $elementObj->GetFields();
		$element["PROPERTIES"] = $elementObj->GetProperties();
		$result[$element["ID"]] = $element;
	}
	return $result;
}

function getCurMenu($iblockId, $id = false, $propertyParent = "PARENT", $level = 1)
{
	$elements = getCurLevel($iblockId, $id, $propertyParent);
	$result = array();
	foreach($elements as $element)
	{

		$result[] = array(
			$element["NAME"],
			(empty($element["DETAIL_PAGE_URL"]))?$element["PROPERTIES"][propertyUrl]["VALUE"]:$element["DETAIL_PAGE_URL"],
			Array(), 
			Array(), 
			"" 
		);
	}
	
    return $result;
}



function GetElementsPathById($id, $propertyParent)
{

    $result = array();
    if(intVal($id) < 1) return $result;
    CModule::IncludeModule('iblock');
    if(empty($propertyParent))
    {
        $elementObj = CIBlockElement::GetByID($id);
        if($element = $elementObj->GetNext())
            $result[] = $element;
    }
    else
    {
        while(intVal($id) > 0)
        {
            $elementObj = CIBlockElement::GetByID($id);
            if($element = $elementObj->GetNextElement())
            {
                $result[] = $element->GetFields();
                $property = $element->GetProperty($propertyParent);
                $id = intVal($property["VALUE"]);
            }
            else
            {
                $id = 0;
            }

        }
    }
    return array_reverse($result);

}
function ucfirst_utf8($str)
{
    return mb_substr(mb_strtoupper($str, 'utf-8'), 0, 1, 'utf-8') . mb_substr($str, 1, mb_strlen($str)-1, 'utf-8');
}

use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
//подключаем модуль highloadblock
CModule::IncludeModule('highloadblock');
//Напишем функцию получения экземпляра класса:
function GetEntityDataClass($HlBlockId) {
    if (empty($HlBlockId) || $HlBlockId < 1)
    {
        return false;
    }
    $hlblock = HLBT::getById($HlBlockId)->fetch();
    $entity = HLBT::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    return $entity_data_class;
}

/*производим поиск элемента по свойству url и определяем переменные*/
AddEventHandler("main", "OnPageStart", array("PrExpert", "SetGlobalElementParams"));
AddEventHandler("main", "OnPageStart", array("PrExpert", "Redirect"));
class PrExpert
{
    public function SetGlobalElementParams()
    {
        global $APPLICATION;
        $strPage = ltrim($APPLICATION->GetCurPage(), "/");
        if(empty($strPage)) $strPage = "index.html";
        if (CModule::IncludeModule("iblock") && (!empty($strPage)))
        {
            $eb_list = CIBlockElement::GetList(array(), array("ACTIVE" => "Y", array("LOGIC" => "OR",array("PROPERTY_URL" => $strPage),array("CODE" => str_replace(".html","",$strPage)))),false,false,array("ID", "IBLOCK_ID", "PROPERTY_TEMPLATE"));
            if($element = $eb_list->GetNext())
            {
                define("CUR_IBLOCK_ID", $element["IBLOCK_ID"]);
                define("CUR_ELEMENT_ID", $element["ID"]);
                if(!empty($element["PROPERTY_TEMPLATE_VALUE"]))
                    define("CUR_TEMPLATE", $element["PROPERTY_TEMPLATE_VALUE"]);
                else
                    define("CUR_TEMPLATE", false);
            }
            else
            {
                define("CUR_IBLOCK_ID", false);
                define("CUR_ELEMENT_ID", false);
                define("CUR_TEMPLATE", false);
            }
        }
        else
        {
            define("CUR_IBLOCK_ID", false);
            define("CUR_ELEMENT_ID", false);
            define("CUR_TEMPLATE", false);
        }
    }
    public function ReplaceText()
    {
        if (!CModule::IncludeModule("iblock")) return false;
        $arReplace = array("/base_images/","/images/");
        $el = new CIBlockElement;
        $eb_list = CIBlockElement::GetList(array(), array(),false,false,array("ID", "PREVIEW_TEXT", "DETAIL_TEXT"));
        while($element = $eb_list->GetNext())
        {
            foreach($arReplace as $textReplace)
            {
                $prevText = str_replace("../","/",$element["PREVIEW_TEXT"]);
                $prevText = str_replace('src="images','src="/images',$prevText);
                $prevText = str_replace($textReplace, "/upload" . $textReplace, $prevText);
                $detText = str_replace("../","/",$element["DETAIL_TEXT"]);
                $detText = str_replace('src="images','src="/images',$detText);
                $detText = str_replace($textReplace, "/upload" . $textReplace, $detText);
            }
            //print_r(array("PREVIEW_TEXT" => $prevText, "DETAIL_TEXT" => $detText));

            //$el->Update($element["ID"], array("PREVIEW_TEXT" => $prevText, "DETAIL_TEXT" => $detText));
        }
    }
    public function Redirect()
    {
        global $APPLICATION;

        $strPage = ltrim($APPLICATION->GetCurPage(), "/");
        //throw new Exception();
        if (CModule::IncludeModule("iblock") && (!empty($strPage)))
        {
            $eb_list = CIBlockElement::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_REDIRECTS, "PROPERTY_OLD_URL" => $APPLICATION->GetCurPage()),false,false,array("NAME", "PROPERTY_NEW_URL", "PROPERTY_TYPE_REDIRECT"));
            if($element = $eb_list->GetNext())
            {
                LocalRedirect($element["PROPERTY_NEW_URL_VALUE"], false, $element["PROPERTY_TYPE_REDIRECT_VALUE"]);
            }
        }
    }

    public static function GetReferalForm()
    {
        $domain=parse_url($_SERVER["HTTP_REFERER"], PHP_URL_HOST);
        $_SERVER["HTTP_REFERER"];
        $script = "";
        if (CModule::IncludeModule("iblock") && (!empty($domain)))
        {
            $eb_list = CIBlockElement::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => BX_INFOBLOCK_REFERALS, "PROPERTY_REFERAL_DOMEN" => $domain, "CHECK_PERMISSIONS" => "N"),false,false,array("ID", "PROPERTY_BOXWIDTH"));

            if($element = $eb_list->GetNext())
            {
                $script = '
                <script type="text/javascript">

                        $(window).load(function() {

                            setTimeout(function() {

                                sitePlugins.onDemandPopUp({
                              url: "/forms/promo/?id='.$element["ID"].'",     // Источник
                              checkCookies: false,                      // Можно записывать показ в куки, чтобы не показывать несколько раз. Есть дополнительные настройки
                              boxWidth: '.$element["PROPERTY_BOXWIDTH_VALUE"].'
                          });

                      }, 600);

                        });

                </script>';
            }
        }
        return $script;
    }
}

?>