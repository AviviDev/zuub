<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//мапинг по папкам (проще потом переходить на нормальные url)

if(CUR_IBLOCK_ID == 1)
{
	$APPLICATION->IncludeFile("/uslugi/detail.php", Array(), Array("MODE"=>"php") );
	die;
}
if(CUR_IBLOCK_ID == 3)
{
	$APPLICATION->IncludeFile("/vrachi/detail.php", Array(), Array("MODE"=>"php") );
	die;
}
if(CUR_IBLOCK_ID == 5)
{
	$APPLICATION->IncludeFile("/articles/detail.php", Array(), Array("MODE"=>"php") );
	die;
}
if(CUR_IBLOCK_ID == 2)
{
	$APPLICATION->IncludeFile("/akcii/detail.php", Array(), Array("MODE"=>"php") );
	die;
}

if(CUR_IBLOCK_ID == 4)
{
	$APPLICATION->IncludeFile("/bolezni/detail.php", Array(), Array("MODE"=>"php") );
	die;
}
if(CUR_IBLOCK_ID == 8)
{
	$APPLICATION->IncludeFile("/contacts/detail.php", Array(), Array("MODE"=>"php") );
	die;
}
if(CUR_IBLOCK_ID == 6)
{
	$APPLICATION->IncludeFile("/novosti/detail.php", Array(), Array("MODE"=>"php") );
	die;
}
if(CUR_IBLOCK_ID == 12)
{
	$APPLICATION->IncludeFile("/video/detail.php", Array(), Array("MODE"=>"php") );
	die;
}
if(CUR_IBLOCK_ID == 15)
{
	$APPLICATION->IncludeFile("/otzyvy-klientov/detail.php", Array(), Array("MODE"=>"php") );
	die;
}

?>

 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.element", 
	(CUR_TEMPLATE)?CUR_TEMPLATE:"",
	array(
		"COMPONENT_TEMPLATE" => "simple",
		"IBLOCK_TYPE" => "content",
		"ELEMENT_ID" => CUR_ELEMENT_ID,
		"IBLOCK_ID" => CUR_IBLOCK_ID,
		"ELEMENT_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"PROPERTY_CODE" => array(
			0 => "WORK_EXPERIENCE",
			1 => "HLB_SPETSIALIZATSIYA_VRACHA",
			2 => "",
		),
		"OFFERS_LIMIT" => "0",
		"BACKGROUND_IMAGE" => "-",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"CHECK_SECTION_ID_VARIABLE" => "N",
		"SEF_MODE" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"DISPLAY_COMPARE" => "N",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"USE_ELEMENT_COUNTER" => "Y",
		"SHOW_DEACTIVATED" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N"
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>